//Maya ASCII 2018 scene
//Name: PlasticWaterBottle.ma
//Last modified: Thu, Oct 25, 2018 02:15:58 PM
//Codeset: 1252
requires maya "2018";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2018";
fileInfo "version" "2018";
fileInfo "cutIdentifier" "201706261615-f9658c4cfc";
fileInfo "osv" "Microsoft Windows 8 Enterprise Edition, 64-bit  (Build 9200)\n";
fileInfo "license" "education";
createNode transform -s -n "persp";
	rename -uid "6E11F4E2-4EE4-C5B3-0373-A49543F261DF";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -3.8652083046435788 3.944626474663639 4.2662394539378052 ;
	setAttr ".r" -type "double3" -23.138352729598921 -40.200000000000344 2.0820707114365837e-15 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "B65C480E-4964-304C-40A8-3AA7CFD0BF5E";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 7.7483371765077553;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
	setAttr ".ai_translator" -type "string" "perspective";
createNode transform -s -n "top";
	rename -uid "27D9169F-4B28-41CC-9CD6-BA8E456AB097";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 1000.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "2BF48F2F-4AA6-FFFA-3699-13A40C66A6DB";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "front";
	rename -uid "E3A45BE2-4B8E-A3AC-051F-DF8E5EE01FB9";
	setAttr ".t" -type "double3" -0.20253987048351058 1.972861738617453 1000.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "6B636114-403C-03E1-0699-3B991FE1F12A";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 6.7298609455511382;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "side";
	rename -uid "8098B8F5-4460-275F-1BD9-26A35DA0079C";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1 1.1869482285735911 0.20048568969845043 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "AF96EDC6-4E51-46A7-28D6-0AA708721A90";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 4.0994056302783957;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -n "pCylinder1";
	rename -uid "8659768A-4DC0-EE97-46FA-7B9954A1849E";
	setAttr ".s" -type "double3" 0.65849331484961005 2.0949718901283076 0.65849331484961005 ;
createNode transform -n "transform2" -p "pCylinder1";
	rename -uid "ADF69BDD-4B8E-1857-BF2B-4C9625325D45";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape1" -p "transform2";
	rename -uid "C51AE44B-4FC6-C67B-C6EF-37A5232B5BD0";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.49999998509883881 0.84374997019767761 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "imagePlane1";
	rename -uid "0985583D-47DE-2020-1635-AC8F26DA0330";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -0.292266483509106 0 -1.4332041637046804 ;
createNode imagePlane -n "imagePlaneShape1" -p "imagePlane1";
	rename -uid "85E7A201-41CD-DE1B-9719-8488F60FFFC0";
	setAttr -k off ".v";
	setAttr ".fc" 101;
	setAttr ".imn" -type "string" "C:/Users/10795516/Git Repos/JuniorProject/Assets/Plastic Junk/References/500ml-plastic-water-bottle-500x500.jpg";
	setAttr ".cov" -type "short2" 500 500 ;
	setAttr ".dlc" no;
	setAttr ".w" 5;
	setAttr ".h" 5;
	setAttr ".cs" -type "string" "sRGB";
createNode transform -n "pCylinder2";
	rename -uid "77CA86FE-4DE4-F1D8-84AF-6B9150087736";
	setAttr ".t" -type "double3" 0 2.020517943338298 0 ;
	setAttr ".s" -type "double3" 0.32731262195144439 0.14656331804042042 0.32731262195144439 ;
createNode transform -n "transform1" -p "pCylinder2";
	rename -uid "9691446D-43C2-07BF-80B5-CB9466EA3D2F";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape2" -p "transform1";
	rename -uid "082D1ECC-4D96-0BE2-793F-7B95B4504420";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.49999998509883881 0.15624996274709702 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCylinder3";
	rename -uid "D917B381-4B6C-E755-65DD-479B9481E159";
	setAttr ".rp" -type "double3" -1.1774778035933409e-07 0.053804894102893019 -1.1774778035933409e-07 ;
	setAttr ".sp" -type "double3" -1.1774778035933409e-07 0.053804894102893019 -1.1774778035933409e-07 ;
createNode mesh -n "pCylinder3Shape" -p "pCylinder3";
	rename -uid "74D20DCF-48A8-5F44-3F77-A8B8545F7619";
	setAttr -k off ".v";
	setAttr -s 6 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode lightLinker -s -n "lightLinker1";
	rename -uid "32FAAAA1-49D9-ACAA-53E5-8FB1FEAE6957";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "F0ED5FAB-4B16-E863-7808-EB87C28530FB";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "17CA5467-48DF-18AA-6484-829A29EF8546";
createNode displayLayerManager -n "layerManager";
	rename -uid "17D126BA-435A-82F2-597D-73BD06AFE1FF";
createNode displayLayer -n "defaultLayer";
	rename -uid "9F1DA4B8-4314-58C3-C0D6-9C9E19DD245D";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "4BBB35A1-4F5D-D69A-5DF4-6DA3F5B66AC8";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "AE80D77A-4C3A-B374-57BC-ECB43B72ECEC";
	setAttr ".g" yes;
createNode polyCylinder -n "polyCylinder1";
	rename -uid "1BA77764-4435-87FB-5EEA-B2A5633058EB";
	setAttr ".sc" 1;
	setAttr ".cuv" 3;
createNode polySplitRing -n "polySplitRing1";
	rename -uid "D8E40831-447B-3D37-80B7-C7B7F3C712E0";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[40:59]";
	setAttr ".ix" -type "matrix" 0.65849331484961005 0 0 0 0 2.0949718901283076 0 0 0 0 0.65849331484961005 0
		 0 0 0 1;
	setAttr ".wt" 0.74600625038146973;
	setAttr ".dr" no;
	setAttr ".re" 59;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing2";
	rename -uid "CB9290F4-4858-4867-CB01-C3B301E49722";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 19 "e[100:101]" "e[103]" "e[105]" "e[107]" "e[109]" "e[111]" "e[113]" "e[115]" "e[117]" "e[119]" "e[121]" "e[123]" "e[125]" "e[127]" "e[129]" "e[131]" "e[133]" "e[135]" "e[137]";
	setAttr ".ix" -type "matrix" 0.65849331484961005 0 0 0 0 2.0949718901283076 0 0 0 0 0.65849331484961005 0
		 0 0 0 1;
	setAttr ".wt" 0.089209042489528656;
	setAttr ".re" 100;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing3";
	rename -uid "CFD42560-44BD-AD9F-72CD-5BB6340C4370";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 19 "e[140:141]" "e[143]" "e[145]" "e[147]" "e[149]" "e[151]" "e[153]" "e[155]" "e[157]" "e[159]" "e[161]" "e[163]" "e[165]" "e[167]" "e[169]" "e[171]" "e[173]" "e[175]" "e[177]";
	setAttr ".ix" -type "matrix" 0.65849331484961005 0 0 0 0 2.0949718901283076 0 0 0 0 0.65849331484961005 0
		 0 0 0 1;
	setAttr ".wt" 0.10171392560005188;
	setAttr ".re" 140;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing4";
	rename -uid "941D6E21-45AA-26CA-BA38-9383E04F09AA";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 19 "e[180:181]" "e[183]" "e[185]" "e[187]" "e[189]" "e[191]" "e[193]" "e[195]" "e[197]" "e[199]" "e[201]" "e[203]" "e[205]" "e[207]" "e[209]" "e[211]" "e[213]" "e[215]" "e[217]";
	setAttr ".ix" -type "matrix" 0.65849331484961005 0 0 0 0 2.0949718901283076 0 0 0 0 0.65849331484961005 0
		 0 0 0 1;
	setAttr ".wt" 0.51583033800125122;
	setAttr ".re" 180;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing5";
	rename -uid "D1D7BAD9-443C-8243-E4AA-A7BCB15685B0";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[40:59]";
	setAttr ".ix" -type "matrix" 0.65849331484961005 0 0 0 0 2.0949718901283076 0 0 0 0 0.65849331484961005 0
		 0 0 0 1;
	setAttr ".wt" 0.96495413780212402;
	setAttr ".dr" no;
	setAttr ".re" 59;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing6";
	rename -uid "03990C37-44B3-60C5-D805-F8A199204EBD";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[40:59]";
	setAttr ".ix" -type "matrix" 0.65849331484961005 0 0 0 0 2.0949718901283076 0 0 0 0 0.65849331484961005 0
		 0 0 0 1;
	setAttr ".wt" 0.97457695007324219;
	setAttr ".dr" no;
	setAttr ".re" 59;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing7";
	rename -uid "11646BF6-4D87-37E5-0507-10918ABA8CB1";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[40:59]";
	setAttr ".ix" -type "matrix" 0.65849331484961005 0 0 0 0 2.0949718901283076 0 0 0 0 0.65849331484961005 0
		 0 0 0 1;
	setAttr ".wt" 0.94037419557571411;
	setAttr ".dr" no;
	setAttr ".re" 59;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing8";
	rename -uid "DBA478ED-450E-6568-D8B6-AAB2B7019B7D";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[40:59]";
	setAttr ".ix" -type "matrix" 0.65849331484961005 0 0 0 0 2.0949718901283076 0 0 0 0 0.65849331484961005 0
		 0 0 0 1;
	setAttr ".wt" 0.96961772441864014;
	setAttr ".dr" no;
	setAttr ".re" 59;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing9";
	rename -uid "B70E0E2F-44F8-CF4D-ECEE-5DA8A64E0804";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[40:59]";
	setAttr ".ix" -type "matrix" 0.65849331484961005 0 0 0 0 2.0949718901283076 0 0 0 0 0.65849331484961005 0
		 0 0 0 1;
	setAttr ".wt" 0.96185398101806641;
	setAttr ".dr" no;
	setAttr ".re" 59;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing10";
	rename -uid "C61B0580-47C6-02E9-C9DB-5499C71A19BB";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[40:59]";
	setAttr ".ix" -type "matrix" 0.65849331484961005 0 0 0 0 2.0949718901283076 0 0 0 0 0.65849331484961005 0
		 0 0 0 1;
	setAttr ".wt" 0.96459019184112549;
	setAttr ".dr" no;
	setAttr ".re" 59;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing11";
	rename -uid "40F24188-48F5-87BB-C652-C888E51C3D71";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[40:59]";
	setAttr ".ix" -type "matrix" 0.65849331484961005 0 0 0 0 2.0949718901283076 0 0 0 0 0.65849331484961005 0
		 0 0 0 1;
	setAttr ".wt" 0.96636867523193359;
	setAttr ".dr" no;
	setAttr ".re" 59;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing12";
	rename -uid "169B1904-46D1-BA1F-8595-B7A4589059B3";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[40:59]";
	setAttr ".ix" -type "matrix" 0.65849331484961005 0 0 0 0 2.0949718901283076 0 0 0 0 0.65849331484961005 0
		 0 0 0 1;
	setAttr ".wt" 0.95746612548828125;
	setAttr ".dr" no;
	setAttr ".re" 59;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing13";
	rename -uid "877E8080-43DB-109F-AB45-18B3B9D38B3C";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[40:59]";
	setAttr ".ix" -type "matrix" 0.65849331484961005 0 0 0 0 2.0949718901283076 0 0 0 0 0.65849331484961005 0
		 0 0 0 1;
	setAttr ".wt" 0.96001899242401123;
	setAttr ".dr" no;
	setAttr ".re" 59;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing14";
	rename -uid "DA161C96-4ECA-82EA-1D14-1E82241DE238";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[40:59]";
	setAttr ".ix" -type "matrix" 0.65849331484961005 0 0 0 0 2.0949718901283076 0 0 0 0 0.65849331484961005 0
		 0 0 0 1;
	setAttr ".wt" 0.95835387706756592;
	setAttr ".dr" no;
	setAttr ".re" 59;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing15";
	rename -uid "E0D42254-44A4-A03E-772B-FAB9C89B18BD";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[40:59]";
	setAttr ".ix" -type "matrix" 0.65849331484961005 0 0 0 0 2.0949718901283076 0 0 0 0 0.65849331484961005 0
		 0 0 0 1;
	setAttr ".wt" 0.91791665554046631;
	setAttr ".dr" no;
	setAttr ".re" 59;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing16";
	rename -uid "2A2A0A15-4E13-45C6-40B8-99B27ACFBDCD";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[40:59]";
	setAttr ".ix" -type "matrix" 0.65849331484961005 0 0 0 0 2.0949718901283076 0 0 0 0 0.65849331484961005 0
		 0 0 0 1;
	setAttr ".wt" 0.94389122724533081;
	setAttr ".dr" no;
	setAttr ".re" 59;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing17";
	rename -uid "38F06718-4365-8AA8-E3F2-FE828F598D16";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 19 "e[180:181]" "e[183]" "e[185]" "e[187]" "e[189]" "e[191]" "e[193]" "e[195]" "e[197]" "e[199]" "e[201]" "e[203]" "e[205]" "e[207]" "e[209]" "e[211]" "e[213]" "e[215]" "e[217]";
	setAttr ".ix" -type "matrix" 0.65849331484961005 0 0 0 0 2.0949718901283076 0 0 0 0 0.65849331484961005 0
		 0 0 0 1;
	setAttr ".wt" 0.57218343019485474;
	setAttr ".dr" no;
	setAttr ".re" 197;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak1";
	rename -uid "B33686EF-4C7A-9808-AC35-F38387EB3BA1";
	setAttr ".uopa" yes;
	setAttr -s 60 ".tk[62:121]" -type "float3"  -0.014735981 0 -2.6349984e-09
		 -0.014014752 0 -0.0045536705 -0.011921659 0 -0.0086615942 -0.0086615924 0 -0.011921661
		 -0.0045536701 0 -0.014014754 -1.3174992e-09 0 -0.014735984 0.0045536673 0 -0.014014754
		 0.0086615905 0 -0.011921662 0.011921658 0 -0.0086615961 0.014014751 0 -0.0045536715
		 0.014735981 0 -2.6349984e-09 0.014014751 0 0.0045536663 0.011921659 0 0.0086615905
		 0.0086615924 0 0.011921659 0.0045536687 0 0.014014753 -1.7566656e-09 0 0.014735984
		 -0.0045536719 0 0.014014754 -0.0086615989 0 0.011921663 -0.011921667 0 0.0086615942
		 -0.014014762 0 0.0045536682 -0.069995902 0 -1.2516241e-08 -0.066570073 0 -0.021629935
		 -0.056627885 0 -0.041142575 -0.041142572 0 -0.056627888 -0.021629928 0 -0.066570073
		 -6.2581207e-09 0 -0.069995925 0.021629922 0 -0.066570073 0.041142561 0 -0.056627888
		 0.056627881 0 -0.041142583 0.066570066 0 -0.021629944 0.069995902 0 -1.2516241e-08
		 0.066570066 0 0.02162992 0.056627881 0 0.041142561 0.041142564 0 0.056627881 0.021629926
		 0 0.066570073 -8.3441609e-09 0 0.069995925 -0.021629944 0 0.066570073 -0.041142594
		 0 0.056627888 -0.056627929 0 0.041142575 -0.066570126 0 0.021629924 -0.57101911 0
		 -1.0210621e-07 -0.54307163 0 -0.17645478 -0.46196422 0 -0.33563676 -0.33563673 0
		 -0.46196422 -0.17645474 0 -0.54307169 -5.1053103e-08 0 -0.57101923 0.17645462 0 -0.54307169
		 0.33563665 0 -0.46196431 0.46196416 0 -0.33563679 0.54307157 0 -0.17645483 0.57101911
		 0 -1.0210621e-07 0.54307157 0 0.17645459 0.46196419 0 0.33563665 0.33563673 0 0.46196419
		 0.17645468 0 0.54307169 -6.8070769e-08 0 0.57101923 -0.17645484 0 0.54307169 -0.33563694
		 0 0.46196431 -0.46196452 0 0.33563676 -0.54307187 0 0.17645465;
createNode polyExtrudeFace -n "polyExtrudeFace1";
	rename -uid "ED4E6A92-4FC8-42FF-7E81-55B13D1810EE";
	setAttr ".ics" -type "componentList" 1 "f[160:179]";
	setAttr ".ix" -type "matrix" 0.65849331484961005 0 0 0 0 2.0949718901283076 0 0 0 0 0.65849331484961005 0
		 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -7.8498523e-08 0.88286841 -1.1774778e-07 ;
	setAttr ".rs" 44810;
	setAttr ".lt" -type "double3" 1.214306433183765e-17 0 -0.036211568209009837 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.65849347184665052 0.84452815090069877 -0.658493628843691 ;
	setAttr ".cbx" -type "double3" 0.65849331484961005 0.92120866665013634 0.65849339334813028 ;
createNode polyTweak -n "polyTweak2";
	rename -uid "53BDCC9D-4C56-BC82-D55A-B99B5F6126E4";
	setAttr ".uopa" yes;
	setAttr -s 20 ".tk[362:381]" -type "float3"  -0.017518438 0 0.005692088
		 -0.018419977 0 3.2937475e-09 -0.017518438 0 -0.0056920825 -0.014902074 0 -0.010826987
		 -0.01082699 0 -0.014902074 -0.0056920848 0 -0.017518438 2.1958322e-09 0 -0.018419979
		 0.005692088 0 -0.017518444 0.010826997 0 -0.014902079 0.014902084 0 -0.010826992
		 0.017518451 0 -0.0056920839 0.018419977 0 3.2937475e-09 0.017518438 0 0.0056920876
		 0.014902075 0 0.010826993 0.01082699 0 0.014902079 0.0056920862 0 0.017518442 1.6468735e-09
		 0 0.018419979 -0.0056920829 0 0.017518444 -0.010826987 0 0.014902079 -0.014902074
		 0 0.010826993;
createNode polyExtrudeFace -n "polyExtrudeFace2";
	rename -uid "6213E234-4F40-D6DC-3D6A-079066A5A693";
	setAttr ".ics" -type "componentList" 1 "f[205:206]";
	setAttr ".ix" -type "matrix" 0.65849331484961005 0 0 0 0 2.0949718901283076 0 0 0 0 0.65849331484961005 0
		 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -0.1935264 0.62726629 0.59561288 ;
	setAttr ".rs" 60867;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.38705277148114975 0.58527450290953231 0.53273236629879916 ;
	setAttr ".cbx" -type "double3" -1.9624632398728439e-08 0.66925810627971094 0.65849339334813028 ;
createNode polyTweak -n "polyTweak3";
	rename -uid "9C410485-41B0-588B-C240-BCA751E5A5FB";
	setAttr ".uopa" yes;
	setAttr -s 40 ".tk[382:421]" -type "float3"  0.019195784 0.0064751273 2.4238642e-09
		 0.018256277 0.0064751273 0.0059318272 0.019195784 -0.0064751273 2.4238642e-09 0.018256277
		 -0.0064751273 0.0059318272 0.015529717 0.0064751273 0.011283001 0.015529717 -0.0064751273
		 0.011283001 0.011283001 0.0064751273 0.01552972 0.011283001 -0.0064751273 0.01552972
		 0.0059318254 0.0064751273 0.018256281 0.0059318254 -0.0064751273 0.018256281 2.4218032e-09
		 0.0064751273 0.019195788 2.4218032e-09 -0.0064751273 0.019195788 -0.0059318217 0.0064751273
		 0.018256281 -0.0059318217 -0.0064751273 0.018256281 -0.011282996 0.0064751273 0.01552972
		 -0.011282996 -0.0064751273 0.01552972 -0.015529715 0.0064751273 0.011283003 -0.015529715
		 -0.0064751273 0.011283003 -0.018256277 0.0064751273 0.0059318272 -0.018256277 -0.0064751273
		 0.0059318272 -0.019195784 0.0064751273 3.6296137e-09 -0.019195784 -0.0064751273 3.6296137e-09
		 -0.018256277 0.0064751273 -0.0059318207 -0.018256277 -0.0064751273 -0.0059318207
		 -0.015529716 0.0064751273 -0.011282996 -0.015529716 -0.0064751273 -0.011282996 -0.011282999
		 0.0064751273 -0.015529715 -0.011282999 -0.0064751273 -0.015529715 -0.0059318226 0.0064751273
		 -0.018256281 -0.0059318226 -0.0064751273 -0.018256281 3.0246778e-09 0.0064751273
		 -0.019195788 3.0246778e-09 -0.0064751273 -0.019195788 0.0059318291 0.0064751273 -0.018256281
		 0.0059318291 -0.0064751273 -0.018256281 0.011283008 0.0064751273 -0.01552972 0.011283008
		 -0.0064751273 -0.01552972 0.015529728 0.0064751273 -0.011283 0.015529728 -0.0064751273
		 -0.011283 0.018256292 0.0064751273 -0.0059318244 0.018256292 -0.0064751273 -0.0059318244;
createNode polyExtrudeFace -n "polyExtrudeFace3";
	rename -uid "668FC081-4542-3B4F-356A-CCB9A1876AA9";
	setAttr ".ics" -type "componentList" 3 "f[200:219]" "f[441]" "f[445]";
	setAttr ".ix" -type "matrix" 0.65849331484961005 0 0 0 0 2.0949718901283076 0 0 0 0 0.65849331484961005 0
		 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -1.1774778e-07 0.62726629 -1.1774778e-07 ;
	setAttr ".rs" 32796;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.658493628843691 0.58527450290953231 -0.658493628843691 ;
	setAttr ".cbx" -type "double3" 0.65849339334813028 0.66925810627971094 0.65849339334813028 ;
createNode polyExtrudeFace -n "polyExtrudeFace4";
	rename -uid "6810AA49-4D41-56D8-3CC4-0F9010880EE0";
	setAttr ".ics" -type "componentList" 1 "f[240:259]";
	setAttr ".ix" -type "matrix" 0.65849331484961005 0 0 0 0 2.0949718901283076 0 0 0 0 0.65849331484961005 0
		 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -1.1774778e-07 0.43739042 -1.1774778e-07 ;
	setAttr ".rs" 59603;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.658493628843691 0.39174708262934399 -0.658493628843691 ;
	setAttr ".cbx" -type "double3" 0.65849339334813028 0.48303377362026373 0.65849339334813028 ;
createNode polyTweak -n "polyTweak4";
	rename -uid "E2602FE4-404B-A68B-E212-58B15E117C79";
	setAttr ".uopa" yes;
	setAttr -s 128 ".tk";
	setAttr ".tk[162]" -type "float3" 0 0 -8.8817842e-16 ;
	setAttr ".tk[163]" -type "float3" -3.7252903e-09 0 -9.3132257e-10 ;
	setAttr ".tk[164]" -type "float3" -3.7252903e-09 0 0 ;
	setAttr ".tk[165]" -type "float3" -3.7252903e-09 0 -3.7252903e-09 ;
	setAttr ".tk[166]" -type "float3" -9.3132257e-10 0 -3.7252903e-09 ;
	setAttr ".tk[167]" -type "float3" -1.3322676e-15 0 0 ;
	setAttr ".tk[169]" -type "float3" -1.8626451e-09 0 -3.7252903e-09 ;
	setAttr ".tk[170]" -type "float3" 3.7252903e-09 0 0 ;
	setAttr ".tk[171]" -type "float3" 0 0 -9.3132257e-10 ;
	setAttr ".tk[172]" -type "float3" 0 0 -8.8817842e-16 ;
	setAttr ".tk[174]" -type "float3" 3.7252903e-09 0 -1.8626451e-09 ;
	setAttr ".tk[175]" -type "float3" 3.7252903e-09 0 3.7252903e-09 ;
	setAttr ".tk[177]" -type "float3" -8.8817842e-16 0 0 ;
	setAttr ".tk[178]" -type "float3" 1.8626451e-09 0 0 ;
	setAttr ".tk[179]" -type "float3" 1.8626451e-09 0 3.7252903e-09 ;
	setAttr ".tk[180]" -type "float3" 0 0 3.7252903e-09 ;
	setAttr ".tk[181]" -type "float3" 0 0 -9.3132257e-10 ;
	setAttr ".tk[182]" -type "float3" 0 0 -8.8817842e-16 ;
	setAttr ".tk[183]" -type "float3" -3.7252903e-09 0 -9.3132257e-10 ;
	setAttr ".tk[184]" -type "float3" -3.7252903e-09 0 0 ;
	setAttr ".tk[185]" -type "float3" -3.7252903e-09 0 -3.7252903e-09 ;
	setAttr ".tk[186]" -type "float3" -9.3132257e-10 0 -3.7252903e-09 ;
	setAttr ".tk[187]" -type "float3" -1.3322676e-15 0 0 ;
	setAttr ".tk[189]" -type "float3" -1.8626451e-09 0 -3.7252903e-09 ;
	setAttr ".tk[190]" -type "float3" 3.7252903e-09 0 0 ;
	setAttr ".tk[191]" -type "float3" 0 0 -9.3132257e-10 ;
	setAttr ".tk[192]" -type "float3" 0 0 -8.8817842e-16 ;
	setAttr ".tk[194]" -type "float3" 3.7252903e-09 0 -1.8626451e-09 ;
	setAttr ".tk[195]" -type "float3" 3.7252903e-09 0 3.7252903e-09 ;
	setAttr ".tk[197]" -type "float3" -8.8817842e-16 0 0 ;
	setAttr ".tk[198]" -type "float3" 1.8626451e-09 0 0 ;
	setAttr ".tk[199]" -type "float3" 1.8626451e-09 0 3.7252903e-09 ;
	setAttr ".tk[200]" -type "float3" 0 0 3.7252903e-09 ;
	setAttr ".tk[201]" -type "float3" 0 0 -9.3132257e-10 ;
	setAttr ".tk[382]" -type "float3" 0 0.0023651989 0 ;
	setAttr ".tk[383]" -type "float3" 0 0.0023651989 0 ;
	setAttr ".tk[384]" -type "float3" 0 -0.0023651989 0 ;
	setAttr ".tk[385]" -type "float3" 0 -0.0023651989 0 ;
	setAttr ".tk[386]" -type "float3" 0 0.0023651989 0 ;
	setAttr ".tk[387]" -type "float3" 0 -0.0023651989 0 ;
	setAttr ".tk[388]" -type "float3" 0 0.0023651989 0 ;
	setAttr ".tk[389]" -type "float3" 0 -0.0023651989 0 ;
	setAttr ".tk[390]" -type "float3" 0 0.0023651989 0 ;
	setAttr ".tk[391]" -type "float3" 0 -0.0023651989 0 ;
	setAttr ".tk[392]" -type "float3" 0 0.0023651989 0 ;
	setAttr ".tk[393]" -type "float3" 0 -0.0023651989 0 ;
	setAttr ".tk[394]" -type "float3" 0 0.0023651989 0 ;
	setAttr ".tk[395]" -type "float3" 0 -0.0023651989 0 ;
	setAttr ".tk[396]" -type "float3" 0 0.0023651989 0 ;
	setAttr ".tk[397]" -type "float3" 0 -0.0023651989 0 ;
	setAttr ".tk[398]" -type "float3" 0 0.0023651989 0 ;
	setAttr ".tk[399]" -type "float3" 0 -0.0023651989 0 ;
	setAttr ".tk[400]" -type "float3" 0 0.0023651989 0 ;
	setAttr ".tk[401]" -type "float3" 0 -0.0023651989 0 ;
	setAttr ".tk[402]" -type "float3" 0 0.0023651989 0 ;
	setAttr ".tk[403]" -type "float3" 0 -0.0023651989 0 ;
	setAttr ".tk[404]" -type "float3" 0 0.0023651989 0 ;
	setAttr ".tk[405]" -type "float3" 0 -0.0023651989 0 ;
	setAttr ".tk[406]" -type "float3" 0 0.0023651989 0 ;
	setAttr ".tk[407]" -type "float3" 0 -0.0023651989 0 ;
	setAttr ".tk[408]" -type "float3" 0 0.0023651989 0 ;
	setAttr ".tk[409]" -type "float3" 0 -0.0023651989 0 ;
	setAttr ".tk[410]" -type "float3" 0 0.0023651989 0 ;
	setAttr ".tk[411]" -type "float3" 0 -0.0023651989 0 ;
	setAttr ".tk[412]" -type "float3" 0 0.0023651989 0 ;
	setAttr ".tk[413]" -type "float3" 0 -0.0023651989 0 ;
	setAttr ".tk[414]" -type "float3" 0 0.0023651989 0 ;
	setAttr ".tk[415]" -type "float3" 0 -0.0023651989 0 ;
	setAttr ".tk[416]" -type "float3" 0 0.0023651989 0 ;
	setAttr ".tk[417]" -type "float3" 0 -0.0023651989 0 ;
	setAttr ".tk[418]" -type "float3" 0 0.0023651989 0 ;
	setAttr ".tk[419]" -type "float3" 0 -0.0023651989 0 ;
	setAttr ".tk[420]" -type "float3" 0 0.0023651989 0 ;
	setAttr ".tk[421]" -type "float3" 0 -0.0023651989 0 ;
	setAttr ".tk[422]" -type "float3" 7.4505806e-09 4.6566129e-10 0 ;
	setAttr ".tk[423]" -type "float3" 0 4.6566129e-10 -3.7252903e-09 ;
	setAttr ".tk[424]" -type "float3" 7.4505806e-09 -4.6566129e-10 0 ;
	setAttr ".tk[425]" -type "float3" 0 -4.6566129e-10 -3.7252903e-09 ;
	setAttr ".tk[426]" -type "float3" -9.3132257e-09 4.6566129e-10 -3.7252903e-09 ;
	setAttr ".tk[427]" -type "float3" -9.3132257e-09 -4.6566129e-10 -3.7252903e-09 ;
	setAttr ".tk[428]" -type "float3" -0.030086577 0.011634042 -5.3798539e-09 ;
	setAttr ".tk[429]" -type "float3" -0.028613925 0.011634042 -0.0092971921 ;
	setAttr ".tk[430]" -type "float3" -0.030086577 -0.011634042 -5.3798539e-09 ;
	setAttr ".tk[431]" -type "float3" -0.028613925 -0.011634042 -0.0092971921 ;
	setAttr ".tk[432]" -type "float3" -0.024340339 0.011634042 -0.0176844 ;
	setAttr ".tk[433]" -type "float3" -0.024340339 -0.011634042 -0.0176844 ;
	setAttr ".tk[434]" -type "float3" -0.017684435 0.011634042 -0.024340339 ;
	setAttr ".tk[435]" -type "float3" -0.017684435 -0.011634042 -0.024340339 ;
	setAttr ".tk[436]" -type "float3" -0.0092971921 0.011634042 -0.028613925 ;
	setAttr ".tk[437]" -type "float3" -0.0092971921 -0.011634042 -0.028613925 ;
	setAttr ".tk[438]" -type "float3" -4.483244e-09 0.011634042 -0.030086577 ;
	setAttr ".tk[439]" -type "float3" -4.483244e-09 -0.011634042 -0.030086577 ;
	setAttr ".tk[440]" -type "float3" -4.483244e-09 0.011634042 -0.030086577 ;
	setAttr ".tk[441]" -type "float3" 0.0092971688 0.011634042 -0.028613925 ;
	setAttr ".tk[442]" -type "float3" -4.483244e-09 -0.011634042 -0.030086577 ;
	setAttr ".tk[443]" -type "float3" 0.0092971688 -0.011634042 -0.028613925 ;
	setAttr ".tk[444]" -type "float3" 0.01768453 0.011634042 -0.024340339 ;
	setAttr ".tk[445]" -type "float3" 0.01768453 -0.011634042 -0.024340339 ;
	setAttr ".tk[446]" -type "float3" 0.01768453 0.011634042 -0.024340339 ;
	setAttr ".tk[447]" -type "float3" 0.024340272 0.011634042 -0.0176844 ;
	setAttr ".tk[448]" -type "float3" 0.01768453 -0.011634042 -0.024340339 ;
	setAttr ".tk[449]" -type "float3" 0.024340272 -0.011634042 -0.0176844 ;
	setAttr ".tk[450]" -type "float3" 0.028613847 0.011634042 -0.009297207 ;
	setAttr ".tk[451]" -type "float3" 0.028613847 -0.011634042 -0.009297207 ;
	setAttr ".tk[452]" -type "float3" 0.030086577 0.011634042 -5.3798539e-09 ;
	setAttr ".tk[453]" -type "float3" 0.030086577 -0.011634042 -5.3798539e-09 ;
	setAttr ".tk[454]" -type "float3" 0.028613847 0.011634042 0.0092971688 ;
	setAttr ".tk[455]" -type "float3" 0.028613847 -0.011634042 0.0092971688 ;
	setAttr ".tk[456]" -type "float3" 0.024340272 0.011634042 0.01768453 ;
	setAttr ".tk[457]" -type "float3" 0.024340272 -0.011634042 0.01768453 ;
	setAttr ".tk[458]" -type "float3" 0.017684435 0.011634042 0.024340272 ;
	setAttr ".tk[459]" -type "float3" 0.017684435 -0.011634042 0.024340272 ;
	setAttr ".tk[460]" -type "float3" 0.0092972284 0.011634042 0.028614163 ;
	setAttr ".tk[461]" -type "float3" 0.0092972284 -0.011634042 0.028614163 ;
	setAttr ".tk[462]" -type "float3" -5.3798539e-09 0.011634042 0.030086577 ;
	setAttr ".tk[463]" -type "float3" -5.3798539e-09 -0.011634042 0.030086577 ;
	setAttr ".tk[464]" -type "float3" -0.009297248 0.011634042 0.028614163 ;
	setAttr ".tk[465]" -type "float3" -0.009297248 -0.011634042 0.028614163 ;
	setAttr ".tk[466]" -type "float3" -0.017684307 0.011634042 0.024340339 ;
	setAttr ".tk[467]" -type "float3" -0.017684307 -0.011634042 0.024340339 ;
	setAttr ".tk[468]" -type "float3" -0.024340438 0.011634042 0.017684435 ;
	setAttr ".tk[469]" -type "float3" -0.024340438 -0.011634042 0.017684435 ;
	setAttr ".tk[470]" -type "float3" -0.028614013 0.011634042 0.0092972582 ;
	setAttr ".tk[471]" -type "float3" -0.028614013 -0.011634042 0.0092972582 ;
createNode polyExtrudeFace -n "polyExtrudeFace5";
	rename -uid "C74A4FE9-4F83-D86A-424A-48AC8EB92B67";
	setAttr ".ics" -type "componentList" 1 "f[280:299]";
	setAttr ".ix" -type "matrix" 0.65849331484961005 0 0 0 0 2.0949718901283076 0 0 0 0 0.65849331484961005 0
		 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -1.1774778e-07 0.25700912 -1.1774778e-07 ;
	setAttr ".rs" 47645;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.658493628843691 0.20590282320079378 -0.658493628843691 ;
	setAttr ".cbx" -type "double3" 0.65849339334813028 0.30811542551010029 0.65849339334813028 ;
createNode polyTweak -n "polyTweak5";
	rename -uid "4426F6A5-4C8E-53A0-D7E5-539A0BF9F52C";
	setAttr ".uopa" yes;
	setAttr -s 40 ".tk[472:511]" -type "float3"  -0.039143797 0.01294818 -6.9994548e-09
		 -0.03722797 0.01294818 -0.012096103 -0.039143797 -0.01294818 -6.9994548e-09 -0.03722797
		 -0.01294818 -0.012096103 -0.031668004 0.01294818 -0.023008157 -0.031668004 -0.01294818
		 -0.023008157 -0.023008147 0.01294818 -0.031668004 -0.023008147 -0.01294818 -0.031668004
		 -0.012096103 0.01294818 -0.03722797 -0.012096103 -0.01294818 -0.03722797 -5.8328791e-09
		 0.01294818 -0.039143797 -5.8328791e-09 -0.01294818 -0.039143797 0.012096093 0.01294818
		 -0.03722797 0.012096093 -0.01294818 -0.03722797 0.02300814 0.01294818 -0.031668004
		 0.02300814 -0.01294818 -0.031668004 0.031667989 0.01294818 -0.023008157 0.031667989
		 -0.01294818 -0.023008157 0.037227973 0.01294818 -0.012096108 0.037227973 -0.01294818
		 -0.012096108 0.039143797 0.01294818 -6.9994548e-09 0.039143797 -0.01294818 -6.9994548e-09
		 0.037227973 0.01294818 0.012096093 0.037227973 -0.01294818 0.012096093 0.031667989
		 0.01294818 0.02300814 0.031667989 -0.01294818 0.02300814 0.023008147 0.01294818 0.031667989
		 0.023008147 -0.01294818 0.031667989 0.012096099 0.01294818 0.037227977 0.012096099
		 -0.01294818 0.037227977 -6.9994548e-09 0.01294818 0.039143797 -6.9994548e-09 -0.01294818
		 0.039143797 -0.012096114 0.01294818 0.037227977 -0.012096114 -0.01294818 0.037227977
		 -0.023008162 0.01294818 0.031668019 -0.023008162 -0.01294818 0.031668004 -0.031668037
		 0.01294818 0.023008147 -0.031668037 -0.01294818 0.023008147 -0.037227999 0.01294818
		 0.012096101 -0.037227999 -0.01294818 0.012096101;
createNode polyExtrudeFace -n "polyExtrudeFace6";
	rename -uid "AE91D3CD-4BF3-5020-58BA-5096675BB3A6";
	setAttr ".ics" -type "componentList" 1 "f[320:339]";
	setAttr ".ix" -type "matrix" 0.65849331484961005 0 0 0 0 2.0949718901283076 0 0 0 0 0.65849331484961005 0
		 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -1.1774778e-07 0.06791582 -1.1774778e-07 ;
	setAttr ".rs" 58175;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.658493628843691 0.021920103143901179 -0.658493628843691 ;
	setAttr ".cbx" -type "double3" 0.65849339334813028 0.11391153731394284 0.65849339334813028 ;
createNode polyTweak -n "polyTweak6";
	rename -uid "BE3E2300-4AEB-66C7-2171-7F909C4162EA";
	setAttr ".uopa" yes;
	setAttr -s 40 ".tk[512:551]" -type "float3"  -0.044859581 0.0142979 -8.0215141e-09
		 -0.042663991 0.0142979 -0.013862373 -0.044859581 -0.014297898 -8.0215141e-09 -0.042663991
		 -0.014297898 -0.013862373 -0.036292166 0.0142979 -0.026367806 -0.036292166 -0.014297898
		 -0.026367806 -0.026367795 0.0142979 -0.036292166 -0.026367795 -0.014297898 -0.036292166
		 -0.013862373 0.0142979 -0.042663991 -0.013862373 -0.014297898 -0.042663991 -6.6845951e-09
		 0.0142979 -0.044859581 -6.6845951e-09 -0.014297898 -0.044859581 0.013862364 0.0142979
		 -0.042663991 0.013862364 -0.014297898 -0.042663991 0.026367784 0.0142979 -0.036292166
		 0.026367784 -0.014297898 -0.036292166 0.036292151 0.0142979 -0.026367806 0.036292151
		 -0.014297898 -0.026367806 0.042663988 0.0142979 -0.01386238 0.042663988 -0.014297898
		 -0.01386238 0.044859581 0.0142979 -8.0215141e-09 0.044859581 -0.014297898 -8.0215141e-09
		 0.042663988 0.0142979 0.013862364 0.042663988 -0.014297898 0.013862364 0.036292151
		 0.0142979 0.026367784 0.036292151 -0.014297898 0.026367784 0.026367795 0.0142979
		 0.036292151 0.026367795 -0.014297898 0.036292151 0.013862374 0.0142979 0.042664006
		 0.013862374 -0.014297898 0.042664006 -8.0215141e-09 0.0142979 0.044859581 -8.0215141e-09
		 -0.014297898 0.044859581 -0.01386239 0.0142979 0.042664006 -0.01386239 -0.014297898
		 0.042664006 -0.02636781 0.0142979 0.03629218 -0.02636781 -0.014297898 0.03629218
		 -0.036292192 0.0142979 0.026367795 -0.036292192 -0.014297898 0.026367795 -0.04266404
		 0.0142979 0.013862377 -0.04266404 -0.014297898 0.013862377;
createNode polySoftEdge -n "polySoftEdge1";
	rename -uid "7B3FD0B0-4DFE-ECDD-B83D-03A623694187";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 119 "e[680:681]" "e[683]" "e[685]" "e[688]" "e[690]" "e[693]" "e[695]" "e[698]" "e[700]" "e[703]" "e[705]" "e[708]" "e[710]" "e[713]" "e[715]" "e[718]" "e[720]" "e[723]" "e[725]" "e[728]" "e[730]" "e[733]" "e[735]" "e[738]" "e[740]" "e[743]" "e[745]" "e[748]" "e[750]" "e[753]" "e[755]" "e[758]" "e[760]" "e[763]" "e[765]" "e[768]" "e[770]" "e[773]" "e[775]" "e[790:791]" "e[793]" "e[795]" "e[798]" "e[800]" "e[803]" "e[805]" "e[808]" "e[810]" "e[813]" "e[815]" "e[818:819]" "e[821]" "e[823]" "e[826]" "e[828]" "e[831:832]" "e[834]" "e[836]" "e[839]" "e[841]" "e[844]" "e[846]" "e[849]" "e[851]" "e[854]" "e[856]" "e[859]" "e[861]" "e[864]" "e[866]" "e[869]" "e[871]" "e[874]" "e[876]" "e[879]" "e[881]" "e[884]" "e[886]" "e[889]" "e[891]" "e[900:901]" "e[903]" "e[905]" "e[908]" "e[910]" "e[913]" "e[915]" "e[918]" "e[920]" "e[923]" "e[925]" "e[928]" "e[930]" "e[933]" "e[935]" "e[938]" "e[940]" "e[943]" "e[945]" "e[948]" "e[950]" "e[953]" "e[955]" "e[958]" "e[960]" "e[963]" "e[965]" "e[968]" "e[970]" "e[973]" "e[975]" "e[978]" "e[980]" "e[983]" "e[985]" "e[988]" "e[990]" "e[993]" "e[995]";
	setAttr ".ix" -type "matrix" 0.65849331484961005 0 0 0 0 2.0949718901283076 0 0 0 0 0.65849331484961005 0
		 0 0 0 1;
	setAttr ".a" 180;
createNode polyTweak -n "polyTweak7";
	rename -uid "4815582A-419E-B60A-43A1-6380C2AB9BE2";
	setAttr ".uopa" yes;
	setAttr -s 40 ".tk[552:591]" -type "float3"  -0.042498719 0.01191134 -7.599362e-09
		 -0.040418692 0.01191134 -0.013132833 -0.042498719 -0.01191134 -7.599362e-09 -0.040418692
		 -0.01191134 -0.013132833 -0.034382194 0.01191134 -0.024980135 -0.034382194 -0.01191134
		 -0.024980135 -0.024980126 0.01191134 -0.034382194 -0.024980126 -0.01191134 -0.034382194
		 -0.013132833 0.01191134 -0.040418692 -0.013132833 -0.01191134 -0.040418692 -6.3328014e-09
		 0.01191134 -0.042498719 -6.3328014e-09 -0.01191134 -0.042498719 0.013132823 0.01191134
		 -0.040418692 0.013132823 -0.01191134 -0.040418692 0.024980118 0.01191134 -0.034382194
		 0.024980118 -0.01191134 -0.034382194 0.034382187 0.01191134 -0.024980135 0.034382187
		 -0.01191134 -0.024980135 0.040418707 0.01191134 -0.013132839 0.040418707 -0.01191134
		 -0.013132839 0.042498719 0.01191134 -7.599362e-09 0.042498719 -0.01191134 -7.599362e-09
		 0.040418707 0.01191134 0.013132823 0.040418707 -0.01191134 0.013132823 0.034382187
		 0.01191134 0.024980118 0.034382187 -0.01191134 0.024980118 0.024980126 0.01191134
		 0.034382187 0.024980126 -0.01191134 0.034382187 0.013132831 0.01191134 0.040418711
		 0.013132831 -0.01191134 0.040418711 -7.599362e-09 0.01191134 0.042498719 -7.599362e-09
		 -0.01191134 0.042498719 -0.013132849 0.01191134 0.040418711 -0.013132849 -0.01191134
		 0.040418711 -0.024980137 0.01191134 0.03438222 -0.024980137 -0.01191134 0.03438222
		 -0.034382235 0.01191134 0.024980126 -0.034382235 -0.01191134 0.024980126 -0.040418737
		 0.01191134 0.013132833 -0.040418737 -0.01191134 0.013132833;
createNode polySoftEdge -n "polySoftEdge2";
	rename -uid "B1275F3C-4FB0-5332-B087-1CB96033A60A";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 78 "e[1000:1001]" "e[1003]" "e[1005]" "e[1008]" "e[1010]" "e[1013]" "e[1015]" "e[1018]" "e[1020]" "e[1023]" "e[1025]" "e[1028]" "e[1030]" "e[1033]" "e[1035]" "e[1038]" "e[1040]" "e[1043]" "e[1045]" "e[1048]" "e[1050]" "e[1053]" "e[1055]" "e[1058]" "e[1060]" "e[1063]" "e[1065]" "e[1068]" "e[1070]" "e[1073]" "e[1075]" "e[1078]" "e[1080]" "e[1083]" "e[1085]" "e[1088]" "e[1090]" "e[1093]" "e[1095]" "e[1100:1101]" "e[1103]" "e[1105]" "e[1108]" "e[1110]" "e[1113]" "e[1115]" "e[1118]" "e[1120]" "e[1123]" "e[1125]" "e[1128]" "e[1130]" "e[1133]" "e[1135]" "e[1138]" "e[1140]" "e[1143]" "e[1145]" "e[1148]" "e[1150]" "e[1153]" "e[1155]" "e[1158]" "e[1160]" "e[1163]" "e[1165]" "e[1168]" "e[1170]" "e[1173]" "e[1175]" "e[1178]" "e[1180]" "e[1183]" "e[1185]" "e[1188]" "e[1190]" "e[1193]" "e[1195]";
	setAttr ".ix" -type "matrix" 0.65849331484961005 0 0 0 0 2.0949718901283076 0 0 0 0 0.65849331484961005 0
		 0 0 0 1;
	setAttr ".a" 180;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "C53F31EF-49D5-E09F-26EB-6A9F15861E21";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n"
		+ "            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n"
		+ "            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n"
		+ "            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 556\n            -height 336\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n"
		+ "            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 556\n            -height 336\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n"
		+ "            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n"
		+ "            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 556\n            -height 336\n"
		+ "            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n"
		+ "            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n"
		+ "            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n"
		+ "            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 556\n            -height 336\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -docTag \"isolOutln_fromSeln\" \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n"
		+ "            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n"
		+ "            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n"
		+ "            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"0\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n"
		+ "                -showParentContainers 1\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n"
		+ "                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 1\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n"
		+ "                -showCurveNames 0\n                -showActiveCurveNames 0\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                -valueLinesToggle 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n"
		+ "                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 1\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n"
		+ "                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n"
		+ "                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n"
		+ "                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n"
		+ "                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n"
		+ "                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -highlightConnections 0\n                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"quad\\\" -ps 1 50 50 -ps 2 50 50 -ps 3 50 50 -ps 4 50 50 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap true\n\t\t\t\t\t(localizedPanelLabel(\"Top View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Top View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera top` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 556\\n    -height 336\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Top View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera top` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 556\\n    -height 336\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 556\\n    -height 336\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 556\\n    -height 336\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Side View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Side View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera side` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 556\\n    -height 336\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Side View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera side` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 556\\n    -height 336\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Front View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Front View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera front` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"wireframe\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 556\\n    -height 336\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Front View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera front` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"wireframe\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 556\\n    -height 336\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "C7D606AA-4E16-C14B-E535-C69D65636811";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode polySoftEdge -n "polySoftEdge3";
	rename -uid "6E2EA990-4437-F6BF-3D61-A38FD6C1520A";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 64 "e[680:681]" "e[683]" "e[685]" "e[688]" "e[690]" "e[693]" "e[695]" "e[698]" "e[700]" "e[703]" "e[705]" "e[708]" "e[710]" "e[713]" "e[715]" "e[718]" "e[720]" "e[723]" "e[725]" "e[728]" "e[730]" "e[733]" "e[735]" "e[738]" "e[740]" "e[743]" "e[745]" "e[748]" "e[750]" "e[753]" "e[755]" "e[758]" "e[760]" "e[763]" "e[765]" "e[768]" "e[770]" "e[773]" "e[775]" "e[790:791]" "e[794]" "e[797:798]" "e[802:803]" "e[807:808]" "e[810]" "e[812:813]" "e[815]" "e[817:819]" "e[821:822]" "e[825:826]" "e[830:832]" "e[835]" "e[838:839]" "e[843:844]" "e[848:849]" "e[853:854]" "e[858:859]" "e[863:864]" "e[868:869]" "e[873:874]" "e[878:879]" "e[883:884]" "e[888:889]" "e[893]";
	setAttr ".ix" -type "matrix" 0.65849331484961005 0 0 0 0 2.0949718901283076 0 0 0 0 0.65849331484961005 0
		 0 0 0 1;
	setAttr ".a" 180;
createNode deleteComponent -n "deleteComponent1";
	rename -uid "52BFD09A-40FB-2A8B-1330-F681318AAAA3";
	setAttr ".dc" -type "componentList" 3 "f[200:219]" "f[441]" "f[445:489]";
createNode polyBridgeEdge -n "polyBridgeEdge1";
	rename -uid "75EFB39E-4857-F02A-23A3-96B8A1003B45";
	setAttr ".ics" -type "componentList" 21 "e[322]" "e[324]" "e[326]" "e[328]" "e[330]" "e[336]" "e[338]" "e[340]" "e[342]" "e[344]" "e[346]" "e[348]" "e[350]" "e[352]" "e[354]" "e[356]" "e[358:364]" "e[367:379]" "e[780]" "e[782:783]" "e[785:789]";
	setAttr ".ix" -type "matrix" 0.65849331484961005 0 0 0 0 2.0949718901283076 0 0 0 0 0.65849331484961005 0
		 0 0 0 1;
	setAttr ".c[0]"  0 1 1;
	setAttr ".dv" 0;
	setAttr ".sv1" 163;
	setAttr ".sv2" 183;
	setAttr ".d" 1;
createNode polySoftEdge -n "polySoftEdge4";
	rename -uid "6DF1D003-4B27-D7A2-9313-D594ED4B5BEC";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[1090:1111]";
	setAttr ".ix" -type "matrix" 0.65849331484961005 0 0 0 0 2.0949718901283076 0 0 0 0 0.65849331484961005 0
		 0 0 0 1;
	setAttr ".a" 180;
createNode polyExtrudeFace -n "polyExtrudeFace7";
	rename -uid "41066ECA-40F2-9852-59F7-3A91CC9DEA9C";
	setAttr ".ics" -type "componentList" 1 "f[544:565]";
	setAttr ".ix" -type "matrix" 0.65849331484961005 0 0 0 0 2.0949718901283076 0 0 0 0 0.65849331484961005 0
		 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -1.1774778e-07 0.62726629 -1.1774778e-07 ;
	setAttr ".rs" 34048;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.658493628843691 0.58527450290953231 -0.658493628843691 ;
	setAttr ".cbx" -type "double3" 0.65849339334813028 0.66925810627971094 0.65849339334813028 ;
createNode polySoftEdge -n "polySoftEdge5";
	rename -uid "ABDADF5B-4B10-DBD3-0417-A58ADF45CA7F";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 43 "e[1090:1091]" "e[1093]" "e[1095]" "e[1098]" "e[1100]" "e[1103]" "e[1105]" "e[1108]" "e[1110]" "e[1113]" "e[1115]" "e[1118]" "e[1120]" "e[1123]" "e[1125]" "e[1128]" "e[1130]" "e[1133]" "e[1135]" "e[1138]" "e[1140]" "e[1143]" "e[1145]" "e[1148]" "e[1150]" "e[1153]" "e[1155]" "e[1158]" "e[1160]" "e[1163]" "e[1165]" "e[1168]" "e[1170]" "e[1173]" "e[1175]" "e[1178]" "e[1180]" "e[1183]" "e[1185]" "e[1188]" "e[1190]" "e[1193]" "e[1195]";
	setAttr ".ix" -type "matrix" 0.65849331484961005 0 0 0 0 2.0949718901283076 0 0 0 0 0.65849331484961005 0
		 0 0 0 1;
	setAttr ".a" 180;
createNode polyTweak -n "polyTweak8";
	rename -uid "C90ED8A4-45B3-D188-B788-F5A4F450E5D4";
	setAttr ".uopa" yes;
	setAttr -s 49 ".tk";
	setAttr ".tk[439]" -type "float3" 4.4703484e-08 4.6566129e-09 1.8626451e-09 ;
	setAttr ".tk[440]" -type "float3" -2.1420419e-08 4.6566129e-10 -4.4703484e-08 ;
	setAttr ".tk[441]" -type "float3" -2.1420419e-08 4.6566129e-10 -4.4703484e-08 ;
	setAttr ".tk[442]" -type "float3" -2.1420419e-08 -4.6566129e-10 -4.4703484e-08 ;
	setAttr ".tk[443]" -type "float3" -2.1420419e-08 -4.6566129e-10 -4.4703484e-08 ;
	setAttr ".tk[548]" -type "float3" -0.038842954 -0.011285829 -0.012620844 ;
	setAttr ".tk[549]" -type "float3" -0.03304179 -0.011285829 -0.024006268 ;
	setAttr ".tk[550]" -type "float3" -0.03304179 0.011285829 -0.024006268 ;
	setAttr ".tk[551]" -type "float3" -0.038842954 0.011285829 -0.012620844 ;
	setAttr ".tk[552]" -type "float3" -0.024006264 -0.011285829 -0.03304179 ;
	setAttr ".tk[553]" -type "float3" -0.024006264 0.011285829 -0.03304179 ;
	setAttr ".tk[554]" -type "float3" -0.012620844 -0.011285829 -0.038842954 ;
	setAttr ".tk[555]" -type "float3" -0.012620844 0.011285829 -0.038842954 ;
	setAttr ".tk[556]" -type "float3" -6.0859149e-09 -0.011285829 -0.040841896 ;
	setAttr ".tk[557]" -type "float3" -6.0859149e-09 0.011285829 -0.040841896 ;
	setAttr ".tk[558]" -type "float3" -6.3902101e-09 -0.011285829 -0.040841896 ;
	setAttr ".tk[559]" -type "float3" -6.3902101e-09 0.011285829 -0.040841896 ;
	setAttr ".tk[560]" -type "float3" 0.012620836 -0.011285829 -0.038842954 ;
	setAttr ".tk[561]" -type "float3" 0.012620836 0.011285829 -0.038842954 ;
	setAttr ".tk[562]" -type "float3" 0.024006255 -0.011285829 -0.03304179 ;
	setAttr ".tk[563]" -type "float3" 0.024006255 0.011285829 -0.03304179 ;
	setAttr ".tk[564]" -type "float3" 0.024006255 -0.011285829 -0.03304179 ;
	setAttr ".tk[565]" -type "float3" 0.024006255 0.011285829 -0.03304179 ;
	setAttr ".tk[566]" -type "float3" 0.033041783 -0.011285829 -0.024006268 ;
	setAttr ".tk[567]" -type "float3" 0.033041783 0.011285829 -0.024006268 ;
	setAttr ".tk[568]" -type "float3" 0.038842969 -0.011285829 -0.012620846 ;
	setAttr ".tk[569]" -type "float3" 0.038842969 0.011285829 -0.012620846 ;
	setAttr ".tk[570]" -type "float3" 0.040841896 -0.011285829 -7.3030972e-09 ;
	setAttr ".tk[571]" -type "float3" 0.040841896 0.011285829 -7.3030972e-09 ;
	setAttr ".tk[572]" -type "float3" 0.038842969 -0.011285829 0.012620836 ;
	setAttr ".tk[573]" -type "float3" 0.038842969 0.011285829 0.012620836 ;
	setAttr ".tk[574]" -type "float3" 0.033041783 -0.011285829 0.024006255 ;
	setAttr ".tk[575]" -type "float3" 0.033041783 0.011285829 0.024006255 ;
	setAttr ".tk[576]" -type "float3" 0.024006264 -0.011285829 0.033041783 ;
	setAttr ".tk[577]" -type "float3" 0.024006264 0.011285829 0.033041783 ;
	setAttr ".tk[578]" -type "float3" 0.012620844 -0.011285829 0.038842976 ;
	setAttr ".tk[579]" -type "float3" 0.012620844 0.011285829 0.038842976 ;
	setAttr ".tk[580]" -type "float3" -7.3030972e-09 -0.011285829 0.040841896 ;
	setAttr ".tk[581]" -type "float3" -7.3030972e-09 0.011285829 0.040841896 ;
	setAttr ".tk[582]" -type "float3" -0.012620861 -0.011285829 0.038842976 ;
	setAttr ".tk[583]" -type "float3" -0.012620861 0.011285829 0.038842976 ;
	setAttr ".tk[584]" -type "float3" -0.024006277 -0.011285829 0.03304179 ;
	setAttr ".tk[585]" -type "float3" -0.024006277 0.011285829 0.03304179 ;
	setAttr ".tk[586]" -type "float3" -0.033041839 -0.011285829 0.024006264 ;
	setAttr ".tk[587]" -type "float3" -0.033041839 0.011285829 0.024006264 ;
	setAttr ".tk[588]" -type "float3" -0.038842995 -0.011285829 0.012620846 ;
	setAttr ".tk[589]" -type "float3" -0.038842995 0.011285829 0.012620846 ;
	setAttr ".tk[590]" -type "float3" -0.040841896 -0.011285829 -7.3030972e-09 ;
	setAttr ".tk[591]" -type "float3" -0.040841896 0.011285829 -7.3030972e-09 ;
createNode deleteComponent -n "deleteComponent2";
	rename -uid "7F873735-4E9C-78F2-77D1-25946896236B";
	setAttr ".dc" -type "componentList" 21 "e[320:381]" "e[383]" "e[385]" "e[387]" "e[389]" "e[391]" "e[393]" "e[395]" "e[397]" "e[399]" "e[401]" "e[403]" "e[405]" "e[407]" "e[409]" "e[411]" "e[413]" "e[415]" "e[417]" "e[780:789]" "e[1090:1199]";
createNode deleteComponent -n "deleteComponent3";
	rename -uid "B41F24DA-4019-A4D6-1AC4-6FA75647F15F";
	setAttr ".dc" -type "componentList" 0;
createNode objectSet -n "set1";
	rename -uid "8CF47C7F-433F-8516-AD64-CDA71DD96ABA";
	setAttr ".ihi" 0;
	setAttr -s 2 ".dsm";
	setAttr -s 2 ".gn";
createNode groupId -n "groupId1";
	rename -uid "9DFAE100-4142-E0CF-E8FC-DCAC52B34704";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts1";
	rename -uid "3BFEAC61-4986-BD41-E8D4-FDBF36B251E5";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 3 "e[300:342]" "e[703:710]" "e[1011:1049]";
createNode deleteComponent -n "deleteComponent4";
	rename -uid "31083C77-40A7-0FA0-235A-D38EC1D41736";
	setAttr ".dc" -type "componentList" 1 "f[180:181]";
createNode polyBridgeEdge -n "polyBridgeEdge2";
	rename -uid "F31AAB12-4F3F-EAA1-DE31-A08D9DFCAD08";
	setAttr ".ics" -type "componentList" 1 "e[300:339]";
	setAttr ".ix" -type "matrix" 0.65849331484961005 0 0 0 0 2.0949718901283076 0 0 0 0 0.65849331484961005 0
		 0 0 0 1;
	setAttr ".c[0]"  0 1 1;
	setAttr ".dv" 0;
	setAttr ".sv1" 143;
	setAttr ".sv2" 163;
	setAttr ".d" 1;
createNode polySoftEdge -n "polySoftEdge6";
	rename -uid "37733E6D-40E4-5C03-6B48-4A8B9E0C4540";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[1000:1019]";
	setAttr ".ix" -type "matrix" 0.65849331484961005 0 0 0 0 2.0949718901283076 0 0 0 0 0.65849331484961005 0
		 0 0 0 1;
	setAttr ".a" 180;
createNode polySplitRing -n "polySplitRing18";
	rename -uid "A327F32A-4ED2-D624-3FBA-C1A30AD3C57D";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[1000:1019]";
	setAttr ".ix" -type "matrix" 0.65849331484961005 0 0 0 0 2.0949718901283076 0 0 0 0 0.65849331484961005 0
		 0 0 0 1;
	setAttr ".wt" 0.48647722601890564;
	setAttr ".re" 1001;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing19";
	rename -uid "97355676-4866-8BE5-9196-F9B7AD751B77";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 19 "e[1020:1021]" "e[1023]" "e[1025]" "e[1027]" "e[1029]" "e[1031]" "e[1033]" "e[1035]" "e[1037]" "e[1039]" "e[1041]" "e[1043]" "e[1045]" "e[1047]" "e[1049]" "e[1051]" "e[1053]" "e[1055]" "e[1057]";
	setAttr ".ix" -type "matrix" 0.65849331484961005 0 0 0 0 2.0949718901283076 0 0 0 0 0.65849331484961005 0
		 0 0 0 1;
	setAttr ".wt" 0.47583073377609253;
	setAttr ".dr" no;
	setAttr ".re" 1020;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyExtrudeFace -n "polyExtrudeFace8";
	rename -uid "BCF79106-42FB-7C5E-C9F1-63BD7D97D55A";
	setAttr ".ics" -type "componentList" 1 "f[520:539]";
	setAttr ".ix" -type "matrix" 0.65849331484961005 0 0 0 0 2.0949718901283076 0 0 0 0 0.65849331484961005 0
		 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -1.1774778e-07 0.62450379 -1.1774778e-07 ;
	setAttr ".rs" 37201;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.658493628843691 0.58033820231743127 -0.658493628843691 ;
	setAttr ".cbx" -type "double3" 0.65849339334813028 0.66866934396885114 0.65849339334813028 ;
createNode polySoftEdge -n "polySoftEdge7";
	rename -uid "A569D0E9-4328-2BEA-988A-37A51C15DEE6";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 39 "e[1080:1081]" "e[1083:1085]" "e[1087:1088]" "e[1090]" "e[1092:1093]" "e[1095]" "e[1097:1098]" "e[1100]" "e[1102:1103]" "e[1105]" "e[1107:1108]" "e[1110]" "e[1112:1113]" "e[1115]" "e[1117:1118]" "e[1120]" "e[1122:1123]" "e[1125]" "e[1127:1128]" "e[1130]" "e[1132:1133]" "e[1135]" "e[1137:1138]" "e[1140]" "e[1142:1143]" "e[1145]" "e[1147:1148]" "e[1150]" "e[1152:1153]" "e[1155]" "e[1157:1158]" "e[1160]" "e[1162:1163]" "e[1165]" "e[1167:1168]" "e[1170]" "e[1172:1173]" "e[1175]" "e[1177]";
	setAttr ".ix" -type "matrix" 0.65849331484961005 0 0 0 0 2.0949718901283076 0 0 0 0 0.65849331484961005 0
		 0 0 0 1;
	setAttr ".a" 180;
createNode polyTweak -n "polyTweak9";
	rename -uid "AB5D071C-45C1-3CBF-92B9-B6B23827A2F6";
	setAttr ".uopa" yes;
	setAttr -s 40 ".tk[542:581]" -type "float3"  -0.033726387 -0.011878183
		 -0.024503659 -0.039647743 -0.011878183 -0.012882337 -0.033726387 0.011878182 -0.024503659
		 -0.039647743 0.011878182 -0.012882337 -0.0416881 -0.011878183 -7.4544104e-09 -0.0416881
		 0.011878182 -7.4544104e-09 -0.039647795 -0.011878183 0.012882337 -0.039647795 0.011878182
		 0.012882337 -0.033726431 -0.011878183 0.02450365 -0.033726431 0.011878182 0.02450365
		 -0.024503667 -0.011878183 0.033726387 -0.024503667 0.011878182 0.033726387 -0.012882354
		 -0.011878183 0.039647773 -0.012882354 0.011878182 0.039647773 -7.4544104e-09 -0.011878179
		 0.041688077 -7.4544104e-09 0.011878182 0.0416881 0.012882337 -0.011878183 0.039647773
		 0.012882337 0.011878182 0.039647773 0.02450365 -0.011878183 0.033726376 0.02450365
		 0.011878182 0.033726376 0.033726376 -0.011878183 0.024503645 0.033726376 0.011878182
		 0.024503645 0.039647765 -0.011878183 0.012882325 0.039647765 0.011878182 0.012882325
		 0.0416881 -0.011878183 -7.4544104e-09 0.0416881 0.011878182 -7.4544104e-09 0.039647765
		 -0.011878183 -0.012882341 0.039647765 0.011878182 -0.012882341 0.033726376 -0.011878183
		 -0.024503659 0.033726376 0.011878182 -0.024503659 0.024503645 -0.011878183 -0.033726387
		 0.024503645 0.011878182 -0.033726387 0.012882325 -0.011878183 -0.039647743 0.012882325
		 0.011878182 -0.039647743 -6.2120096e-09 -0.011878183 -0.0416881 -6.2120096e-09 0.011878182
		 -0.0416881 -0.012882337 -0.011878183 -0.039647743 -0.012882337 0.011878182 -0.039647743
		 -0.02450365 -0.011878183 -0.033726387 -0.02450365 0.011878182 -0.033726387;
createNode polySplitRing -n "polySplitRing20";
	rename -uid "5475A2B5-4FCF-CA9B-CE17-11B714206555";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[40:59]";
	setAttr ".ix" -type "matrix" 0.65849331484961005 0 0 0 0 2.0949718901283076 0 0 0 0 0.65849331484961005 0
		 0 0 0 1;
	setAttr ".wt" 0.94829583168029785;
	setAttr ".dr" no;
	setAttr ".re" 49;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing21";
	rename -uid "117F054E-421B-D675-5754-E79FA8845033";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[40:59]";
	setAttr ".ix" -type "matrix" 0.65849331484961005 0 0 0 0 2.0949718901283076 0 0 0 0 0.65849331484961005 0
		 0 0 0 1;
	setAttr ".wt" 0.94043153524398804;
	setAttr ".dr" no;
	setAttr ".re" 49;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing22";
	rename -uid "2AD46BBF-4E93-C913-C446-31A6B51ACD90";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[40:59]";
	setAttr ".ix" -type "matrix" 0.65849331484961005 0 0 0 0 2.0949718901283076 0 0 0 0 0.65849331484961005 0
		 0 0 0 1;
	setAttr ".wt" 0.90280330181121826;
	setAttr ".dr" no;
	setAttr ".re" 49;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing23";
	rename -uid "2F66D582-4A58-AA85-9298-34A1C4B7FA6B";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[40:59]";
	setAttr ".ix" -type "matrix" 0.65849331484961005 0 0 0 0 2.0949718901283076 0 0 0 0 0.65849331484961005 0
		 0 0 0 1;
	setAttr ".wt" 0.93104863166809082;
	setAttr ".dr" no;
	setAttr ".re" 49;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing24";
	rename -uid "20B5A396-4AF9-8271-DBF4-78A2E86AF308";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[40:59]";
	setAttr ".ix" -type "matrix" 0.65849331484961005 0 0 0 0 2.0949718901283076 0 0 0 0 0.65849331484961005 0
		 0 0 0 1;
	setAttr ".wt" 0.9254186749458313;
	setAttr ".dr" no;
	setAttr ".re" 49;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing25";
	rename -uid "C7D9C9E3-42A3-BD7C-EF8D-35B5B732EAE7";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[40:59]";
	setAttr ".ix" -type "matrix" 0.65849331484961005 0 0 0 0 2.0949718901283076 0 0 0 0 0.65849331484961005 0
		 0 0 0 1;
	setAttr ".wt" 0.9240342378616333;
	setAttr ".dr" no;
	setAttr ".re" 49;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing26";
	rename -uid "BCBD05A4-4BAA-7FFA-2A8E-048E677BEB59";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[40:59]";
	setAttr ".ix" -type "matrix" 0.65849331484961005 0 0 0 0 2.0949718901283076 0 0 0 0 0.65849331484961005 0
		 0 0 0 1;
	setAttr ".wt" 0.90587431192398071;
	setAttr ".dr" no;
	setAttr ".re" 49;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing27";
	rename -uid "A58D2F09-4FFA-EEA1-826D-7DBF3B3D6DC8";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[40:59]";
	setAttr ".ix" -type "matrix" 0.65849331484961005 0 0 0 0 2.0949718901283076 0 0 0 0 0.65849331484961005 0
		 0 0 0 1;
	setAttr ".wt" 0.90793144702911377;
	setAttr ".dr" no;
	setAttr ".re" 49;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing28";
	rename -uid "265250AA-41DF-2A2E-0732-C7B027CD2F34";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[40:59]";
	setAttr ".ix" -type "matrix" 0.65849331484961005 0 0 0 0 2.0949718901283076 0 0 0 0 0.65849331484961005 0
		 0 0 0 1;
	setAttr ".wt" 0.88700616359710693;
	setAttr ".dr" no;
	setAttr ".re" 49;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing29";
	rename -uid "05D0E8EC-4DD8-8F87-FADA-3CB3507A50F3";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[40:59]";
	setAttr ".ix" -type "matrix" 0.65849331484961005 0 0 0 0 2.0949718901283076 0 0 0 0 0.65849331484961005 0
		 0 0 0 1;
	setAttr ".wt" 0.90527564287185669;
	setAttr ".dr" no;
	setAttr ".re" 49;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing30";
	rename -uid "D37B1EF4-4763-B795-760F-37965492A8D0";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[40:59]";
	setAttr ".ix" -type "matrix" 0.65849331484961005 0 0 0 0 2.0949718901283076 0 0 0 0 0.65849331484961005 0
		 0 0 0 1;
	setAttr ".wt" 0.84845823049545288;
	setAttr ".dr" no;
	setAttr ".re" 49;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing31";
	rename -uid "C783F7FE-418A-8D0F-573E-E39C71B78081";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[40:59]";
	setAttr ".ix" -type "matrix" 0.65849331484961005 0 0 0 0 2.0949718901283076 0 0 0 0 0.65849331484961005 0
		 0 0 0 1;
	setAttr ".wt" 0.8596649169921875;
	setAttr ".dr" no;
	setAttr ".re" 49;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing32";
	rename -uid "DCDA6A77-47E7-5B69-B71E-E0AD42FF3BD5";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[40:59]";
	setAttr ".ix" -type "matrix" 0.65849331484961005 0 0 0 0 2.0949718901283076 0 0 0 0 0.65849331484961005 0
		 0 0 0 1;
	setAttr ".wt" 0.68835252523422241;
	setAttr ".dr" no;
	setAttr ".re" 49;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing33";
	rename -uid "6099AAB6-4DCC-0725-0C7E-59AFB602169E";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[40:59]";
	setAttr ".ix" -type "matrix" 0.65849331484961005 0 0 0 0 2.0949718901283076 0 0 0 0 0.65849331484961005 0
		 0 0 0 1;
	setAttr ".wt" 0.50471895933151245;
	setAttr ".dr" no;
	setAttr ".re" 50;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyExtrudeFace -n "polyExtrudeFace9";
	rename -uid "C779B9F1-4075-3A03-425B-47A40E825E31";
	setAttr ".ics" -type "componentList" 7 "f[300:319]" "f[620:639]" "f[660:679]" "f[700:719]" "f[740:759]" "f[780:799]" "f[820:839]";
	setAttr ".ix" -type "matrix" 0.65849331484961005 0 0 0 0 2.0949718901283076 0 0 0 0 0.65849331484961005 0
		 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -1.1774778e-07 -0.84023815 -1.1774778e-07 ;
	setAttr ".rs" 44982;
	setAttr ".lt" -type "double3" 1.6479873021779667e-17 0 -0.032333257248033868 ;
	setAttr ".ls" -type "double3" 1 0.4777530413229229 1 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.658493628843691 -1.5286348676343064 -0.658493628843691 ;
	setAttr ".cbx" -type "double3" 0.65849339334813028 -0.15184147218686195 0.65849339334813028 ;
createNode polySoftEdge -n "polySoftEdge8";
	rename -uid "AC9EDE7D-4D80-E1BF-F6B7-99B956B559A4";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 273 "e[1600:1601]" "e[1603]" "e[1605]" "e[1608]" "e[1610]" "e[1613]" "e[1615]" "e[1618]" "e[1620]" "e[1623]" "e[1625]" "e[1628]" "e[1630]" "e[1633]" "e[1635]" "e[1638]" "e[1640]" "e[1643]" "e[1645]" "e[1648]" "e[1650]" "e[1653]" "e[1655]" "e[1658]" "e[1660]" "e[1663]" "e[1665]" "e[1668]" "e[1670]" "e[1673]" "e[1675]" "e[1678]" "e[1680]" "e[1683]" "e[1685]" "e[1688]" "e[1690]" "e[1693]" "e[1695]" "e[1700:1701]" "e[1703]" "e[1705]" "e[1708]" "e[1710]" "e[1713]" "e[1715]" "e[1718]" "e[1720]" "e[1723]" "e[1725]" "e[1728]" "e[1730]" "e[1733]" "e[1735]" "e[1738]" "e[1740]" "e[1743]" "e[1745]" "e[1748]" "e[1750]" "e[1753]" "e[1755]" "e[1758]" "e[1760]" "e[1763]" "e[1765]" "e[1768]" "e[1770]" "e[1773]" "e[1775]" "e[1778]" "e[1780]" "e[1783]" "e[1785]" "e[1788]" "e[1790]" "e[1793]" "e[1795]" "e[1800:1801]" "e[1803]" "e[1805]" "e[1808]" "e[1810]" "e[1813]" "e[1815]" "e[1818]" "e[1820]" "e[1823]" "e[1825]" "e[1828]" "e[1830]" "e[1833]" "e[1835]" "e[1838]" "e[1840]" "e[1843]" "e[1845]" "e[1848]" "e[1850]" "e[1853]" "e[1855]" "e[1858]" "e[1860]" "e[1863]" "e[1865]" "e[1868]" "e[1870]" "e[1873]" "e[1875]" "e[1878]" "e[1880]" "e[1883]" "e[1885]" "e[1888]" "e[1890]" "e[1893]" "e[1895]" "e[1900:1901]" "e[1903]" "e[1905]" "e[1908]" "e[1910]" "e[1913]" "e[1915]" "e[1918]" "e[1920]" "e[1923]" "e[1925]" "e[1928]" "e[1930]" "e[1933]" "e[1935]" "e[1938]" "e[1940]" "e[1943]" "e[1945]" "e[1948]" "e[1950]" "e[1953]" "e[1955]" "e[1958]" "e[1960]" "e[1963]" "e[1965]" "e[1968]" "e[1970]" "e[1973]" "e[1975]" "e[1978]" "e[1980]" "e[1983]" "e[1985]" "e[1988]" "e[1990]" "e[1993]" "e[1995]" "e[2000:2001]" "e[2003]" "e[2005]" "e[2008]" "e[2010]" "e[2013]" "e[2015]" "e[2018]" "e[2020]" "e[2023]" "e[2025]" "e[2028]" "e[2030]" "e[2033]" "e[2035]" "e[2038]" "e[2040]" "e[2043]" "e[2045]" "e[2048]" "e[2050]" "e[2053]" "e[2055]" "e[2058]" "e[2060]" "e[2063]" "e[2065]" "e[2068]" "e[2070]" "e[2073]" "e[2075]" "e[2078]" "e[2080]" "e[2083]" "e[2085]" "e[2088]" "e[2090]" "e[2093]" "e[2095]" "e[2100:2101]" "e[2103]" "e[2105]" "e[2108]" "e[2110]" "e[2113]" "e[2115]" "e[2118]" "e[2120]" "e[2123]" "e[2125]" "e[2128]" "e[2130]" "e[2133]" "e[2135]" "e[2138]" "e[2140]" "e[2143]" "e[2145]" "e[2148]" "e[2150]" "e[2153]" "e[2155]" "e[2158]" "e[2160]" "e[2163]" "e[2165]" "e[2168]" "e[2170]" "e[2173]" "e[2175]" "e[2178]" "e[2180]" "e[2183]" "e[2185]" "e[2188]" "e[2190]" "e[2193]" "e[2195]" "e[2200:2201]" "e[2203]" "e[2205]" "e[2208]" "e[2210]" "e[2213]" "e[2215]" "e[2218]" "e[2220]" "e[2223]" "e[2225]" "e[2228]" "e[2230]" "e[2233]" "e[2235]" "e[2238]" "e[2240]" "e[2243]" "e[2245]" "e[2248]" "e[2250]" "e[2253]" "e[2255]" "e[2258]" "e[2260]" "e[2263]" "e[2265]" "e[2268]" "e[2270]" "e[2273]" "e[2275]" "e[2278]" "e[2280]" "e[2283]" "e[2285]" "e[2288]" "e[2290]" "e[2293]" "e[2295]";
	setAttr ".ix" -type "matrix" 0.65849331484961005 0 0 0 0 2.0949718901283076 0 0 0 0 0.65849331484961005 0
		 0 0 0 1;
	setAttr ".a" 180;
createNode polySplitRing -n "polySplitRing34";
	rename -uid "D22497F0-435F-7D03-9BDD-168BCE911707";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 19 "e[1560:1561]" "e[1563]" "e[1565]" "e[1567]" "e[1569]" "e[1571]" "e[1573]" "e[1575]" "e[1577]" "e[1579]" "e[1581]" "e[1583]" "e[1585]" "e[1587]" "e[1589]" "e[1591]" "e[1593]" "e[1595]" "e[1597]";
	setAttr ".ix" -type "matrix" 0.65849331484961005 0 0 0 0 2.0949718901283076 0 0 0 0 0.65849331484961005 0
		 0 0 0 1;
	setAttr ".wt" 0.49167147278785706;
	setAttr ".re" 1560;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak10";
	rename -uid "5D93C635-4288-FADA-24E4-0E85C9F9521B";
	setAttr ".uopa" yes;
	setAttr -s 41 ".tk";
	setAttr ".tk[0]" -type "float3" -0.33147952 0.016945563 0.10770412 ;
	setAttr ".tk[1]" -type "float3" -0.28197333 0.016945563 0.20486529 ;
	setAttr ".tk[2]" -type "float3" -0.20486544 0.016945563 0.28197291 ;
	setAttr ".tk[3]" -type "float3" -0.10770426 0.016945563 0.33147937 ;
	setAttr ".tk[4]" -type "float3" -6.2323352e-08 0.016945563 0.34853768 ;
	setAttr ".tk[5]" -type "float3" 0.10770412 0.016945563 0.33147937 ;
	setAttr ".tk[6]" -type "float3" 0.20486529 0.016945563 0.28197289 ;
	setAttr ".tk[7]" -type "float3" 0.28197289 0.016945563 0.20486523 ;
	setAttr ".tk[8]" -type "float3" 0.33147928 0.016945563 0.10770402 ;
	setAttr ".tk[9]" -type "float3" 0.34853768 0.016945563 -6.2323352e-08 ;
	setAttr ".tk[10]" -type "float3" 0.33147928 0.016945563 -0.10770415 ;
	setAttr ".tk[11]" -type "float3" 0.28197289 0.016945563 -0.20486538 ;
	setAttr ".tk[12]" -type "float3" 0.20486523 0.016945563 -0.28197291 ;
	setAttr ".tk[13]" -type "float3" 0.10770402 0.016945563 -0.33147907 ;
	setAttr ".tk[14]" -type "float3" -5.1936134e-08 0.016945563 -0.34853768 ;
	setAttr ".tk[15]" -type "float3" -0.10770411 0.016945563 -0.33147907 ;
	setAttr ".tk[16]" -type "float3" -0.20486529 0.016945563 -0.28197291 ;
	setAttr ".tk[17]" -type "float3" -0.28197291 0.016945563 -0.20486538 ;
	setAttr ".tk[18]" -type "float3" -0.33147907 0.016945563 -0.10770411 ;
	setAttr ".tk[19]" -type "float3" -0.34853768 0.016945563 -6.2323352e-08 ;
	setAttr ".tk[40]" -type "float3" -5.4559253e-18 0.016945561 -5.4559253e-18 ;
	setAttr ".tk[842]" -type "float3" 0.11394602 0 -0.037023298 ;
	setAttr ".tk[843]" -type "float3" 0.1198099 0 -2.142367e-08 ;
	setAttr ".tk[844]" -type "float3" 0.11394602 0 0.03702325 ;
	setAttr ".tk[845]" -type "float3" 0.096928164 0 0.070422411 ;
	setAttr ".tk[846]" -type "float3" 0.070422448 0 0.096928164 ;
	setAttr ".tk[847]" -type "float3" 0.037023272 0 0.11394604 ;
	setAttr ".tk[848]" -type "float3" -2.142367e-08 0 0.1198099 ;
	setAttr ".tk[849]" -type "float3" -0.037023351 0 0.11394604 ;
	setAttr ".tk[850]" -type "float3" -0.070422485 0 0.096928224 ;
	setAttr ".tk[851]" -type "float3" -0.09692838 0 0.070422448 ;
	setAttr ".tk[852]" -type "float3" -0.1139461 0 0.037023272 ;
	setAttr ".tk[853]" -type "float3" -0.1198099 0 -2.142367e-08 ;
	setAttr ".tk[854]" -type "float3" -0.11394592 0 -0.037023265 ;
	setAttr ".tk[855]" -type "float3" -0.096928194 0 -0.070422433 ;
	setAttr ".tk[856]" -type "float3" -0.070422448 0 -0.096928194 ;
	setAttr ".tk[857]" -type "float3" -0.037023265 0 -0.11394592 ;
	setAttr ".tk[858]" -type "float3" -1.785304e-08 0 -0.1198099 ;
	setAttr ".tk[859]" -type "float3" 0.03702325 0 -0.11394592 ;
	setAttr ".tk[860]" -type "float3" 0.070422411 0 -0.096928194 ;
	setAttr ".tk[861]" -type "float3" 0.096928164 0 -0.070422433 ;
createNode polySplitRing -n "polySplitRing35";
	rename -uid "F785B182-4F2E-52E5-F204-619182416FF8";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 19 "e[220:221]" "e[223]" "e[225]" "e[227]" "e[229]" "e[231]" "e[233]" "e[235]" "e[237]" "e[239]" "e[241]" "e[243]" "e[245]" "e[247]" "e[249]" "e[251]" "e[253]" "e[255]" "e[257]";
	setAttr ".ix" -type "matrix" 0.65849331484961005 0 0 0 0 2.0949718901283076 0 0 0 0 0.65849331484961005 0
		 0 0 0 1;
	setAttr ".wt" 0.31268051266670227;
	setAttr ".re" 233;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak11";
	rename -uid "9423A2E3-439D-07C8-5E83-D4B3B3D049DE";
	setAttr ".uopa" yes;
	setAttr -s 40 ".tk";
	setAttr ".tk[20]" -type "float3" -0.54195213 -3.7252903e-09 0.17609073 ;
	setAttr ".tk[21]" -type "float3" -0.46101207 -3.7252903e-09 0.33494428 ;
	setAttr ".tk[22]" -type "float3" -0.33494461 -3.7252903e-09 0.4610115 ;
	setAttr ".tk[23]" -type "float3" -0.17609096 -3.7252903e-09 0.54195195 ;
	setAttr ".tk[24]" -type "float3" -1.0189558e-07 -3.7252903e-09 0.56984156 ;
	setAttr ".tk[25]" -type "float3" 0.17609073 -3.7252903e-09 0.54195195 ;
	setAttr ".tk[26]" -type "float3" 0.33494428 -3.7252903e-09 0.46101138 ;
	setAttr ".tk[27]" -type "float3" 0.46101138 -3.7252903e-09 0.33494425 ;
	setAttr ".tk[28]" -type "float3" 0.54195189 -3.7252903e-09 0.17609055 ;
	setAttr ".tk[29]" -type "float3" 0.56984156 -3.7252903e-09 -1.0189558e-07 ;
	setAttr ".tk[30]" -type "float3" 0.54195189 -3.7252903e-09 -0.17609076 ;
	setAttr ".tk[31]" -type "float3" 0.46101138 -3.7252903e-09 -0.33494443 ;
	setAttr ".tk[32]" -type "float3" 0.33494425 -3.7252903e-09 -0.4610115 ;
	setAttr ".tk[33]" -type "float3" 0.17609055 -3.7252903e-09 -0.54195148 ;
	setAttr ".tk[34]" -type "float3" -8.4912941e-08 -3.7252903e-09 -0.56984156 ;
	setAttr ".tk[35]" -type "float3" -0.1760907 -3.7252903e-09 -0.54195148 ;
	setAttr ".tk[36]" -type "float3" -0.33494428 -3.7252903e-09 -0.4610115 ;
	setAttr ".tk[37]" -type "float3" -0.4610115 -3.7252903e-09 -0.33494443 ;
	setAttr ".tk[38]" -type "float3" -0.54195148 -3.7252903e-09 -0.1760907 ;
	setAttr ".tk[39]" -type "float3" -0.56984156 -3.7252903e-09 -1.0189558e-07 ;
	setAttr ".tk[1142]" -type "float3" -0.020717457 0 0.0067315092 ;
	setAttr ".tk[1143]" -type "float3" -0.021783605 0 3.8952108e-09 ;
	setAttr ".tk[1144]" -type "float3" -0.020717457 0 -0.0067315018 ;
	setAttr ".tk[1145]" -type "float3" -0.017623305 0 -0.012804079 ;
	setAttr ".tk[1146]" -type "float3" -0.012804083 0 -0.017623305 ;
	setAttr ".tk[1147]" -type "float3" -0.0067315078 0 -0.020717459 ;
	setAttr ".tk[1148]" -type "float3" 3.8952108e-09 0 -0.021783605 ;
	setAttr ".tk[1149]" -type "float3" 0.0067315153 0 -0.020717459 ;
	setAttr ".tk[1150]" -type "float3" 0.012804091 0 -0.017623313 ;
	setAttr ".tk[1151]" -type "float3" 0.017623335 0 -0.012804083 ;
	setAttr ".tk[1152]" -type "float3" 0.020717474 0 -0.0067315078 ;
	setAttr ".tk[1153]" -type "float3" 0.021783605 0 3.8952108e-09 ;
	setAttr ".tk[1154]" -type "float3" 0.020717444 0 0.0067315078 ;
	setAttr ".tk[1155]" -type "float3" 0.017623309 0 0.012804086 ;
	setAttr ".tk[1156]" -type "float3" 0.012804083 0 0.017623309 ;
	setAttr ".tk[1157]" -type "float3" 0.0067315078 0 0.020717444 ;
	setAttr ".tk[1158]" -type "float3" 3.2460092e-09 0 0.021783605 ;
	setAttr ".tk[1159]" -type "float3" -0.0067315018 0 0.020717444 ;
	setAttr ".tk[1160]" -type "float3" -0.012804079 0 0.017623309 ;
	setAttr ".tk[1161]" -type "float3" -0.017623305 0 0.012804086 ;
createNode polySplitRing -n "polySplitRing36";
	rename -uid "D66356A7-48E2-4854-7562-49B9F94A8994";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 19 "e[2340:2341]" "e[2343]" "e[2345]" "e[2347]" "e[2349]" "e[2351]" "e[2353]" "e[2355]" "e[2357]" "e[2359]" "e[2361]" "e[2363]" "e[2365]" "e[2367]" "e[2369]" "e[2371]" "e[2373]" "e[2375]" "e[2377]";
	setAttr ".ix" -type "matrix" 0.65849331484961005 0 0 0 0 2.0949718901283076 0 0 0 0 0.65849331484961005 0
		 0 0 0 1;
	setAttr ".wt" 0.073199115693569183;
	setAttr ".re" 2340;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyExtrudeFace -n "polyExtrudeFace10";
	rename -uid "5105BACF-4F81-8248-D181-2486EF1D36A4";
	setAttr ".ics" -type "componentList" 1 "f[1180:1199]";
	setAttr ".ix" -type "matrix" 0.65849331484961005 0 0 0 0 2.0949718901283076 0 0 0 0 0.65849331484961005 0
		 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -1.0793546e-07 1.8158283 -1.275601e-07 ;
	setAttr ".rs" 33268;
	setAttr ".lt" -type "double3" -3.4694469519536142e-18 -1.3780209612290761e-16 0.062486179041392906 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.28276275781023713 1.8052236634910668 -0.28276279705949725 ;
	setAttr ".cbx" -type "double3" 0.2827625419393065 1.8264329672581627 0.2827625419393065 ;
createNode polyCylinder -n "polyCylinder2";
	rename -uid "E87C5096-47C8-CBE3-5E8C-C6B99EEB2044";
	setAttr ".sc" 1;
	setAttr ".cuv" 3;
createNode polyBevel3 -n "polyBevel1";
	rename -uid "46091D8E-4830-12E6-7EB5-42A544855B05";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[20:39]";
	setAttr ".ix" -type "matrix" 0.32731262195144439 0 0 0 0 0.14656331804042042 0 0
		 0 0 0.32731262195144439 0 0 2.020517943338298 0 1;
	setAttr ".ws" yes;
	setAttr ".oaf" yes;
	setAttr ".f" 0.05;
	setAttr ".sg" 2;
	setAttr ".at" 180;
	setAttr ".sn" yes;
	setAttr ".mv" yes;
	setAttr ".mvt" 0.0001;
	setAttr ".sa" 30;
createNode polySplitRing -n "polySplitRing37";
	rename -uid "E13A3A29-4A63-5CA4-B303-57B59EA4C202";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[120:139]";
	setAttr ".ix" -type "matrix" 0.32731262195144439 0 0 0 0 0.14656331804042042 0 0
		 0 0 0.32731262195144439 0 0 2.020517943338298 0 1;
	setAttr ".wt" 0.27975568175315857;
	setAttr ".re" 130;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyExtrudeFace -n "polyExtrudeFace11";
	rename -uid "9E2542FC-4238-031D-86D8-90BA9D0C82DA";
	setAttr ".ics" -type "componentList" 1 "f[20:39]";
	setAttr ".ix" -type "matrix" 0.32731262195144439 0 0 0 0 0.14656331804042042 0 0
		 0 0 0.32731262195144439 0 0 2.020517943338298 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -3.9018705e-08 1.9129065 -5.8528059e-08 ;
	setAttr ".rs" 36732;
	setAttr ".lt" -type "double3" -2.5153490401663703e-17 -2.2334035003563125e-16 0.010580561770970259 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.32731269998885465 1.8739544855242054 -0.32731277802626491 ;
	setAttr ".cbx" -type "double3" 0.32731262195144439 1.9518585559765653 0.32731266097014949 ;
createNode polyExtrudeFace -n "polyExtrudeFace12";
	rename -uid "B64035D8-4C41-F3CE-E3A1-088CAA7124FE";
	setAttr ".ics" -type "componentList" 21 "f[0:19]" "f[120]" "f[122]" "f[124]" "f[126]" "f[128]" "f[130]" "f[132]" "f[134]" "f[136]" "f[138]" "f[140]" "f[142]" "f[144]" "f[146]" "f[148]" "f[150]" "f[152]" "f[154]" "f[156]" "f[158]";
	setAttr ".ix" -type "matrix" 0.32731262195144439 0 0 0 0 0.14656331804042042 0 0
		 0 0 0.32731262195144439 0 0 2.020517943338298 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -3.9018705e-08 1.8742745 -5.8528059e-08 ;
	setAttr ".rs" 61563;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.33221290309053658 1.8739544855242054 -0.33221298112794684 ;
	setAttr ".cbx" -type "double3" 0.33221282505312633 1.8745945441123637 0.33221286407183148 ;
createNode polyTweak -n "polyTweak12";
	rename -uid "0DC12037-4F4F-5300-1EB1-CAB1313C9F13";
	setAttr ".uopa" yes;
	setAttr -s 40 ".tk[102:141]" -type "float3"  -0.016126707 0.0043671113
		 0.0052398816 -0.013718197 0.0043671113 0.0099668503 -0.013718197 -0.0043671113 0.0099668503
		 -0.016126707 -0.0043671113 0.0052398816 -0.009966854 0.0043671113 0.01371819 -0.009966854
		 -0.0043671113 0.01371819 -0.0052398862 0.0043671113 0.016126702 -0.0052398862 -0.0043671113
		 0.016126702 -1.9004007e-09 0.0043671113 0.016956616 -1.9004007e-09 -0.0043671113
		 0.016956616 0.0052398816 0.0043671113 0.016126698 0.0052398816 -0.0043671113 0.016126698
		 0.0099668484 0.0043671113 0.013718192 0.0099668484 -0.0043671113 0.013718192 0.013718188
		 0.0043671113 0.0099668466 0.013718188 -0.0043671113 0.0099668447 0.0161267 0.0043671113
		 0.0052398806 0.016126696 -0.0043671113 0.0052398806 0.016956616 0.0043671113 -2.9148892e-09
		 0.016956616 -0.0043671113 -2.9148892e-09 0.0161267 0.0043671113 -0.0052398844 0.016126696
		 -0.0043671113 -0.0052398844 0.013718192 0.0043671113 -0.0099668521 0.013718192 -0.0043671113
		 -0.0099668521 0.0099668447 0.0043671113 -0.01371819 0.0099668447 -0.0043671113 -0.01371819
		 0.0052398802 0.0043671113 -0.016126702 0.0052398802 -0.0043671113 -0.016126702 -1.453307e-09
		 0.0043671113 -0.016956616 -1.453307e-09 -0.0043671113 -0.016956614 -0.0052398825
		 0.0043671113 -0.016126698 -0.005239882 -0.0043671113 -0.016126698 -0.0099668484 0.0043671113
		 -0.01371819 -0.0099668484 -0.0043671113 -0.01371819 -0.013718188 0.0043671113 -0.0099668503
		 -0.013718188 -0.0043671113 -0.0099668503 -0.0161267 0.0043671113 -0.0052398834 -0.0161267
		 -0.0043671113 -0.0052398834 -0.016956616 0.0043671113 -3.3824379e-09 -0.016956616
		 -0.0043671113 -3.3824379e-09;
createNode polySoftEdge -n "polySoftEdge9";
	rename -uid "AD0218DA-4AD9-C282-F5C2-FEBCDB257FE5";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 20 "e[240]" "e[243]" "e[245]" "e[247]" "e[249]" "e[251]" "e[253]" "e[255]" "e[257]" "e[259]" "e[261]" "e[263]" "e[265]" "e[267]" "e[269]" "e[271]" "e[273]" "e[275]" "e[277]" "e[279]";
	setAttr ".ix" -type "matrix" 0.32731262195144439 0 0 0 0 0.14656331804042042 0 0
		 0 0 0.32731262195144439 0 0 2.020517943338298 0 1;
	setAttr ".a" 0;
createNode polyTweak -n "polyTweak13";
	rename -uid "E568F1D1-411F-67A7-F965-84B5625A3847";
	setAttr ".uopa" yes;
	setAttr -s 41 ".tk[121:161]" -type "float3"  -0.098902129 1.65871167 0.032135226
		 -0.084131181 1.65871167 0.061124854 -1.2396786e-08 1.65871167 -1.8595175e-08 -0.061124869
		 1.65871167 0.084131137 -0.032135248 1.65871167 0.098902084 -1.2396786e-08 1.65871167
		 0.1039918 0.032135226 1.65871167 0.098902076 0.061124839 1.65871167 0.084131114 0.084131114
		 1.65871167 0.061124831 0.098902047 1.65871167 0.032135211 0.10399178 1.65871167 -1.8595175e-08
		 0.098902047 1.65871167 -0.032135248 0.084131114 1.65871167 -0.061124861 0.061124824
		 1.65871167 -0.084131137 0.032135215 1.65871167 -0.098902084 -9.2975876e-09 1.65871167
		 -0.1039918 -0.032135233 1.65871167 -0.098902076 -0.061124839 1.65871167 -0.084131137
		 -0.084131114 1.65871167 -0.061124858 -0.098902047 1.65871167 -0.032135244 -0.10399178
		 1.65871167 -1.8595175e-08 -0.085390694 -0.00022703409 0.062039945 -0.10038279 -0.00022703409
		 0.032616317 -0.062039968 -0.00022703409 0.085390665 -0.032616351 -0.00022703409 0.10038275
		 -1.182929e-08 -0.00022703409 0.10554867 0.032616328 -0.00022703409 0.10038275 0.062039938
		 -0.00022703409 0.085390642 0.085390657 -0.00022703409 0.062039927 0.10038274 -0.00022703409
		 0.03261631 0.10554864 -0.00022703409 -1.8144108e-08 0.10038274 -0.00022703409 -0.032616347
		 0.085390642 -0.00022703409 -0.06203996 0.062039923 -0.00022703409 -0.085390665 0.032616317
		 -0.00022703409 -0.10038275 -9.0462988e-09 -0.00022703409 -0.10554867 -0.032616332
		 -0.00022703409 -0.10038275 -0.062039938 -0.00022703409 -0.085390665 -0.085390657
		 -0.00022703409 -0.062039945 -0.10038274 -0.00022703409 -0.032616336 -0.10554864 -0.00022703409
		 -2.1054424e-08;
createNode groupId -n "groupId2";
	rename -uid "C610ADEC-4CB7-56CA-437C-158456EBDD9D";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts2";
	rename -uid "AA6F68E5-4EC2-3742-240F-C89980EA41A9";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 21 "f[0:19]" "f[120]" "f[122]" "f[124]" "f[126]" "f[128]" "f[130]" "f[132]" "f[134]" "f[136]" "f[138]" "f[140]" "f[142]" "f[144]" "f[146]" "f[148]" "f[150]" "f[152]" "f[154]" "f[156]" "f[158]";
createNode polyUnite -n "polyUnite1";
	rename -uid "92BD3DFC-47AF-95A3-8D57-D7B56A59EF12";
	setAttr -s 2 ".ip";
	setAttr -s 2 ".im";
createNode groupId -n "groupId3";
	rename -uid "92D232B6-47D1-2BCC-5832-639BE16E5C47";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts3";
	rename -uid "14F4B5ED-4AB2-CC69-AEA6-1C9C76E2EE3A";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:1259]";
createNode groupId -n "groupId4";
	rename -uid "8A3E05F6-44B1-A977-5942-F2828F9A83C2";
	setAttr ".ihi" 0;
createNode groupId -n "groupId5";
	rename -uid "E115DE1B-4A93-F702-E5DB-668FFF918554";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts4";
	rename -uid "3564D306-4F07-4540-7BE5-45BDA673FDD5";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:179]";
createNode groupId -n "groupId6";
	rename -uid "58DF8E2D-4F3E-8FCF-CEB1-6CBCCB0A5A4A";
	setAttr ".ihi" 0;
createNode groupId -n "groupId7";
	rename -uid "8472D2A5-40B1-0053-F4F5-85BB5D399B22";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts5";
	rename -uid "4246ABD6-409E-3891-AC05-DFAFB5812EB9";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "e[300:339]";
createNode groupId -n "groupId8";
	rename -uid "F5DD4409-4FCB-F641-67D7-9C80020FBD47";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts6";
	rename -uid "16699BA3-47A7-997F-5CF0-ABB3D691699E";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:1439]";
createNode groupId -n "groupId9";
	rename -uid "F58BE9AE-4944-76D3-CD6A-A0889E823940";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts7";
	rename -uid "8673307F-405B-6EC7-1434-E0A606D0C97D";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 21 "f[1260:1279]" "f[1380]" "f[1382]" "f[1384]" "f[1386]" "f[1388]" "f[1390]" "f[1392]" "f[1394]" "f[1396]" "f[1398]" "f[1400]" "f[1402]" "f[1404]" "f[1406]" "f[1408]" "f[1410]" "f[1412]" "f[1414]" "f[1416]" "f[1418]";
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr -s 5 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 5 ".gn";
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultRenderGlobals;
	setAttr ".ren" -type "string" "arnold";
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :ikSystem;
	setAttr -s 4 ".sol";
connectAttr "groupId1.id" "pCylinderShape1.iog.og[0].gid";
connectAttr "set1.mwc" "pCylinderShape1.iog.og[0].gco";
connectAttr "groupId3.id" "pCylinderShape1.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "pCylinderShape1.iog.og[1].gco";
connectAttr "groupParts3.og" "pCylinderShape1.i";
connectAttr "groupId4.id" "pCylinderShape1.ciog.cog[0].cgid";
connectAttr ":defaultColorMgtGlobals.cme" "imagePlaneShape1.cme";
connectAttr ":defaultColorMgtGlobals.cfe" "imagePlaneShape1.cmcf";
connectAttr ":defaultColorMgtGlobals.cfp" "imagePlaneShape1.cmcp";
connectAttr ":defaultColorMgtGlobals.wsn" "imagePlaneShape1.ws";
connectAttr ":frontShape.msg" "imagePlaneShape1.ltc";
connectAttr "groupId2.id" "pCylinderShape2.iog.og[0].gid";
connectAttr "groupId5.id" "pCylinderShape2.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "pCylinderShape2.iog.og[1].gco";
connectAttr "groupParts4.og" "pCylinderShape2.i";
connectAttr "groupId6.id" "pCylinderShape2.ciog.cog[0].cgid";
connectAttr "groupParts7.og" "pCylinder3Shape.i";
connectAttr "groupId7.id" "pCylinder3Shape.iog.og[0].gid";
connectAttr "set1.mwc" "pCylinder3Shape.iog.og[0].gco";
connectAttr "groupId8.id" "pCylinder3Shape.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "pCylinder3Shape.iog.og[1].gco";
connectAttr "groupId9.id" "pCylinder3Shape.iog.og[2].gid";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "polyCylinder1.out" "polySplitRing1.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing1.mp";
connectAttr "polySplitRing1.out" "polySplitRing2.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing2.mp";
connectAttr "polySplitRing2.out" "polySplitRing3.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing3.mp";
connectAttr "polySplitRing3.out" "polySplitRing4.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing4.mp";
connectAttr "polySplitRing4.out" "polySplitRing5.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing5.mp";
connectAttr "polySplitRing5.out" "polySplitRing6.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing6.mp";
connectAttr "polySplitRing6.out" "polySplitRing7.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing7.mp";
connectAttr "polySplitRing7.out" "polySplitRing8.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing8.mp";
connectAttr "polySplitRing8.out" "polySplitRing9.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing9.mp";
connectAttr "polySplitRing9.out" "polySplitRing10.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing10.mp";
connectAttr "polySplitRing10.out" "polySplitRing11.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing11.mp";
connectAttr "polySplitRing11.out" "polySplitRing12.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing12.mp";
connectAttr "polySplitRing12.out" "polySplitRing13.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing13.mp";
connectAttr "polySplitRing13.out" "polySplitRing14.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing14.mp";
connectAttr "polySplitRing14.out" "polySplitRing15.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing15.mp";
connectAttr "polySplitRing15.out" "polySplitRing16.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing16.mp";
connectAttr "polyTweak1.out" "polySplitRing17.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing17.mp";
connectAttr "polySplitRing16.out" "polyTweak1.ip";
connectAttr "polyTweak2.out" "polyExtrudeFace1.ip";
connectAttr "pCylinderShape1.wm" "polyExtrudeFace1.mp";
connectAttr "polySplitRing17.out" "polyTweak2.ip";
connectAttr "polyTweak3.out" "polyExtrudeFace2.ip";
connectAttr "pCylinderShape1.wm" "polyExtrudeFace2.mp";
connectAttr "polyExtrudeFace1.out" "polyTweak3.ip";
connectAttr "polyExtrudeFace2.out" "polyExtrudeFace3.ip";
connectAttr "pCylinderShape1.wm" "polyExtrudeFace3.mp";
connectAttr "polyTweak4.out" "polyExtrudeFace4.ip";
connectAttr "pCylinderShape1.wm" "polyExtrudeFace4.mp";
connectAttr "polyExtrudeFace3.out" "polyTweak4.ip";
connectAttr "polyTweak5.out" "polyExtrudeFace5.ip";
connectAttr "pCylinderShape1.wm" "polyExtrudeFace5.mp";
connectAttr "polyExtrudeFace4.out" "polyTweak5.ip";
connectAttr "polyTweak6.out" "polyExtrudeFace6.ip";
connectAttr "pCylinderShape1.wm" "polyExtrudeFace6.mp";
connectAttr "polyExtrudeFace5.out" "polyTweak6.ip";
connectAttr "polyTweak7.out" "polySoftEdge1.ip";
connectAttr "pCylinderShape1.wm" "polySoftEdge1.mp";
connectAttr "polyExtrudeFace6.out" "polyTweak7.ip";
connectAttr "polySoftEdge1.out" "polySoftEdge2.ip";
connectAttr "pCylinderShape1.wm" "polySoftEdge2.mp";
connectAttr "polySoftEdge2.out" "polySoftEdge3.ip";
connectAttr "pCylinderShape1.wm" "polySoftEdge3.mp";
connectAttr "polySoftEdge3.out" "deleteComponent1.ig";
connectAttr "deleteComponent1.og" "polyBridgeEdge1.ip";
connectAttr "pCylinderShape1.wm" "polyBridgeEdge1.mp";
connectAttr "polyBridgeEdge1.out" "polySoftEdge4.ip";
connectAttr "pCylinderShape1.wm" "polySoftEdge4.mp";
connectAttr "polySoftEdge4.out" "polyExtrudeFace7.ip";
connectAttr "pCylinderShape1.wm" "polyExtrudeFace7.mp";
connectAttr "polyTweak8.out" "polySoftEdge5.ip";
connectAttr "pCylinderShape1.wm" "polySoftEdge5.mp";
connectAttr "polyExtrudeFace7.out" "polyTweak8.ip";
connectAttr "polySoftEdge5.out" "deleteComponent2.ig";
connectAttr "deleteComponent2.og" "deleteComponent3.ig";
connectAttr "groupId1.msg" "set1.gn" -na;
connectAttr "groupId7.msg" "set1.gn" -na;
connectAttr "pCylinderShape1.iog.og[0]" "set1.dsm" -na;
connectAttr "pCylinder3Shape.iog.og[0]" "set1.dsm" -na;
connectAttr "deleteComponent3.og" "groupParts1.ig";
connectAttr "groupId1.id" "groupParts1.gi";
connectAttr "groupParts1.og" "deleteComponent4.ig";
connectAttr "deleteComponent4.og" "polyBridgeEdge2.ip";
connectAttr "pCylinderShape1.wm" "polyBridgeEdge2.mp";
connectAttr "polyBridgeEdge2.out" "polySoftEdge6.ip";
connectAttr "pCylinderShape1.wm" "polySoftEdge6.mp";
connectAttr "polySoftEdge6.out" "polySplitRing18.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing18.mp";
connectAttr "polySplitRing18.out" "polySplitRing19.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing19.mp";
connectAttr "polySplitRing19.out" "polyExtrudeFace8.ip";
connectAttr "pCylinderShape1.wm" "polyExtrudeFace8.mp";
connectAttr "polyTweak9.out" "polySoftEdge7.ip";
connectAttr "pCylinderShape1.wm" "polySoftEdge7.mp";
connectAttr "polyExtrudeFace8.out" "polyTweak9.ip";
connectAttr "polySoftEdge7.out" "polySplitRing20.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing20.mp";
connectAttr "polySplitRing20.out" "polySplitRing21.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing21.mp";
connectAttr "polySplitRing21.out" "polySplitRing22.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing22.mp";
connectAttr "polySplitRing22.out" "polySplitRing23.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing23.mp";
connectAttr "polySplitRing23.out" "polySplitRing24.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing24.mp";
connectAttr "polySplitRing24.out" "polySplitRing25.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing25.mp";
connectAttr "polySplitRing25.out" "polySplitRing26.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing26.mp";
connectAttr "polySplitRing26.out" "polySplitRing27.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing27.mp";
connectAttr "polySplitRing27.out" "polySplitRing28.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing28.mp";
connectAttr "polySplitRing28.out" "polySplitRing29.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing29.mp";
connectAttr "polySplitRing29.out" "polySplitRing30.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing30.mp";
connectAttr "polySplitRing30.out" "polySplitRing31.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing31.mp";
connectAttr "polySplitRing31.out" "polySplitRing32.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing32.mp";
connectAttr "polySplitRing32.out" "polySplitRing33.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing33.mp";
connectAttr "polySplitRing33.out" "polyExtrudeFace9.ip";
connectAttr "pCylinderShape1.wm" "polyExtrudeFace9.mp";
connectAttr "polyExtrudeFace9.out" "polySoftEdge8.ip";
connectAttr "pCylinderShape1.wm" "polySoftEdge8.mp";
connectAttr "polyTweak10.out" "polySplitRing34.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing34.mp";
connectAttr "polySoftEdge8.out" "polyTweak10.ip";
connectAttr "polyTweak11.out" "polySplitRing35.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing35.mp";
connectAttr "polySplitRing34.out" "polyTweak11.ip";
connectAttr "polySplitRing35.out" "polySplitRing36.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing36.mp";
connectAttr "polySplitRing36.out" "polyExtrudeFace10.ip";
connectAttr "pCylinderShape1.wm" "polyExtrudeFace10.mp";
connectAttr "polyCylinder2.out" "polyBevel1.ip";
connectAttr "pCylinderShape2.wm" "polyBevel1.mp";
connectAttr "polyBevel1.out" "polySplitRing37.ip";
connectAttr "pCylinderShape2.wm" "polySplitRing37.mp";
connectAttr "polySplitRing37.out" "polyExtrudeFace11.ip";
connectAttr "pCylinderShape2.wm" "polyExtrudeFace11.mp";
connectAttr "polyTweak12.out" "polyExtrudeFace12.ip";
connectAttr "pCylinderShape2.wm" "polyExtrudeFace12.mp";
connectAttr "polyExtrudeFace11.out" "polyTweak12.ip";
connectAttr "polyTweak13.out" "polySoftEdge9.ip";
connectAttr "pCylinderShape2.wm" "polySoftEdge9.mp";
connectAttr "polyExtrudeFace12.out" "polyTweak13.ip";
connectAttr "polySoftEdge9.out" "groupParts2.ig";
connectAttr "groupId2.id" "groupParts2.gi";
connectAttr "pCylinderShape1.o" "polyUnite1.ip[0]";
connectAttr "pCylinderShape2.o" "polyUnite1.ip[1]";
connectAttr "pCylinderShape1.wm" "polyUnite1.im[0]";
connectAttr "pCylinderShape2.wm" "polyUnite1.im[1]";
connectAttr "polyExtrudeFace10.out" "groupParts3.ig";
connectAttr "groupId3.id" "groupParts3.gi";
connectAttr "groupParts2.og" "groupParts4.ig";
connectAttr "groupId5.id" "groupParts4.gi";
connectAttr "polyUnite1.out" "groupParts5.ig";
connectAttr "groupId7.id" "groupParts5.gi";
connectAttr "groupParts5.og" "groupParts6.ig";
connectAttr "groupId8.id" "groupParts6.gi";
connectAttr "groupParts6.og" "groupParts7.ig";
connectAttr "groupId9.id" "groupParts7.gi";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "pCylinderShape1.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinderShape1.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinderShape2.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinderShape2.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinder3Shape.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "groupId3.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId4.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId5.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId6.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId8.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId2.msg" ":defaultLastHiddenSet.gn" -na;
connectAttr "groupId9.msg" ":defaultLastHiddenSet.gn" -na;
connectAttr "pCylinderShape2.iog.og[0]" ":defaultLastHiddenSet.dsm" -na;
connectAttr "pCylinder3Shape.iog.og[2]" ":defaultLastHiddenSet.dsm" -na;
// End of PlasticWaterBottle.ma
