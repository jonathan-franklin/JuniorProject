node = hou.pwd()
geo = node.geometry()


# Add code to modify contained geometries.
# Use drop down menu to select examples.

import csv
import os
import time
from datetime import date
import math

COLUMNS = 'buoy,month,day,year,latitude,longitude'.split(',')
# Ignore the first three columns, will be fed into the point position.
for col in COLUMNS[:4]:
  geo.addAttrib(hou.attribType.Point, col, 0.0)
geo.addAttrib(hou.attribType.Point, 'norDate', 0)

def main():
  maxrows = node.parm('maxRows').eval()
  directory = node.parm('dataDirectory').eval()

  files = []
  for name in os.listdir(directory):
    if name.endswith('.csv'):
      files.append(os.path.join(directory, name))
  files.sort()

  count = 0
  with hou.InterruptableOperation('Reading CSV', 'Reading Files', True) as op:
    for fi, filename in enumerate(files):
      #op.updateLongProgress(fi / float(len(files)-1), os.path.basename(filename))
      if count >= maxrows:
        break
      with open(filename) as fp:
        count += read_csv(fp, maxrows - count)
        
def read_csv(fp, maxrows):
  reader = csv.reader(fp)
  header = {name: index for index, name in enumerate(next(reader))}
  col_indices = [header[name] for name in COLUMNS]

  count = 0
  for row in reader:
    if count >= maxrows:
      break
    if len(row) != len(header):
      # There's an empty line after the header in the data. We also don't
      # want to be screwed over by lines that have less columns than the
      # header specifies.
      continue
    
    count += 1
    point = geo.createPoint()
    data = [float(row[index]) for index in col_indices]

    # Use the first three values as point position.
    point.setPosition(hou.Vector3(*data[3:]))

    # Transfer the rest to geometry attributes.
    for name, value in zip(COLUMNS[:4], data[:4]):
      point.setAttribValue(name, value)
    
    # Date values.
    day = data[2]
    year = data[3]
    month = data[1]

    # Converting date values to integers.
    day = math.floor(day)
    year = math.floor(year)
    month = math.floor(month)
    day = int(day)
    year = int(year)
    month = int(month)
    
    # Converting dates to number of days from start of data.
    d0 = date(1979, 2, 15)
    d1 = date(year, month, day)
    delta = d1 - d0
    days = delta.days
    point.setAttribValue('norDate', days)
  
  return count

# Put this at the very end of your node's code.
main()