//Maya ASCII 2018 scene
//Name: Bluefin_Tuna.ma
//Last modified: Tue, Dec 04, 2018 10:05:32 AM
//Codeset: 1252
requires maya "2018";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2018";
fileInfo "version" "2018";
fileInfo "cutIdentifier" "201706261615-f9658c4cfc";
fileInfo "osv" "Microsoft Windows 8 Enterprise Edition, 64-bit  (Build 9200)\n";
fileInfo "license" "student";
createNode transform -s -n "persp";
	rename -uid "E6EF66F4-43FF-B0B0-B9C2-E086E44C7B6D";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0.062378427776634382 5.0063443650221853 -5.821606586013047 ;
	setAttr ".r" -type "double3" -35.138352729554974 170.19999999998922 0 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "85CBC8FB-4F50-1764-5014-FB94491340CE";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 3.9374148906407074;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
	setAttr ".ai_translator" -type "string" "perspective";
createNode transform -s -n "top";
	rename -uid "E0F85246-411F-3FCD-59EA-7E8E62CF6363";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -0.8758208947333439 1000.1 2.7921448808202904 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "9B4968A5-44B1-4DE3-81DF-D3BD187D9252";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 34.426832331606313;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "front";
	rename -uid "C6F0B856-448C-4651-DCFC-169E60CBFA8E";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -0.1836523157786088 -2.5159140965046078 1000.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "8DAB1602-461D-DAD9-9BEA-2EB3F6B1573E";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 1.3641096199242537;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "side";
	rename -uid "4162D9C6-4B25-15D4-74E9-27AAF9F67781";
	setAttr ".t" -type "double3" 1000.1 -0.98156062555817591 -0.94137942987173284 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "A1C0DED4-4594-B00F-656D-19A2371BD829";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 1025.2217987388681;
	setAttr ".ow" 19.689621553815016;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".tp" -type "double3" -25.121798738867938 -0.99965372694264854 -4.2036761699922716 ;
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -n "pCube1";
	rename -uid "026F66A6-4B25-BC0B-A4E7-469EA4C6E74F";
	setAttr ".t" -type "double3" 0 0 0.084883071105291502 ;
	setAttr ".s" -type "double3" 4.3541510445179226 4.3541510445179226 15.384666766119331 ;
createNode mesh -n "pCubeShape1" -p "pCube1";
	rename -uid "9BBCC260-4452-6716-EB4C-F3A9CCCF9DB9";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.082078292965888977 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "imagePlane1";
	rename -uid "DB4A890B-4B1C-51B6-A483-ACB5CDEC8F0A";
	setAttr ".t" -type "double3" -25.121798738867973 0 -5.5781598759780888e-15 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode imagePlane -n "imagePlaneShape1" -p "imagePlane1";
	rename -uid "B1CAAB31-4DA7-EC62-A696-D99FFCAEB0F4";
	setAttr -k off ".v";
	setAttr ".fc" 204;
	setAttr ".imn" -type "string" "C:/Users/10795516/Git Repos/JuniorProject/Layout/09 - Mid-Ocean/References/Bluefin-big.jpg";
	setAttr ".cov" -type "short2" 1728 1140 ;
	setAttr ".dic" yes;
	setAttr ".ag" 0.12337662302586552;
	setAttr ".dlc" no;
	setAttr ".w" 17.28;
	setAttr ".h" 11.4;
	setAttr ".cs" -type "string" "sRGB";
createNode transform -n "pCube2";
	rename -uid "123D2F3C-48C2-ACF0-C290-87AB90DFE990";
	setAttr ".t" -type "double3" 0 2.2563016321647891 2.8101931747067881 ;
	setAttr ".s" -type "double3" 0.054791416844971783 1 1 ;
createNode mesh -n "pCubeShape2" -p "pCube2";
	rename -uid "E75FBFBA-4B8D-9663-9B8F-0F8B8E190417";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5000000074505806 0.39614701271057129 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 30 ".pt";
	setAttr ".pt[6]" -type "float3" 0 -0.085933447 -0.01273088 ;
	setAttr ".pt[7]" -type "float3" 0 -0.085933447 -0.01273088 ;
	setAttr ".pt[14]" -type "float3" 0 -0.022279041 -0.0031827202 ;
	setAttr ".pt[15]" -type "float3" 0 -0.022279041 -0.0031827202 ;
	setAttr ".pt[60]" -type "float3" 0 -0.087102912 0.030486019 ;
	setAttr ".pt[61]" -type "float3" 0 -0.087102912 0.030486019 ;
	setAttr ".pt[64]" -type "float3" 0 -0.094361492 0.055165179 ;
	setAttr ".pt[65]" -type "float3" 0 -0.094361492 0.055165179 ;
	setAttr ".pt[68]" -type "float3" 0 -0.11033036 0.029034296 ;
	setAttr ".pt[69]" -type "float3" 0 -0.11033036 0.029034296 ;
	setAttr ".pt[72]" -type "float3" 0 -0.078392625 0.018872298 ;
	setAttr ".pt[73]" -type "float3" 0 -0.078392625 0.018872298 ;
	setAttr ".pt[76]" -type "float3" 0 -0.053713467 0.0014517149 ;
	setAttr ".pt[77]" -type "float3" 0 -0.053713467 0.0014517149 ;
	setAttr ".pt[80]" -type "float3" 0 -0.029034305 0.029034305 ;
	setAttr ".pt[81]" -type "float3" 0 -0.029034305 0.029034305 ;
	setAttr ".pt[84]" -type "float3" 0 -0.034841161 0.013065435 ;
	setAttr ".pt[85]" -type "float3" 0 -0.034841161 0.013065435 ;
	setAttr ".pt[88]" -type "float3" 0 -0.011613719 0.045003172 ;
	setAttr ".pt[89]" -type "float3" 0 -0.011613719 0.045003172 ;
	setAttr ".pt[92]" -type "float3" 0 -0.02758259 0.037744593 ;
	setAttr ".pt[93]" -type "float3" 0 -0.02758259 0.037744593 ;
	setAttr ".pt[104]" -type "float3" 0 -0.043551456 1.1641532e-10 ;
	setAttr ".pt[105]" -type "float3" 0 -0.043551456 1.1641532e-10 ;
	setAttr ".pt[106]" -type "float3" 0 -0.0063654403 0 ;
	setAttr ".pt[107]" -type "float3" 0 -0.0063654403 0 ;
	setAttr ".pt[110]" -type "float3" 0 -0.038192641 -0.0095481602 ;
	setAttr ".pt[111]" -type "float3" 0 -0.038192641 -0.0095481602 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube3";
	rename -uid "E1A0B5FC-42F8-780C-D4C6-67AD052B92DB";
	setAttr ".t" -type "double3" 1.7337253692175811 -0.18582301530098655 2.0975388498019512 ;
	setAttr ".s" -type "double3" 0.087683040107048332 1 1 ;
createNode mesh -n "pCubeShape3" -p "pCube3";
	rename -uid "7CF68970-414C-9482-50C9-10B3D12BC7F4";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 24 ".pt[0:23]" -type "float3"  -1.3598065 -0.0014382924 
		8.9406967e-08 -1.3929045 0.020933853 8.9406967e-08 -3.0738072 -0.020933868 8.9406967e-08 
		-3.1069047 0.0014382884 8.9406967e-08 0 -3.7252903e-09 8.9406967e-08 0 -3.7252903e-09 
		8.9406967e-08 -0.01697981 0.0098055536 0.01208395 -0.016980853 0.0099322284 0.01208395 
		-2.7682693 0.0060334937 8.9406967e-08 -2.7502844 -0.010521329 8.9406967e-08 -1.3438486 
		0.00010583803 8.9406967e-08 -1.3692687 0.019750262 8.9406967e-08 -1.5662149 0.0091961371 
		8.9406967e-08 -1.5620564 0.0012076175 8.9406967e-08 -0.90065128 0.0042999154 8.9406967e-08 
		-0.90665883 0.013896636 8.9406967e-08 -0.55426621 0.0049356874 8.9406967e-08 -0.55375898 
		0.0021431083 8.9406967e-08 -0.2584976 0.0020329002 8.9406967e-08 -0.25880972 0.0042235474 
		8.9406967e-08 0 -0.094478257 -0.45676979 0 -0.094478257 -0.45676979 0 0.26848584 
		-0.45676979 0 0.26848584 -0.45676979;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube4";
	rename -uid "37D3EB63-4391-8D3C-7DB5-369C569E1B6D";
	setAttr ".t" -type "double3" 0 -2.6813777023466527 2.8016959325801558 ;
	setAttr ".s" -type "double3" 0.03333330020876149 1 1 ;
createNode mesh -n "pCubeShape4" -p "pCube4";
	rename -uid "5A5A82AF-4291-FE59-6D1F-F98762EC805C";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.19359087944030762 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 2 ".pt[14:15]" -type "float3"  0 0.035677131 -0.030580398 
		0 0.035677131 -0.030580398;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube5";
	rename -uid "A4B80484-4FB3-AB11-D2C1-24BDB8CDB99D";
	setAttr ".t" -type "double3" 0 -2.0776299056160927 -1.48809104418961 ;
	setAttr ".s" -type "double3" 0.03333330020876149 1 1 ;
createNode mesh -n "pCubeShape5" -p "pCube5";
	rename -uid "FBE05452-4D12-6903-954A-CF969F004981";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt";
	setAttr ".pt[6]" -type "float3" 0 -0.043455534 -0.055043675 ;
	setAttr ".pt[7]" -type "float3" 0 -0.043455534 -0.055043675 ;
	setAttr ".pt[24]" -type "float3" 0 -0.19314368 -0.52294183 ;
	setAttr ".pt[25]" -type "float3" 0 -0.19314368 -0.52294183 ;
	setAttr ".pt[26]" -type "float3" 0 -0.1272171 -0.52294183 ;
	setAttr ".pt[27]" -type "float3" 0 -0.1272171 -0.52294183 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pCube5";
	rename -uid "454F934D-45FF-9456-EEF8-18A8727AFCFD";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 26 ".uvst[0].uvsp[0:25]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.625 0.67984933 0.875 0.070150673 0.125 0.070150673
		 0.375 0.67984933 0.375 0.070150673 0.625 0.070150673 0.625 0.55640912 0.875 0.19359088
		 0.125 0.19359088 0.375 0.55640912 0.375 0.19359088 0.625 0.19359088;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 14 ".pt";
	setAttr ".pt[0]" -type "float3" 0 0.65485507 -0.10364614 ;
	setAttr ".pt[1]" -type "float3" 0 0.65485507 -0.10364614 ;
	setAttr ".pt[4]" -type "float3" 0 0.27796009 -0.35333908 ;
	setAttr ".pt[5]" -type "float3" 0 0.27796009 -0.35333908 ;
	setAttr ".pt[6]" -type "float3" 0 -0.13191327 0.16489159 ;
	setAttr ".pt[7]" -type "float3" 0 -0.13191327 0.16489159 ;
	setAttr ".pt[8]" -type "float3" 0 0 0.35805026 ;
	setAttr ".pt[9]" -type "float3" 0 0 0.35805026 ;
	setAttr ".pt[10]" -type "float3" 0 0.56534261 -9.3132257e-10 ;
	setAttr ".pt[11]" -type "float3" 0 0.56534261 -9.3132257e-10 ;
	setAttr ".pt[12]" -type "float3" 0 0.2214258 -0.39102861 ;
	setAttr ".pt[13]" -type "float3" 0 0.2214258 -0.39102861 ;
	setAttr ".pt[14]" -type "float3" 0 0.0875002 -0.10595943 ;
	setAttr ".pt[15]" -type "float3" 0 0.0875002 -0.10595943 ;
	setAttr -s 16 ".vt[0:15]"  -0.5 -1.084172726 -0.37087685 0.5 -1.084172726 -0.37087685
		 -0.5 0.30632415 0.54652816 0.5 0.30632415 0.54652816 -0.5 0.30632415 0.024305999
		 0.5 0.30632415 0.024305999 -0.5 -0.12598681 -0.73348546 0.5 -0.12598681 -0.73348546
		 0.5 0.163513 -0.66865242 -0.5 0.163513 -0.66865242 -0.5 -0.70928574 -0.036999531
		 0.5 -0.70928574 -0.036999531 0.5 0.26153192 -0.19303802 -0.5 0.26153192 -0.19303802
		 -0.5 -0.012218386 0.3635067 0.5 -0.012218386 0.3635067;
	setAttr -s 28 ".ed[0:27]"  0 1 0 2 3 0 4 5 0 6 7 0 0 10 0 1 11 0 2 4 0
		 3 5 0 4 13 0 5 12 0 6 0 0 7 1 0 8 7 0 9 6 0 8 9 1 10 14 0 9 10 1 11 15 0 10 11 1
		 11 8 1 12 8 0 13 9 0 12 13 1 14 2 0 13 14 1 15 3 0 14 15 1 15 12 1;
	setAttr -s 14 -ch 56 ".fc[0:13]" -type "polyFaces" 
		f 4 26 25 -2 -24
		mu 0 4 24 25 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 22 -9
		mu 0 4 4 5 20 23
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 27 -10 -8 -26
		mu 0 4 25 21 11 3
		f 4 24 23 6 8
		mu 0 4 22 24 2 13
		f 4 -15 12 -4 -14
		mu 0 4 17 14 7 6
		f 4 10 4 -17 13
		mu 0 4 12 0 18 16
		f 4 0 5 -19 -5
		mu 0 4 0 1 19 18
		f 4 -12 -13 -20 -6
		mu 0 4 1 10 15 19
		f 4 -23 20 14 -22
		mu 0 4 23 20 14 17
		f 4 16 15 -25 21
		mu 0 4 16 18 24 22
		f 4 18 17 -27 -16
		mu 0 4 18 19 25 24
		f 4 19 -21 -28 -18
		mu 0 4 19 15 21 25;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube6";
	rename -uid "0467CB9B-4480-C518-001B-679506602AC3";
	setAttr ".t" -type "double3" 0 1.9204837397134784 -0.99943270976044019 ;
	setAttr ".r" -type "double3" 0 0 180 ;
	setAttr ".s" -type "double3" 0.03333330020876149 1 1 ;
createNode mesh -n "pCubeShape6" -p "pCube6";
	rename -uid "45246F36-4F37-F500-4A9C-C19DDD32F000";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.37499998323619366 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 40 ".uvst[0].uvsp[0:39]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.625 0.67984933 0.875 0.070150673 0.125 0.070150673
		 0.375 0.67984933 0.375 0.070150673 0.625 0.070150673 0.625 0.55640912 0.875 0.19359088
		 0.125 0.19359088 0.375 0.55640912 0.375 0.19359088 0.625 0.19359088 0.625 0.71561909
		 0.875 0.034380879 0.125 0.034380879 0.375 0.71561909 0.375 0.034380879 0.625 0.034380879
		 0.375 0.75 0.625 0.75 0.625 1 0.375 1 0.375 0.75 0.625 0.75 0.625 1 0.375 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 28 ".pt[0:27]" -type "float3"  2.4424907e-15 -0.56003386 
		0.067473911 -2.4424907e-15 -0.56003386 0.067473911 -1.110223e-16 -0.060726527 0.25640091 
		1.110223e-16 -0.060726527 0.25640091 0 -0.01349478 -0.22266397 0 -0.01349478 -0.22266397 
		1.9984014e-15 -0.59674156 0.025925038 -1.9984014e-15 -0.59674156 0.025925038 -6.6613381e-16 
		-0.35761186 9.3132257e-10 6.6613381e-16 -0.35761186 9.3132257e-10 5.5511151e-16 -0.32387486 
		0.18217961 -5.5511151e-16 -0.32387486 0.18217961 -3.3306691e-16 -0.10121088 -0.08771611 
		3.3306691e-16 -0.10121088 -0.08771611 9.9920072e-16 -0.26989567 0.15519005 -9.9920072e-16 
		-0.26989567 0.15519005 -9.9920072e-16 -0.42350295 0.019389359 9.9920072e-16 -0.42350295 
		0.019389359 1.2212453e-15 -0.39809617 0.15519002 -1.2212453e-15 -0.39809617 0.15519002 
		1.6653345e-15 -0.61401266 0.11470568 -1.6653345e-15 -0.61401266 0.11470568 -2.3314684e-15 
		-0.66124427 0.060726527 2.3314684e-15 -0.66124427 0.060726527 1.8873791e-15 -0.86113536 
		-0.2125617 -1.8873791e-15 -0.86113536 -0.2125617 -2.220446e-15 -0.85593534 -0.30027786 
		2.220446e-15 -0.85593534 -0.30027786;
	setAttr -s 28 ".vt[0:27]"  -0.50000006 -0.41047287 -0.47923422 0.50000006 -0.41047287 -0.47923422
		 -0.50000006 0.30632412 0.54652816 0.50000006 0.30632412 0.54652816 -0.50000006 0.58428431 -0.32903314
		 0.50000006 0.58428431 -0.32903314 -0.50000006 -0.2579 -0.56859398 0.50000006 -0.2579 -0.56859398
		 0.50000006 0.16351295 -0.31060219 -0.50000006 0.16351295 -0.31060219 -0.50000006 -0.14394307 -0.036999583
		 0.50000006 -0.14394307 -0.036999583 0.50000006 0.48295772 -0.58406663 -0.50000006 0.48295772 -0.58406663
		 -0.50000006 0.075281858 0.25754726 0.50000006 0.075281858 0.25754726 0.50000006 -0.10318828 -0.37148416
		 -0.50000006 -0.10318828 -0.37148416 -0.50000006 -0.29887795 -0.2600925 0.50000006 -0.29887795 -0.2600925
		 -0.50000006 -0.48686528 -0.90910792 0.50000006 -0.48686528 -0.90910792 0.50000006 -0.57162809 -0.81974816
		 -0.50000006 -0.57162809 -0.81974816 -0.50000006 -0.48686528 -0.90910792 0.50000006 -0.48686528 -0.90910792
		 0.50000006 -0.57162809 -0.81974816 -0.50000006 -0.57162809 -0.81974816;
	setAttr -s 52 ".ed[0:51]"  0 1 1 2 3 0 4 5 0 6 7 1 0 18 0 1 19 0 2 4 0
		 3 5 0 4 13 0 5 12 0 6 0 1 7 1 1 8 16 0 9 17 0 8 9 1 10 14 0 9 10 1 11 15 0 10 11 1
		 11 8 1 12 8 0 13 9 0 12 13 1 14 2 0 13 14 1 15 3 0 14 15 1 15 12 1 16 7 0 17 6 0
		 16 17 1 18 10 0 17 18 1 19 11 0 18 19 1 19 16 1 6 20 0 7 21 0 20 21 0 1 22 0 21 22 0
		 0 23 0 23 22 0 20 23 0 20 24 0 21 25 0 24 25 0 22 26 0 25 26 0 23 27 0 27 26 0 24 27 0;
	setAttr -s 26 -ch 104 ".fc[0:25]" -type "polyFaces" 
		f 4 26 25 -2 -24
		mu 0 4 24 25 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 22 -9
		mu 0 4 4 5 20 23
		f 4 46 48 -51 -52
		mu 0 4 36 37 38 39
		f 4 27 -10 -8 -26
		mu 0 4 25 21 11 3
		f 4 24 23 6 8
		mu 0 4 22 24 2 13
		f 4 -15 12 30 -14
		mu 0 4 17 14 26 29
		f 4 32 31 -17 13
		mu 0 4 28 30 18 16
		f 4 34 33 -19 -32
		mu 0 4 30 31 19 18
		f 4 35 -13 -20 -34
		mu 0 4 31 27 15 19
		f 4 -23 20 14 -22
		mu 0 4 23 20 14 17
		f 4 16 15 -25 21
		mu 0 4 16 18 24 22
		f 4 18 17 -27 -16
		mu 0 4 18 19 25 24
		f 4 19 -21 -28 -18
		mu 0 4 19 15 21 25
		f 4 -31 28 -4 -30
		mu 0 4 29 26 7 6
		f 4 10 4 -33 29
		mu 0 4 12 0 30 28
		f 4 0 5 -35 -5
		mu 0 4 0 1 31 30
		f 4 -12 -29 -36 -6
		mu 0 4 1 10 27 31
		f 4 3 37 -39 -37
		mu 0 4 6 7 33 32
		f 4 11 39 -41 -38
		mu 0 4 7 9 34 33
		f 4 -1 41 42 -40
		mu 0 4 9 8 35 34
		f 4 -11 36 43 -42
		mu 0 4 8 6 32 35
		f 4 38 45 -47 -45
		mu 0 4 32 33 37 36
		f 4 40 47 -49 -46
		mu 0 4 33 34 38 37
		f 4 -43 49 50 -48
		mu 0 4 34 35 39 38
		f 4 -44 44 51 -50
		mu 0 4 35 32 36 39;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pCube6";
	rename -uid "0CD0D036-4872-C286-4F65-88B170BF4307";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 26 ".uvst[0].uvsp[0:25]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.625 0.67984933 0.875 0.070150673 0.125 0.070150673
		 0.375 0.67984933 0.375 0.070150673 0.625 0.070150673 0.625 0.55640912 0.875 0.19359088
		 0.125 0.19359088 0.375 0.55640912 0.375 0.19359088 0.625 0.19359088;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 14 ".pt";
	setAttr ".pt[0]" -type "float3" 0 0.65485507 -0.10364614 ;
	setAttr ".pt[1]" -type "float3" 0 0.65485507 -0.10364614 ;
	setAttr ".pt[4]" -type "float3" 0 0.27796009 -0.35333908 ;
	setAttr ".pt[5]" -type "float3" 0 0.27796009 -0.35333908 ;
	setAttr ".pt[6]" -type "float3" 0 -0.13191327 0.16489159 ;
	setAttr ".pt[7]" -type "float3" 0 -0.13191327 0.16489159 ;
	setAttr ".pt[8]" -type "float3" 0 0 0.35805026 ;
	setAttr ".pt[9]" -type "float3" 0 0 0.35805026 ;
	setAttr ".pt[10]" -type "float3" 0 0.56534261 -9.3132257e-10 ;
	setAttr ".pt[11]" -type "float3" 0 0.56534261 -9.3132257e-10 ;
	setAttr ".pt[12]" -type "float3" 0 0.2214258 -0.39102861 ;
	setAttr ".pt[13]" -type "float3" 0 0.2214258 -0.39102861 ;
	setAttr ".pt[14]" -type "float3" 0 0.0875002 -0.10595943 ;
	setAttr ".pt[15]" -type "float3" 0 0.0875002 -0.10595943 ;
	setAttr -s 16 ".vt[0:15]"  -0.5 -1.084172726 -0.37087685 0.5 -1.084172726 -0.37087685
		 -0.5 0.30632415 0.54652816 0.5 0.30632415 0.54652816 -0.5 0.30632415 0.024305999
		 0.5 0.30632415 0.024305999 -0.5 -0.12598681 -0.73348546 0.5 -0.12598681 -0.73348546
		 0.5 0.163513 -0.66865242 -0.5 0.163513 -0.66865242 -0.5 -0.70928574 -0.036999531
		 0.5 -0.70928574 -0.036999531 0.5 0.26153192 -0.19303802 -0.5 0.26153192 -0.19303802
		 -0.5 -0.012218386 0.3635067 0.5 -0.012218386 0.3635067;
	setAttr -s 28 ".ed[0:27]"  0 1 0 2 3 0 4 5 0 6 7 0 0 10 0 1 11 0 2 4 0
		 3 5 0 4 13 0 5 12 0 6 0 0 7 1 0 8 7 0 9 6 0 8 9 1 10 14 0 9 10 1 11 15 0 10 11 1
		 11 8 1 12 8 0 13 9 0 12 13 1 14 2 0 13 14 1 15 3 0 14 15 1 15 12 1;
	setAttr -s 14 -ch 56 ".fc[0:13]" -type "polyFaces" 
		f 4 26 25 -2 -24
		mu 0 4 24 25 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 22 -9
		mu 0 4 4 5 20 23
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 27 -10 -8 -26
		mu 0 4 25 21 11 3
		f 4 24 23 6 8
		mu 0 4 22 24 2 13
		f 4 -15 12 -4 -14
		mu 0 4 17 14 7 6
		f 4 10 4 -17 13
		mu 0 4 12 0 18 16
		f 4 0 5 -19 -5
		mu 0 4 0 1 19 18
		f 4 -12 -13 -20 -6
		mu 0 4 1 10 15 19
		f 4 -23 20 14 -22
		mu 0 4 23 20 14 17
		f 4 16 15 -25 21
		mu 0 4 16 18 24 22
		f 4 18 17 -27 -16
		mu 0 4 18 19 25 24
		f 4 19 -21 -28 -18
		mu 0 4 19 15 21 25;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube7";
	rename -uid "4632E193-4B6F-703E-223A-C69DFC8F7B38";
	setAttr ".t" -type "double3" 0 0.37200970981348958 -6.7557166035190948 ;
	setAttr ".r" -type "double3" 0 0 180 ;
	setAttr ".s" -type "double3" 0.03333330020876149 1 1 ;
createNode mesh -n "pCubeShape7" -p "pCube7";
	rename -uid "F9FE6B00-43C3-F7A6-B51E-859C664CB595";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 9 ".pt";
	setAttr ".pt[52]" -type "float3" 0 -0.010985598 0.1537984 ;
	setAttr ".pt[53]" -type "float3" 0 -0.010985598 0.1537984 ;
	setAttr ".pt[54]" -type "float3" 3.3306691e-16 0.1812624 -0.2416832 ;
	setAttr ".pt[55]" -type "float3" -3.3306691e-16 0.1812624 -0.2416832 ;
	setAttr ".pt[56]" -type "float3" -3.3306691e-16 0.18675518 -0.071406387 ;
	setAttr ".pt[57]" -type "float3" 3.3306691e-16 0.18675518 -0.071406387 ;
	setAttr ".pt[58]" -type "float3" 1.110223e-16 0.071406387 0.032956794 ;
	setAttr ".pt[59]" -type "float3" -1.110223e-16 0.071406387 0.032956794 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "pCube7";
	rename -uid "00C363F0-4AAF-763F-A7AF-C4A28299E400";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 26 ".uvst[0].uvsp[0:25]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.625 0.67984933 0.875 0.070150673 0.125 0.070150673
		 0.375 0.67984933 0.375 0.070150673 0.625 0.070150673 0.625 0.55640912 0.875 0.19359088
		 0.125 0.19359088 0.375 0.55640912 0.375 0.19359088 0.625 0.19359088;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 14 ".pt";
	setAttr ".pt[0]" -type "float3" 0 0.65485507 -0.10364614 ;
	setAttr ".pt[1]" -type "float3" 0 0.65485507 -0.10364614 ;
	setAttr ".pt[4]" -type "float3" 0 0.27796009 -0.35333908 ;
	setAttr ".pt[5]" -type "float3" 0 0.27796009 -0.35333908 ;
	setAttr ".pt[6]" -type "float3" 0 -0.13191327 0.16489159 ;
	setAttr ".pt[7]" -type "float3" 0 -0.13191327 0.16489159 ;
	setAttr ".pt[8]" -type "float3" 0 0 0.35805026 ;
	setAttr ".pt[9]" -type "float3" 0 0 0.35805026 ;
	setAttr ".pt[10]" -type "float3" 0 0.56534261 -9.3132257e-10 ;
	setAttr ".pt[11]" -type "float3" 0 0.56534261 -9.3132257e-10 ;
	setAttr ".pt[12]" -type "float3" 0 0.2214258 -0.39102861 ;
	setAttr ".pt[13]" -type "float3" 0 0.2214258 -0.39102861 ;
	setAttr ".pt[14]" -type "float3" 0 0.0875002 -0.10595943 ;
	setAttr ".pt[15]" -type "float3" 0 0.0875002 -0.10595943 ;
	setAttr -s 16 ".vt[0:15]"  -0.5 -1.084172726 -0.37087685 0.5 -1.084172726 -0.37087685
		 -0.5 0.30632415 0.54652816 0.5 0.30632415 0.54652816 -0.5 0.30632415 0.024305999
		 0.5 0.30632415 0.024305999 -0.5 -0.12598681 -0.73348546 0.5 -0.12598681 -0.73348546
		 0.5 0.163513 -0.66865242 -0.5 0.163513 -0.66865242 -0.5 -0.70928574 -0.036999531
		 0.5 -0.70928574 -0.036999531 0.5 0.26153192 -0.19303802 -0.5 0.26153192 -0.19303802
		 -0.5 -0.012218386 0.3635067 0.5 -0.012218386 0.3635067;
	setAttr -s 28 ".ed[0:27]"  0 1 0 2 3 0 4 5 0 6 7 0 0 10 0 1 11 0 2 4 0
		 3 5 0 4 13 0 5 12 0 6 0 0 7 1 0 8 7 0 9 6 0 8 9 1 10 14 0 9 10 1 11 15 0 10 11 1
		 11 8 1 12 8 0 13 9 0 12 13 1 14 2 0 13 14 1 15 3 0 14 15 1 15 12 1;
	setAttr -s 14 -ch 56 ".fc[0:13]" -type "polyFaces" 
		f 4 26 25 -2 -24
		mu 0 4 24 25 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 22 -9
		mu 0 4 4 5 20 23
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 27 -10 -8 -26
		mu 0 4 25 21 11 3
		f 4 24 23 6 8
		mu 0 4 22 24 2 13
		f 4 -15 12 -4 -14
		mu 0 4 17 14 7 6
		f 4 10 4 -17 13
		mu 0 4 12 0 18 16
		f 4 0 5 -19 -5
		mu 0 4 0 1 19 18
		f 4 -12 -13 -20 -6
		mu 0 4 1 10 15 19
		f 4 -23 20 14 -22
		mu 0 4 23 20 14 17
		f 4 16 15 -25 21
		mu 0 4 16 18 24 22
		f 4 18 17 -27 -16
		mu 0 4 18 19 25 24
		f 4 19 -21 -28 -18
		mu 0 4 19 15 21 25;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape2" -p "pCube7";
	rename -uid "22F53BA9-4AF2-4D77-295E-B7B3BB52A28B";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 40 ".uvst[0].uvsp[0:39]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.625 0.67984933 0.875 0.070150673 0.125 0.070150673
		 0.375 0.67984933 0.375 0.070150673 0.625 0.070150673 0.625 0.55640912 0.875 0.19359088
		 0.125 0.19359088 0.375 0.55640912 0.375 0.19359088 0.625 0.19359088 0.625 0.71561909
		 0.875 0.034380879 0.125 0.034380879 0.375 0.71561909 0.375 0.034380879 0.625 0.034380879
		 0.375 0.75 0.625 0.75 0.625 1 0.375 1 0.375 0.75 0.625 0.75 0.625 1 0.375 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 28 ".pt[0:27]" -type "float3"  3.3306691e-15 -0.89435399 
		0.11847195 -3.3306691e-15 -0.89435399 0.11847195 -1.110223e-16 -0.072059423 0.34139752 
		1.110223e-16 -0.072059423 0.34139752 1.3322676e-15 -0.35348165 0.026659774 -1.3322676e-15 
		-0.35348165 0.026659774 2.1094237e-15 -0.83473188 -0.14973487 -2.1094237e-15 -0.83473188 
		-0.14973487 -6.6613381e-16 -0.46527433 -0.073663831 6.6613381e-16 -0.46527433 -0.073663831 
		7.7715612e-16 -0.41453794 0.36917239 -7.7715612e-16 -0.41453794 0.36917239 -1.4432899e-15 
		-0.41853189 0.24660437 1.4432899e-15 -0.41853189 0.24660437 7.7715612e-16 -0.16789961 
		0.31385058 -7.7715612e-16 -0.16789961 0.31385058 -1.5543122e-15 -0.59916276 -0.17893635 
		1.5543122e-15 -0.59916276 -0.17893635 1.5543122e-15 -0.63042015 0.27985197 -1.5543122e-15 
		-0.63042015 0.27985197 2.9976022e-15 -1.0333294 -0.10061936 -2.9976022e-15 -1.0333294 
		-0.10061936 -3.4416914e-15 -1.0012311 0.077725872 3.4416914e-15 -1.0012311 0.077725872 
		3.663736e-15 -1.4051145 -0.51854974 -3.663736e-15 -1.4051145 -0.51854974 -3.663736e-15 
		-1.3149177 -0.47593763 3.663736e-15 -1.3149177 -0.47593763;
	setAttr -s 28 ".vt[0:27]"  -0.50000006 -0.41047287 -0.47923422 0.50000006 -0.41047287 -0.47923422
		 -0.50000006 0.30632412 0.54652816 0.50000006 0.30632412 0.54652816 -0.50000006 0.58428431 -0.32903314
		 0.50000006 0.58428431 -0.32903314 -0.50000006 -0.2579 -0.56859398 0.50000006 -0.2579 -0.56859398
		 0.50000006 0.16351295 -0.31060219 -0.50000006 0.16351295 -0.31060219 -0.50000006 -0.14394307 -0.036999583
		 0.50000006 -0.14394307 -0.036999583 0.50000006 0.48295772 -0.58406663 -0.50000006 0.48295772 -0.58406663
		 -0.50000006 0.075281858 0.25754726 0.50000006 0.075281858 0.25754726 0.50000006 -0.10318828 -0.37148416
		 -0.50000006 -0.10318828 -0.37148416 -0.50000006 -0.29887795 -0.2600925 0.50000006 -0.29887795 -0.2600925
		 -0.50000006 -0.48686528 -0.90910792 0.50000006 -0.48686528 -0.90910792 0.50000006 -0.57162809 -0.81974816
		 -0.50000006 -0.57162809 -0.81974816 -0.50000006 -0.48686528 -0.90910792 0.50000006 -0.48686528 -0.90910792
		 0.50000006 -0.57162809 -0.81974816 -0.50000006 -0.57162809 -0.81974816;
	setAttr -s 52 ".ed[0:51]"  0 1 1 2 3 0 4 5 0 6 7 1 0 18 0 1 19 0 2 4 0
		 3 5 0 4 13 0 5 12 0 6 0 1 7 1 1 8 16 0 9 17 0 8 9 1 10 14 0 9 10 1 11 15 0 10 11 1
		 11 8 1 12 8 0 13 9 0 12 13 1 14 2 0 13 14 1 15 3 0 14 15 1 15 12 1 16 7 0 17 6 0
		 16 17 1 18 10 0 17 18 1 19 11 0 18 19 1 19 16 1 6 20 0 7 21 0 20 21 0 1 22 0 21 22 0
		 0 23 0 23 22 0 20 23 0 20 24 0 21 25 0 24 25 0 22 26 0 25 26 0 23 27 0 27 26 0 24 27 0;
	setAttr -s 26 -ch 104 ".fc[0:25]" -type "polyFaces" 
		f 4 26 25 -2 -24
		mu 0 4 24 25 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 22 -9
		mu 0 4 4 5 20 23
		f 4 46 48 -51 -52
		mu 0 4 36 37 38 39
		f 4 27 -10 -8 -26
		mu 0 4 25 21 11 3
		f 4 24 23 6 8
		mu 0 4 22 24 2 13
		f 4 -15 12 30 -14
		mu 0 4 17 14 26 29
		f 4 32 31 -17 13
		mu 0 4 28 30 18 16
		f 4 34 33 -19 -32
		mu 0 4 30 31 19 18
		f 4 35 -13 -20 -34
		mu 0 4 31 27 15 19
		f 4 -23 20 14 -22
		mu 0 4 23 20 14 17
		f 4 16 15 -25 21
		mu 0 4 16 18 24 22
		f 4 18 17 -27 -16
		mu 0 4 18 19 25 24
		f 4 19 -21 -28 -18
		mu 0 4 19 15 21 25
		f 4 -31 28 -4 -30
		mu 0 4 29 26 7 6
		f 4 10 4 -33 29
		mu 0 4 12 0 30 28
		f 4 0 5 -35 -5
		mu 0 4 0 1 31 30
		f 4 -12 -29 -36 -6
		mu 0 4 1 10 27 31
		f 4 3 37 -39 -37
		mu 0 4 6 7 33 32
		f 4 11 39 -41 -38
		mu 0 4 7 9 34 33
		f 4 -1 41 42 -40
		mu 0 4 9 8 35 34
		f 4 -11 36 43 -42
		mu 0 4 8 6 32 35
		f 4 38 45 -47 -45
		mu 0 4 32 33 37 36
		f 4 40 47 -49 -46
		mu 0 4 33 34 38 37
		f 4 -43 49 50 -48
		mu 0 4 34 35 39 38
		f 4 -44 44 51 -50
		mu 0 4 35 32 36 39;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube8";
	rename -uid "315C6872-448C-10DD-2836-F39A8D2AAE6A";
	setAttr ".t" -type "double3" 0 1.4015440702648601 -1.8578607443045823 ;
	setAttr ".r" -type "double3" -37.586874782320805 0 0 ;
	setAttr ".s" -type "double3" 0.023012869104460574 1 0.32458073594192532 ;
createNode mesh -n "pCubeShape8" -p "pCube8";
	rename -uid "305248B9-4ABB-CADE-5D75-99864DFA227D";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.17778418958187103 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[8:15]" -type "float3"  0 0.013662159 -0.18877198 
		0 0.013662159 -0.18877198 0 -0.0068112309 -0.097249001 0 -0.0068112309 -0.097249001 
		0 0.005911727 -0.14817291 0 0.005911727 -0.14817291 0 -0.0072758682 -0.076233715 
		0 -0.0072758682 -0.076233715;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube9";
	rename -uid "B0EF9DFB-4CF4-02A0-E536-E1A69DF92A96";
	setAttr ".t" -type "double3" 0 1.2896663863438467 -2.2617581455787494 ;
	setAttr ".r" -type "double3" -37.586874782320805 0 0 ;
	setAttr ".s" -type "double3" 0.023012869104460574 1 0.32458073594192532 ;
createNode mesh -n "pCubeShape9" -p "pCube9";
	rename -uid "0F06DB4A-4A24-E4F8-7EA8-8D80854F96AE";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.17778418958187103 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 26 ".uvst[0].uvsp[0:25]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.625 0.085778117 0.375 0.085778117 0.125 0.085778117
		 0.375 0.66422188 0.625 0.66422188 0.875 0.085778117 0.625 0.17778419 0.375 0.17778419
		 0.125 0.17778419 0.375 0.5722158 0.625 0.5722158 0.875 0.17778419;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[8:15]" -type "float3"  0 0.013662159 -0.18877198 
		0 0.013662159 -0.18877198 0 -0.0068112309 -0.097249001 0 -0.0068112309 -0.097249001 
		0 0.005911727 -0.14817291 0 0.005911727 -0.14817291 0 -0.0072758682 -0.076233715 
		0 -0.0072758682 -0.076233715;
	setAttr -s 16 ".vt[0:15]"  -0.5 -0.19228154 0.5831573 0.5 -0.19228154 0.5831573
		 -0.5 0.1003958 0.072429478 0.5 0.1003958 0.072429478 -0.5 0.20285535 -0.51591229
		 0.5 0.20285535 -0.51591229 -0.5 -0.095532835 -0.20146108 0.5 -0.095532835 -0.20146108
		 0.5 -0.091860302 0.40792024 -0.5 -0.091860302 0.40792024 -0.5 0.0068478733 -0.30935323
		 0.5 0.0068478733 -0.30935323 0.5 0.015852071 0.21995999 -0.5 0.015852071 0.21995999
		 -0.5 0.11666197 -0.42507893 0.5 0.11666197 -0.42507893;
	setAttr -s 28 ".ed[0:27]"  0 1 0 2 3 0 4 5 0 6 7 0 0 9 0 1 8 0 2 4 0
		 3 5 0 4 14 0 5 15 0 6 0 0 7 1 0 8 12 0 9 13 0 8 9 1 10 6 0 9 10 1 11 7 0 10 11 1
		 11 8 1 12 3 0 13 2 0 12 13 1 14 10 0 13 14 1 15 11 0 14 15 1 15 12 1;
	setAttr -s 14 -ch 56 ".fc[0:13]" -type "polyFaces" 
		f 4 0 5 14 -5
		mu 0 4 0 1 14 15
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 18 17 -4 -16
		mu 0 4 17 18 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -18 19 -6
		mu 0 4 1 10 19 14
		f 4 10 4 16 15
		mu 0 4 12 0 15 16
		f 4 -15 12 22 -14
		mu 0 4 15 14 20 21
		f 4 -17 13 24 23
		mu 0 4 16 15 21 22
		f 4 26 25 -19 -24
		mu 0 4 23 24 18 17
		f 4 -20 -26 27 -13
		mu 0 4 14 19 25 20
		f 4 -23 20 -2 -22
		mu 0 4 21 20 3 2
		f 4 -25 21 6 8
		mu 0 4 22 21 2 13
		f 4 2 9 -27 -9
		mu 0 4 4 5 24 23
		f 4 -28 -10 -8 -21
		mu 0 4 20 25 11 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube10";
	rename -uid "479DB335-42B5-96D6-15A9-4F91A039F316";
	setAttr ".t" -type "double3" 0 1.1133168167734357 -2.8249390290455456 ;
	setAttr ".r" -type "double3" -37.586874782320805 0 0 ;
	setAttr ".s" -type "double3" 0.023012869104460574 1 0.32458073594192532 ;
createNode mesh -n "pCubeShape10" -p "pCube10";
	rename -uid "04CFAF55-4051-72FA-B6E1-75A0CBFDBC91";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.17778418958187103 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 26 ".uvst[0].uvsp[0:25]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.625 0.085778117 0.375 0.085778117 0.125 0.085778117
		 0.375 0.66422188 0.625 0.66422188 0.875 0.085778117 0.625 0.17778419 0.375 0.17778419
		 0.125 0.17778419 0.375 0.5722158 0.625 0.5722158 0.875 0.17778419;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[8:15]" -type "float3"  0 0.013662159 -0.18877198 
		0 0.013662159 -0.18877198 0 -0.0068112309 -0.097249001 0 -0.0068112309 -0.097249001 
		0 0.005911727 -0.14817291 0 0.005911727 -0.14817291 0 -0.0072758682 -0.076233715 
		0 -0.0072758682 -0.076233715;
	setAttr -s 16 ".vt[0:15]"  -0.5 -0.19228154 0.5831573 0.5 -0.19228154 0.5831573
		 -0.5 0.1003958 0.072429478 0.5 0.1003958 0.072429478 -0.5 0.20285535 -0.51591229
		 0.5 0.20285535 -0.51591229 -0.5 -0.095532835 -0.20146108 0.5 -0.095532835 -0.20146108
		 0.5 -0.091860302 0.40792024 -0.5 -0.091860302 0.40792024 -0.5 0.0068478733 -0.30935323
		 0.5 0.0068478733 -0.30935323 0.5 0.015852071 0.21995999 -0.5 0.015852071 0.21995999
		 -0.5 0.11666197 -0.42507893 0.5 0.11666197 -0.42507893;
	setAttr -s 28 ".ed[0:27]"  0 1 0 2 3 0 4 5 0 6 7 0 0 9 0 1 8 0 2 4 0
		 3 5 0 4 14 0 5 15 0 6 0 0 7 1 0 8 12 0 9 13 0 8 9 1 10 6 0 9 10 1 11 7 0 10 11 1
		 11 8 1 12 3 0 13 2 0 12 13 1 14 10 0 13 14 1 15 11 0 14 15 1 15 12 1;
	setAttr -s 14 -ch 56 ".fc[0:13]" -type "polyFaces" 
		f 4 0 5 14 -5
		mu 0 4 0 1 14 15
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 18 17 -4 -16
		mu 0 4 17 18 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -18 19 -6
		mu 0 4 1 10 19 14
		f 4 10 4 16 15
		mu 0 4 12 0 15 16
		f 4 -15 12 22 -14
		mu 0 4 15 14 20 21
		f 4 -17 13 24 23
		mu 0 4 16 15 21 22
		f 4 26 25 -19 -24
		mu 0 4 23 24 18 17
		f 4 -20 -26 27 -13
		mu 0 4 14 19 25 20
		f 4 -23 20 -2 -22
		mu 0 4 21 20 3 2
		f 4 -25 21 6 8
		mu 0 4 22 21 2 13
		f 4 2 9 -27 -9
		mu 0 4 4 5 24 23
		f 4 -28 -10 -8 -21
		mu 0 4 20 25 11 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube11";
	rename -uid "805D7282-4BD5-9A28-0764-5BA0881393C6";
	setAttr ".t" -type "double3" 0 0.96541072616599433 -3.3938086083049357 ;
	setAttr ".r" -type "double3" -37.586874782320805 0 0 ;
	setAttr ".s" -type "double3" 0.023012869104460574 1 0.32458073594192532 ;
createNode mesh -n "pCubeShape11" -p "pCube11";
	rename -uid "0E41D1D4-4437-BB46-B716-0DB3E8F56104";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.17778418958187103 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 26 ".uvst[0].uvsp[0:25]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.625 0.085778117 0.375 0.085778117 0.125 0.085778117
		 0.375 0.66422188 0.625 0.66422188 0.875 0.085778117 0.625 0.17778419 0.375 0.17778419
		 0.125 0.17778419 0.375 0.5722158 0.625 0.5722158 0.875 0.17778419;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[8:15]" -type "float3"  0 0.013662159 -0.18877198 
		0 0.013662159 -0.18877198 0 -0.0068112309 -0.097249001 0 -0.0068112309 -0.097249001 
		0 0.005911727 -0.14817291 0 0.005911727 -0.14817291 0 -0.0072758682 -0.076233715 
		0 -0.0072758682 -0.076233715;
	setAttr -s 16 ".vt[0:15]"  -0.5 -0.19228154 0.5831573 0.5 -0.19228154 0.5831573
		 -0.5 0.1003958 0.072429478 0.5 0.1003958 0.072429478 -0.5 0.20285535 -0.51591229
		 0.5 0.20285535 -0.51591229 -0.5 -0.095532835 -0.20146108 0.5 -0.095532835 -0.20146108
		 0.5 -0.091860302 0.40792024 -0.5 -0.091860302 0.40792024 -0.5 0.0068478733 -0.30935323
		 0.5 0.0068478733 -0.30935323 0.5 0.015852071 0.21995999 -0.5 0.015852071 0.21995999
		 -0.5 0.11666197 -0.42507893 0.5 0.11666197 -0.42507893;
	setAttr -s 28 ".ed[0:27]"  0 1 0 2 3 0 4 5 0 6 7 0 0 9 0 1 8 0 2 4 0
		 3 5 0 4 14 0 5 15 0 6 0 0 7 1 0 8 12 0 9 13 0 8 9 1 10 6 0 9 10 1 11 7 0 10 11 1
		 11 8 1 12 3 0 13 2 0 12 13 1 14 10 0 13 14 1 15 11 0 14 15 1 15 12 1;
	setAttr -s 14 -ch 56 ".fc[0:13]" -type "polyFaces" 
		f 4 0 5 14 -5
		mu 0 4 0 1 14 15
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 18 17 -4 -16
		mu 0 4 17 18 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -18 19 -6
		mu 0 4 1 10 19 14
		f 4 10 4 16 15
		mu 0 4 12 0 15 16
		f 4 -15 12 22 -14
		mu 0 4 15 14 20 21
		f 4 -17 13 24 23
		mu 0 4 16 15 21 22
		f 4 26 25 -19 -24
		mu 0 4 23 24 18 17
		f 4 -20 -26 27 -13
		mu 0 4 14 19 25 20
		f 4 -23 20 -2 -22
		mu 0 4 21 20 3 2
		f 4 -25 21 6 8
		mu 0 4 22 21 2 13
		f 4 2 9 -27 -9
		mu 0 4 4 5 24 23
		f 4 -28 -10 -8 -21
		mu 0 4 20 25 11 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube12";
	rename -uid "47B9937C-43B5-23D8-F43C-B3ADC4ED210D";
	setAttr ".t" -type "double3" 0 0.81341756348002658 -3.8205586420001532 ;
	setAttr ".r" -type "double3" -37.586874782320805 0 0 ;
	setAttr ".s" -type "double3" 0.023012869104460574 1 0.32458073594192532 ;
createNode mesh -n "pCubeShape12" -p "pCube12";
	rename -uid "285879D5-4861-8388-0BF8-548A0EFFFD8F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.17778418958187103 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 26 ".uvst[0].uvsp[0:25]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.625 0.085778117 0.375 0.085778117 0.125 0.085778117
		 0.375 0.66422188 0.625 0.66422188 0.875 0.085778117 0.625 0.17778419 0.375 0.17778419
		 0.125 0.17778419 0.375 0.5722158 0.625 0.5722158 0.875 0.17778419;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[8:15]" -type "float3"  0 0.013662159 -0.18877198 
		0 0.013662159 -0.18877198 0 -0.0068112309 -0.097249001 0 -0.0068112309 -0.097249001 
		0 0.005911727 -0.14817291 0 0.005911727 -0.14817291 0 -0.0072758682 -0.076233715 
		0 -0.0072758682 -0.076233715;
	setAttr -s 16 ".vt[0:15]"  -0.5 -0.19228154 0.5831573 0.5 -0.19228154 0.5831573
		 -0.5 0.1003958 0.072429478 0.5 0.1003958 0.072429478 -0.5 0.20285535 -0.51591229
		 0.5 0.20285535 -0.51591229 -0.5 -0.095532835 -0.20146108 0.5 -0.095532835 -0.20146108
		 0.5 -0.091860302 0.40792024 -0.5 -0.091860302 0.40792024 -0.5 0.0068478733 -0.30935323
		 0.5 0.0068478733 -0.30935323 0.5 0.015852071 0.21995999 -0.5 0.015852071 0.21995999
		 -0.5 0.11666197 -0.42507893 0.5 0.11666197 -0.42507893;
	setAttr -s 28 ".ed[0:27]"  0 1 0 2 3 0 4 5 0 6 7 0 0 9 0 1 8 0 2 4 0
		 3 5 0 4 14 0 5 15 0 6 0 0 7 1 0 8 12 0 9 13 0 8 9 1 10 6 0 9 10 1 11 7 0 10 11 1
		 11 8 1 12 3 0 13 2 0 12 13 1 14 10 0 13 14 1 15 11 0 14 15 1 15 12 1;
	setAttr -s 14 -ch 56 ".fc[0:13]" -type "polyFaces" 
		f 4 0 5 14 -5
		mu 0 4 0 1 14 15
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 18 17 -4 -16
		mu 0 4 17 18 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -18 19 -6
		mu 0 4 1 10 19 14
		f 4 10 4 16 15
		mu 0 4 12 0 15 16
		f 4 -15 12 22 -14
		mu 0 4 15 14 20 21
		f 4 -17 13 24 23
		mu 0 4 16 15 21 22
		f 4 26 25 -19 -24
		mu 0 4 23 24 18 17
		f 4 -20 -26 27 -13
		mu 0 4 14 19 25 20
		f 4 -23 20 -2 -22
		mu 0 4 21 20 3 2
		f 4 -25 21 6 8
		mu 0 4 22 21 2 13
		f 4 2 9 -27 -9
		mu 0 4 4 5 24 23
		f 4 -28 -10 -8 -21
		mu 0 4 20 25 11 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube13";
	rename -uid "ECBF6575-4C98-D118-0192-7A81326F0DC9";
	setAttr ".t" -type "double3" 0 0.62050316468629829 -4.3233052570383546 ;
	setAttr ".r" -type "double3" -37.586874782320805 0 0 ;
	setAttr ".s" -type "double3" 0.023012869104460574 1 0.32458073594192532 ;
createNode mesh -n "pCubeShape13" -p "pCube13";
	rename -uid "F112F698-4038-7A17-703F-19B2D847467B";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.17778418958187103 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 26 ".uvst[0].uvsp[0:25]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.625 0.085778117 0.375 0.085778117 0.125 0.085778117
		 0.375 0.66422188 0.625 0.66422188 0.875 0.085778117 0.625 0.17778419 0.375 0.17778419
		 0.125 0.17778419 0.375 0.5722158 0.625 0.5722158 0.875 0.17778419;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[8:15]" -type "float3"  0 0.013662159 -0.18877198 
		0 0.013662159 -0.18877198 0 -0.0068112309 -0.097249001 0 -0.0068112309 -0.097249001 
		0 0.005911727 -0.14817291 0 0.005911727 -0.14817291 0 -0.0072758682 -0.076233715 
		0 -0.0072758682 -0.076233715;
	setAttr -s 16 ".vt[0:15]"  -0.5 -0.19228154 0.5831573 0.5 -0.19228154 0.5831573
		 -0.5 0.1003958 0.072429478 0.5 0.1003958 0.072429478 -0.5 0.20285535 -0.51591229
		 0.5 0.20285535 -0.51591229 -0.5 -0.095532835 -0.20146108 0.5 -0.095532835 -0.20146108
		 0.5 -0.091860302 0.40792024 -0.5 -0.091860302 0.40792024 -0.5 0.0068478733 -0.30935323
		 0.5 0.0068478733 -0.30935323 0.5 0.015852071 0.21995999 -0.5 0.015852071 0.21995999
		 -0.5 0.11666197 -0.42507893 0.5 0.11666197 -0.42507893;
	setAttr -s 28 ".ed[0:27]"  0 1 0 2 3 0 4 5 0 6 7 0 0 9 0 1 8 0 2 4 0
		 3 5 0 4 14 0 5 15 0 6 0 0 7 1 0 8 12 0 9 13 0 8 9 1 10 6 0 9 10 1 11 7 0 10 11 1
		 11 8 1 12 3 0 13 2 0 12 13 1 14 10 0 13 14 1 15 11 0 14 15 1 15 12 1;
	setAttr -s 14 -ch 56 ".fc[0:13]" -type "polyFaces" 
		f 4 0 5 14 -5
		mu 0 4 0 1 14 15
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 18 17 -4 -16
		mu 0 4 17 18 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -18 19 -6
		mu 0 4 1 10 19 14
		f 4 10 4 16 15
		mu 0 4 12 0 15 16
		f 4 -15 12 22 -14
		mu 0 4 15 14 20 21
		f 4 -17 13 24 23
		mu 0 4 16 15 21 22
		f 4 26 25 -19 -24
		mu 0 4 23 24 18 17
		f 4 -20 -26 27 -13
		mu 0 4 14 19 25 20
		f 4 -23 20 -2 -22
		mu 0 4 21 20 3 2
		f 4 -25 21 6 8
		mu 0 4 22 21 2 13
		f 4 2 9 -27 -9
		mu 0 4 4 5 24 23
		f 4 -28 -10 -8 -21
		mu 0 4 20 25 11 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube14";
	rename -uid "7552AC9D-4A8E-ED97-7EFF-05A6AAE2B389";
	setAttr ".t" -type "double3" 0 0.5328148015982398 -4.7559011816061094 ;
	setAttr ".r" -type "double3" -37.586874782320805 0 0 ;
	setAttr ".s" -type "double3" 0.023012869104460574 1 0.32458073594192532 ;
createNode mesh -n "pCubeShape14" -p "pCube14";
	rename -uid "3C090524-44AF-3985-CAFD-03A1F8BF57B5";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.17778418958187103 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 26 ".uvst[0].uvsp[0:25]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.625 0.085778117 0.375 0.085778117 0.125 0.085778117
		 0.375 0.66422188 0.625 0.66422188 0.875 0.085778117 0.625 0.17778419 0.375 0.17778419
		 0.125 0.17778419 0.375 0.5722158 0.625 0.5722158 0.875 0.17778419;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[8:15]" -type "float3"  0 0.013662159 -0.18877198 
		0 0.013662159 -0.18877198 0 -0.0068112309 -0.097249001 0 -0.0068112309 -0.097249001 
		0 0.005911727 -0.14817291 0 0.005911727 -0.14817291 0 -0.0072758682 -0.076233715 
		0 -0.0072758682 -0.076233715;
	setAttr -s 16 ".vt[0:15]"  -0.5 -0.19228154 0.5831573 0.5 -0.19228154 0.5831573
		 -0.5 0.1003958 0.072429478 0.5 0.1003958 0.072429478 -0.5 0.20285535 -0.51591229
		 0.5 0.20285535 -0.51591229 -0.5 -0.095532835 -0.20146108 0.5 -0.095532835 -0.20146108
		 0.5 -0.091860302 0.40792024 -0.5 -0.091860302 0.40792024 -0.5 0.0068478733 -0.30935323
		 0.5 0.0068478733 -0.30935323 0.5 0.015852071 0.21995999 -0.5 0.015852071 0.21995999
		 -0.5 0.11666197 -0.42507893 0.5 0.11666197 -0.42507893;
	setAttr -s 28 ".ed[0:27]"  0 1 0 2 3 0 4 5 0 6 7 0 0 9 0 1 8 0 2 4 0
		 3 5 0 4 14 0 5 15 0 6 0 0 7 1 0 8 12 0 9 13 0 8 9 1 10 6 0 9 10 1 11 7 0 10 11 1
		 11 8 1 12 3 0 13 2 0 12 13 1 14 10 0 13 14 1 15 11 0 14 15 1 15 12 1;
	setAttr -s 14 -ch 56 ".fc[0:13]" -type "polyFaces" 
		f 4 0 5 14 -5
		mu 0 4 0 1 14 15
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 18 17 -4 -16
		mu 0 4 17 18 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -18 19 -6
		mu 0 4 1 10 19 14
		f 4 10 4 16 15
		mu 0 4 12 0 15 16
		f 4 -15 12 22 -14
		mu 0 4 15 14 20 21
		f 4 -17 13 24 23
		mu 0 4 16 15 21 22
		f 4 26 25 -19 -24
		mu 0 4 23 24 18 17
		f 4 -20 -26 27 -13
		mu 0 4 14 19 25 20
		f 4 -23 20 -2 -22
		mu 0 4 21 20 3 2
		f 4 -25 21 6 8
		mu 0 4 22 21 2 13
		f 4 2 9 -27 -9
		mu 0 4 4 5 24 23
		f 4 -28 -10 -8 -21
		mu 0 4 20 25 11 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube15";
	rename -uid "535F32C9-4087-652D-9382-92A284A4B05D";
	setAttr ".t" -type "double3" 0 0.38666752978480923 -5.2878772510069965 ;
	setAttr ".r" -type "double3" -37.586874782320805 0 0 ;
	setAttr ".s" -type "double3" 0.023012869104460574 1 0.32458073594192532 ;
createNode mesh -n "pCubeShape15" -p "pCube15";
	rename -uid "D6B1543D-4971-705C-5BF7-50961CC294EC";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.17778418958187103 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 26 ".uvst[0].uvsp[0:25]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.625 0.085778117 0.375 0.085778117 0.125 0.085778117
		 0.375 0.66422188 0.625 0.66422188 0.875 0.085778117 0.625 0.17778419 0.375 0.17778419
		 0.125 0.17778419 0.375 0.5722158 0.625 0.5722158 0.875 0.17778419;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[8:15]" -type "float3"  0 0.013662159 -0.18877198 
		0 0.013662159 -0.18877198 0 -0.0068112309 -0.097249001 0 -0.0068112309 -0.097249001 
		0 0.005911727 -0.14817291 0 0.005911727 -0.14817291 0 -0.0072758682 -0.076233715 
		0 -0.0072758682 -0.076233715;
	setAttr -s 16 ".vt[0:15]"  -0.5 -0.19228154 0.5831573 0.5 -0.19228154 0.5831573
		 -0.5 0.1003958 0.072429478 0.5 0.1003958 0.072429478 -0.5 0.20285535 -0.51591229
		 0.5 0.20285535 -0.51591229 -0.5 -0.095532835 -0.20146108 0.5 -0.095532835 -0.20146108
		 0.5 -0.091860302 0.40792024 -0.5 -0.091860302 0.40792024 -0.5 0.0068478733 -0.30935323
		 0.5 0.0068478733 -0.30935323 0.5 0.015852071 0.21995999 -0.5 0.015852071 0.21995999
		 -0.5 0.11666197 -0.42507893 0.5 0.11666197 -0.42507893;
	setAttr -s 28 ".ed[0:27]"  0 1 0 2 3 0 4 5 0 6 7 0 0 9 0 1 8 0 2 4 0
		 3 5 0 4 14 0 5 15 0 6 0 0 7 1 0 8 12 0 9 13 0 8 9 1 10 6 0 9 10 1 11 7 0 10 11 1
		 11 8 1 12 3 0 13 2 0 12 13 1 14 10 0 13 14 1 15 11 0 14 15 1 15 12 1;
	setAttr -s 14 -ch 56 ".fc[0:13]" -type "polyFaces" 
		f 4 0 5 14 -5
		mu 0 4 0 1 14 15
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 18 17 -4 -16
		mu 0 4 17 18 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -18 19 -6
		mu 0 4 1 10 19 14
		f 4 10 4 16 15
		mu 0 4 12 0 15 16
		f 4 -15 12 22 -14
		mu 0 4 15 14 20 21
		f 4 -17 13 24 23
		mu 0 4 16 15 21 22
		f 4 26 25 -19 -24
		mu 0 4 23 24 18 17
		f 4 -20 -26 27 -13
		mu 0 4 14 19 25 20
		f 4 -23 20 -2 -22
		mu 0 4 21 20 3 2
		f 4 -25 21 6 8
		mu 0 4 22 21 2 13
		f 4 2 9 -27 -9
		mu 0 4 4 5 24 23
		f 4 -28 -10 -8 -21
		mu 0 4 20 25 11 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube16";
	rename -uid "D5CBA159-44C7-4FF8-3794-388E4A75584E";
	setAttr ".t" -type "double3" 0 0.27559560320660192 -5.7497026299374374 ;
	setAttr ".r" -type "double3" -37.586874782320805 0 0 ;
	setAttr ".s" -type "double3" 0.023012869104460574 1 0.32458073594192532 ;
createNode mesh -n "pCubeShape16" -p "pCube16";
	rename -uid "F2F66F3E-4AC0-2528-6AE0-398BBF5F0E66";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.17778418958187103 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 26 ".uvst[0].uvsp[0:25]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.625 0.085778117 0.375 0.085778117 0.125 0.085778117
		 0.375 0.66422188 0.625 0.66422188 0.875 0.085778117 0.625 0.17778419 0.375 0.17778419
		 0.125 0.17778419 0.375 0.5722158 0.625 0.5722158 0.875 0.17778419;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[8:15]" -type "float3"  0 0.013662159 -0.18877198 
		0 0.013662159 -0.18877198 0 -0.0068112309 -0.097249001 0 -0.0068112309 -0.097249001 
		0 0.005911727 -0.14817291 0 0.005911727 -0.14817291 0 -0.0072758682 -0.076233715 
		0 -0.0072758682 -0.076233715;
	setAttr -s 16 ".vt[0:15]"  -0.5 -0.19228154 0.5831573 0.5 -0.19228154 0.5831573
		 -0.5 0.1003958 0.072429478 0.5 0.1003958 0.072429478 -0.5 0.20285535 -0.51591229
		 0.5 0.20285535 -0.51591229 -0.5 -0.095532835 -0.20146108 0.5 -0.095532835 -0.20146108
		 0.5 -0.091860302 0.40792024 -0.5 -0.091860302 0.40792024 -0.5 0.0068478733 -0.30935323
		 0.5 0.0068478733 -0.30935323 0.5 0.015852071 0.21995999 -0.5 0.015852071 0.21995999
		 -0.5 0.11666197 -0.42507893 0.5 0.11666197 -0.42507893;
	setAttr -s 28 ".ed[0:27]"  0 1 0 2 3 0 4 5 0 6 7 0 0 9 0 1 8 0 2 4 0
		 3 5 0 4 14 0 5 15 0 6 0 0 7 1 0 8 12 0 9 13 0 8 9 1 10 6 0 9 10 1 11 7 0 10 11 1
		 11 8 1 12 3 0 13 2 0 12 13 1 14 10 0 13 14 1 15 11 0 14 15 1 15 12 1;
	setAttr -s 14 -ch 56 ".fc[0:13]" -type "polyFaces" 
		f 4 0 5 14 -5
		mu 0 4 0 1 14 15
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 18 17 -4 -16
		mu 0 4 17 18 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -18 19 -6
		mu 0 4 1 10 19 14
		f 4 10 4 16 15
		mu 0 4 12 0 15 16
		f 4 -15 12 22 -14
		mu 0 4 15 14 20 21
		f 4 -17 13 24 23
		mu 0 4 16 15 21 22
		f 4 26 25 -19 -24
		mu 0 4 23 24 18 17
		f 4 -20 -26 27 -13
		mu 0 4 14 19 25 20
		f 4 -23 20 -2 -22
		mu 0 4 21 20 3 2
		f 4 -25 21 6 8
		mu 0 4 22 21 2 13
		f 4 2 9 -27 -9
		mu 0 4 4 5 24 23
		f 4 -28 -10 -8 -21
		mu 0 4 20 25 11 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube17";
	rename -uid "DA92D1A3-4EA7-A13F-6EF1-69BC39FC917E";
	setAttr ".t" -type "double3" 0 -1.4405425613629825 -2.3813817603474448 ;
	setAttr ".r" -type "double3" -37.586874782320805 0 180 ;
	setAttr ".s" -type "double3" 0.023012869104460574 1 0.32458073594192532 ;
createNode mesh -n "pCubeShape17" -p "pCube17";
	rename -uid "E4B4DD4D-48BE-50FA-487D-50AFB1888B10";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.085778117179870605 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 26 ".uvst[0].uvsp[0:25]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.625 0.085778117 0.375 0.085778117 0.125 0.085778117
		 0.375 0.66422188 0.625 0.66422188 0.875 0.085778117 0.625 0.17778419 0.375 0.17778419
		 0.125 0.17778419 0.375 0.5722158 0.625 0.5722158 0.875 0.17778419;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 14 ".pt";
	setAttr ".pt[0]" -type "float3" 0 -0.011419426 0.14515497 ;
	setAttr ".pt[1]" -type "float3" 0 -0.011419426 0.14515497 ;
	setAttr ".pt[2]" -type "float3" 0 0.015012002 -0.060085893 ;
	setAttr ".pt[3]" -type "float3" 0 0.015012002 -0.060085893 ;
	setAttr ".pt[4]" -type "float3" 3.3306691e-16 -0.12453522 -0.12309758 ;
	setAttr ".pt[5]" -type "float3" -3.3306691e-16 -0.12453522 -0.12309758 ;
	setAttr ".pt[8]" -type "float3" 0 0.012905118 -0.12358664 ;
	setAttr ".pt[9]" -type "float3" 0 0.012905118 -0.12358664 ;
	setAttr ".pt[10]" -type "float3" 0 -0.046586595 -0.00020243879 ;
	setAttr ".pt[11]" -type "float3" 0 -0.046586595 -0.00020243879 ;
	setAttr ".pt[12]" -type "float3" -5.5511151e-17 0.0027553423 -0.098246261 ;
	setAttr ".pt[13]" -type "float3" 5.5511151e-17 0.0027553423 -0.098246261 ;
	setAttr ".pt[14]" -type "float3" 5.5511151e-17 -0.10106869 -0.011602171 ;
	setAttr ".pt[15]" -type "float3" -5.5511151e-17 -0.10106869 -0.011602171 ;
	setAttr -s 16 ".vt[0:15]"  -0.5 -0.19228154 0.5831573 0.5 -0.19228154 0.5831573
		 -0.5 0.1003958 0.072429478 0.5 0.1003958 0.072429478 -0.5 0.20285535 -0.51591229
		 0.5 0.20285535 -0.51591229 -0.5 -0.095532835 -0.20146108 0.5 -0.095532835 -0.20146108
		 0.5 -0.091860302 0.40792024 -0.5 -0.091860302 0.40792024 -0.5 0.0068478733 -0.30935323
		 0.5 0.0068478733 -0.30935323 0.5 0.015852071 0.21995999 -0.5 0.015852071 0.21995999
		 -0.5 0.11666197 -0.42507893 0.5 0.11666197 -0.42507893;
	setAttr -s 28 ".ed[0:27]"  0 1 0 2 3 0 4 5 0 6 7 0 0 9 0 1 8 0 2 4 0
		 3 5 0 4 14 0 5 15 0 6 0 0 7 1 0 8 12 0 9 13 0 8 9 1 10 6 0 9 10 1 11 7 0 10 11 1
		 11 8 1 12 3 0 13 2 0 12 13 1 14 10 0 13 14 1 15 11 0 14 15 1 15 12 1;
	setAttr -s 14 -ch 56 ".fc[0:13]" -type "polyFaces" 
		f 4 0 5 14 -5
		mu 0 4 0 1 14 15
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 18 17 -4 -16
		mu 0 4 17 18 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -18 19 -6
		mu 0 4 1 10 19 14
		f 4 10 4 16 15
		mu 0 4 12 0 15 16
		f 4 -15 12 22 -14
		mu 0 4 15 14 20 21
		f 4 -17 13 24 23
		mu 0 4 16 15 21 22
		f 4 26 25 -19 -24
		mu 0 4 23 24 18 17
		f 4 -20 -26 27 -13
		mu 0 4 14 19 25 20
		f 4 -23 20 -2 -22
		mu 0 4 21 20 3 2
		f 4 -25 21 6 8
		mu 0 4 22 21 2 13
		f 4 2 9 -27 -9
		mu 0 4 4 5 24 23
		f 4 -28 -10 -8 -21
		mu 0 4 20 25 11 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube18";
	rename -uid "A5CF87B9-4041-A020-A58E-86930E5663BA";
	setAttr ".t" -type "double3" 0 -1.2830301954670797 -2.9326750409831046 ;
	setAttr ".r" -type "double3" -37.586874782320805 0 180 ;
	setAttr ".s" -type "double3" 0.023012869104460574 1.2615368486637675 0.32458073594192532 ;
createNode mesh -n "pCubeShape18" -p "pCube18";
	rename -uid "A4BE6719-4481-EB5D-FEA7-40B3D83B2689";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.085778117179870605 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 26 ".uvst[0].uvsp[0:25]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.625 0.085778117 0.375 0.085778117 0.125 0.085778117
		 0.375 0.66422188 0.625 0.66422188 0.875 0.085778117 0.625 0.17778419 0.375 0.17778419
		 0.125 0.17778419 0.375 0.5722158 0.625 0.5722158 0.875 0.17778419;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 14 ".pt";
	setAttr ".pt[0]" -type "float3" 0 -0.011419426 0.14515497 ;
	setAttr ".pt[1]" -type "float3" 0 -0.011419426 0.14515497 ;
	setAttr ".pt[2]" -type "float3" 0 0.015012002 -0.060085893 ;
	setAttr ".pt[3]" -type "float3" 0 0.015012002 -0.060085893 ;
	setAttr ".pt[4]" -type "float3" 3.3306691e-16 -0.12453522 -0.12309758 ;
	setAttr ".pt[5]" -type "float3" -3.3306691e-16 -0.12453522 -0.12309758 ;
	setAttr ".pt[8]" -type "float3" 0 0.012905118 -0.12358664 ;
	setAttr ".pt[9]" -type "float3" 0 0.012905118 -0.12358664 ;
	setAttr ".pt[10]" -type "float3" 0 -0.046586595 -0.00020243879 ;
	setAttr ".pt[11]" -type "float3" 0 -0.046586595 -0.00020243879 ;
	setAttr ".pt[12]" -type "float3" -5.5511151e-17 0.0027553423 -0.098246261 ;
	setAttr ".pt[13]" -type "float3" 5.5511151e-17 0.0027553423 -0.098246261 ;
	setAttr ".pt[14]" -type "float3" 5.5511151e-17 -0.10106869 -0.011602171 ;
	setAttr ".pt[15]" -type "float3" -5.5511151e-17 -0.10106869 -0.011602171 ;
	setAttr -s 16 ".vt[0:15]"  -0.5 -0.19228154 0.5831573 0.5 -0.19228154 0.5831573
		 -0.5 0.1003958 0.072429478 0.5 0.1003958 0.072429478 -0.5 0.20285535 -0.51591229
		 0.5 0.20285535 -0.51591229 -0.5 -0.095532835 -0.20146108 0.5 -0.095532835 -0.20146108
		 0.5 -0.091860302 0.40792024 -0.5 -0.091860302 0.40792024 -0.5 0.0068478733 -0.30935323
		 0.5 0.0068478733 -0.30935323 0.5 0.015852071 0.21995999 -0.5 0.015852071 0.21995999
		 -0.5 0.11666197 -0.42507893 0.5 0.11666197 -0.42507893;
	setAttr -s 28 ".ed[0:27]"  0 1 0 2 3 0 4 5 0 6 7 0 0 9 0 1 8 0 2 4 0
		 3 5 0 4 14 0 5 15 0 6 0 0 7 1 0 8 12 0 9 13 0 8 9 1 10 6 0 9 10 1 11 7 0 10 11 1
		 11 8 1 12 3 0 13 2 0 12 13 1 14 10 0 13 14 1 15 11 0 14 15 1 15 12 1;
	setAttr -s 14 -ch 56 ".fc[0:13]" -type "polyFaces" 
		f 4 0 5 14 -5
		mu 0 4 0 1 14 15
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 18 17 -4 -16
		mu 0 4 17 18 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -18 19 -6
		mu 0 4 1 10 19 14
		f 4 10 4 16 15
		mu 0 4 12 0 15 16
		f 4 -15 12 22 -14
		mu 0 4 15 14 20 21
		f 4 -17 13 24 23
		mu 0 4 16 15 21 22
		f 4 26 25 -19 -24
		mu 0 4 23 24 18 17
		f 4 -20 -26 27 -13
		mu 0 4 14 19 25 20
		f 4 -23 20 -2 -22
		mu 0 4 21 20 3 2
		f 4 -25 21 6 8
		mu 0 4 22 21 2 13
		f 4 2 9 -27 -9
		mu 0 4 4 5 24 23
		f 4 -28 -10 -8 -21
		mu 0 4 20 25 11 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube19";
	rename -uid "28CAEBE8-4323-F2BB-6B10-6B980D2CAA99";
	setAttr ".t" -type "double3" 0 -1.140284613873918 -3.4150566615393072 ;
	setAttr ".r" -type "double3" -37.586874782320805 0 180 ;
	setAttr ".s" -type "double3" 0.023012869104460574 1.2615368486637675 0.32458073594192532 ;
createNode mesh -n "pCubeShape19" -p "pCube19";
	rename -uid "9F38F6DD-4FA0-9959-FD1D-508630F83940";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.085778117179870605 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 26 ".uvst[0].uvsp[0:25]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.625 0.085778117 0.375 0.085778117 0.125 0.085778117
		 0.375 0.66422188 0.625 0.66422188 0.875 0.085778117 0.625 0.17778419 0.375 0.17778419
		 0.125 0.17778419 0.375 0.5722158 0.625 0.5722158 0.875 0.17778419;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 14 ".pt";
	setAttr ".pt[0]" -type "float3" 0 -0.011419426 0.14515497 ;
	setAttr ".pt[1]" -type "float3" 0 -0.011419426 0.14515497 ;
	setAttr ".pt[2]" -type "float3" 0 0.015012002 -0.060085893 ;
	setAttr ".pt[3]" -type "float3" 0 0.015012002 -0.060085893 ;
	setAttr ".pt[4]" -type "float3" 3.3306691e-16 -0.12453522 -0.12309758 ;
	setAttr ".pt[5]" -type "float3" -3.3306691e-16 -0.12453522 -0.12309758 ;
	setAttr ".pt[8]" -type "float3" 0 0.012905118 -0.12358664 ;
	setAttr ".pt[9]" -type "float3" 0 0.012905118 -0.12358664 ;
	setAttr ".pt[10]" -type "float3" 0 -0.046586595 -0.00020243879 ;
	setAttr ".pt[11]" -type "float3" 0 -0.046586595 -0.00020243879 ;
	setAttr ".pt[12]" -type "float3" -5.5511151e-17 0.0027553423 -0.098246261 ;
	setAttr ".pt[13]" -type "float3" 5.5511151e-17 0.0027553423 -0.098246261 ;
	setAttr ".pt[14]" -type "float3" 5.5511151e-17 -0.10106869 -0.011602171 ;
	setAttr ".pt[15]" -type "float3" -5.5511151e-17 -0.10106869 -0.011602171 ;
	setAttr -s 16 ".vt[0:15]"  -0.5 -0.19228154 0.5831573 0.5 -0.19228154 0.5831573
		 -0.5 0.1003958 0.072429478 0.5 0.1003958 0.072429478 -0.5 0.20285535 -0.51591229
		 0.5 0.20285535 -0.51591229 -0.5 -0.095532835 -0.20146108 0.5 -0.095532835 -0.20146108
		 0.5 -0.091860302 0.40792024 -0.5 -0.091860302 0.40792024 -0.5 0.0068478733 -0.30935323
		 0.5 0.0068478733 -0.30935323 0.5 0.015852071 0.21995999 -0.5 0.015852071 0.21995999
		 -0.5 0.11666197 -0.42507893 0.5 0.11666197 -0.42507893;
	setAttr -s 28 ".ed[0:27]"  0 1 0 2 3 0 4 5 0 6 7 0 0 9 0 1 8 0 2 4 0
		 3 5 0 4 14 0 5 15 0 6 0 0 7 1 0 8 12 0 9 13 0 8 9 1 10 6 0 9 10 1 11 7 0 10 11 1
		 11 8 1 12 3 0 13 2 0 12 13 1 14 10 0 13 14 1 15 11 0 14 15 1 15 12 1;
	setAttr -s 14 -ch 56 ".fc[0:13]" -type "polyFaces" 
		f 4 0 5 14 -5
		mu 0 4 0 1 14 15
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 18 17 -4 -16
		mu 0 4 17 18 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -18 19 -6
		mu 0 4 1 10 19 14
		f 4 10 4 16 15
		mu 0 4 12 0 15 16
		f 4 -15 12 22 -14
		mu 0 4 15 14 20 21
		f 4 -17 13 24 23
		mu 0 4 16 15 21 22
		f 4 26 25 -19 -24
		mu 0 4 23 24 18 17
		f 4 -20 -26 27 -13
		mu 0 4 14 19 25 20
		f 4 -23 20 -2 -22
		mu 0 4 21 20 3 2
		f 4 -25 21 6 8
		mu 0 4 22 21 2 13
		f 4 2 9 -27 -9
		mu 0 4 4 5 24 23
		f 4 -28 -10 -8 -21
		mu 0 4 20 25 11 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube20";
	rename -uid "5644ADF2-4F4A-36B2-D102-1DA6E0E27CA4";
	setAttr ".t" -type "double3" 0 -0.95569981008965688 -3.9441997657208558 ;
	setAttr ".r" -type "double3" -37.586874782320805 0 180 ;
	setAttr ".s" -type "double3" 0.023012869104460574 1.2615368486637675 0.32458073594192532 ;
createNode mesh -n "pCubeShape20" -p "pCube20";
	rename -uid "3D2AFE00-46FC-74E4-EA19-7C836F839B66";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.085778117179870605 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 26 ".uvst[0].uvsp[0:25]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.625 0.085778117 0.375 0.085778117 0.125 0.085778117
		 0.375 0.66422188 0.625 0.66422188 0.875 0.085778117 0.625 0.17778419 0.375 0.17778419
		 0.125 0.17778419 0.375 0.5722158 0.625 0.5722158 0.875 0.17778419;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 14 ".pt";
	setAttr ".pt[0]" -type "float3" 0 -0.011419426 0.14515497 ;
	setAttr ".pt[1]" -type "float3" 0 -0.011419426 0.14515497 ;
	setAttr ".pt[2]" -type "float3" 0 0.015012002 -0.060085893 ;
	setAttr ".pt[3]" -type "float3" 0 0.015012002 -0.060085893 ;
	setAttr ".pt[4]" -type "float3" 3.3306691e-16 -0.12453522 -0.12309758 ;
	setAttr ".pt[5]" -type "float3" -3.3306691e-16 -0.12453522 -0.12309758 ;
	setAttr ".pt[8]" -type "float3" 0 0.012905118 -0.12358664 ;
	setAttr ".pt[9]" -type "float3" 0 0.012905118 -0.12358664 ;
	setAttr ".pt[10]" -type "float3" 0 -0.046586595 -0.00020243879 ;
	setAttr ".pt[11]" -type "float3" 0 -0.046586595 -0.00020243879 ;
	setAttr ".pt[12]" -type "float3" -5.5511151e-17 0.0027553423 -0.098246261 ;
	setAttr ".pt[13]" -type "float3" 5.5511151e-17 0.0027553423 -0.098246261 ;
	setAttr ".pt[14]" -type "float3" 5.5511151e-17 -0.10106869 -0.011602171 ;
	setAttr ".pt[15]" -type "float3" -5.5511151e-17 -0.10106869 -0.011602171 ;
	setAttr -s 16 ".vt[0:15]"  -0.5 -0.19228154 0.5831573 0.5 -0.19228154 0.5831573
		 -0.5 0.1003958 0.072429478 0.5 0.1003958 0.072429478 -0.5 0.20285535 -0.51591229
		 0.5 0.20285535 -0.51591229 -0.5 -0.095532835 -0.20146108 0.5 -0.095532835 -0.20146108
		 0.5 -0.091860302 0.40792024 -0.5 -0.091860302 0.40792024 -0.5 0.0068478733 -0.30935323
		 0.5 0.0068478733 -0.30935323 0.5 0.015852071 0.21995999 -0.5 0.015852071 0.21995999
		 -0.5 0.11666197 -0.42507893 0.5 0.11666197 -0.42507893;
	setAttr -s 28 ".ed[0:27]"  0 1 0 2 3 0 4 5 0 6 7 0 0 9 0 1 8 0 2 4 0
		 3 5 0 4 14 0 5 15 0 6 0 0 7 1 0 8 12 0 9 13 0 8 9 1 10 6 0 9 10 1 11 7 0 10 11 1
		 11 8 1 12 3 0 13 2 0 12 13 1 14 10 0 13 14 1 15 11 0 14 15 1 15 12 1;
	setAttr -s 14 -ch 56 ".fc[0:13]" -type "polyFaces" 
		f 4 0 5 14 -5
		mu 0 4 0 1 14 15
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 18 17 -4 -16
		mu 0 4 17 18 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -18 19 -6
		mu 0 4 1 10 19 14
		f 4 10 4 16 15
		mu 0 4 12 0 15 16
		f 4 -15 12 22 -14
		mu 0 4 15 14 20 21
		f 4 -17 13 24 23
		mu 0 4 16 15 21 22
		f 4 26 25 -19 -24
		mu 0 4 23 24 18 17
		f 4 -20 -26 27 -13
		mu 0 4 14 19 25 20
		f 4 -23 20 -2 -22
		mu 0 4 21 20 3 2
		f 4 -25 21 6 8
		mu 0 4 22 21 2 13
		f 4 2 9 -27 -9
		mu 0 4 4 5 24 23
		f 4 -28 -10 -8 -21
		mu 0 4 20 25 11 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube21";
	rename -uid "F4682DE2-4890-9F30-9D76-10B76649AD8E";
	setAttr ".t" -type "double3" 0 -0.80117753235553391 -4.459274024834599 ;
	setAttr ".r" -type "double3" -37.586874782320805 0 180 ;
	setAttr ".s" -type "double3" 0.023012869104460574 1.2615368486637675 0.32458073594192532 ;
createNode mesh -n "pCubeShape21" -p "pCube21";
	rename -uid "6A0F0523-485F-512E-A061-40B15200C076";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.085778117179870605 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 26 ".uvst[0].uvsp[0:25]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.625 0.085778117 0.375 0.085778117 0.125 0.085778117
		 0.375 0.66422188 0.625 0.66422188 0.875 0.085778117 0.625 0.17778419 0.375 0.17778419
		 0.125 0.17778419 0.375 0.5722158 0.625 0.5722158 0.875 0.17778419;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 14 ".pt";
	setAttr ".pt[0]" -type "float3" 0 -0.011419426 0.14515497 ;
	setAttr ".pt[1]" -type "float3" 0 -0.011419426 0.14515497 ;
	setAttr ".pt[2]" -type "float3" 0 0.015012002 -0.060085893 ;
	setAttr ".pt[3]" -type "float3" 0 0.015012002 -0.060085893 ;
	setAttr ".pt[4]" -type "float3" 3.3306691e-16 -0.12453522 -0.12309758 ;
	setAttr ".pt[5]" -type "float3" -3.3306691e-16 -0.12453522 -0.12309758 ;
	setAttr ".pt[8]" -type "float3" 0 0.012905118 -0.12358664 ;
	setAttr ".pt[9]" -type "float3" 0 0.012905118 -0.12358664 ;
	setAttr ".pt[10]" -type "float3" 0 -0.046586595 -0.00020243879 ;
	setAttr ".pt[11]" -type "float3" 0 -0.046586595 -0.00020243879 ;
	setAttr ".pt[12]" -type "float3" -5.5511151e-17 0.0027553423 -0.098246261 ;
	setAttr ".pt[13]" -type "float3" 5.5511151e-17 0.0027553423 -0.098246261 ;
	setAttr ".pt[14]" -type "float3" 5.5511151e-17 -0.10106869 -0.011602171 ;
	setAttr ".pt[15]" -type "float3" -5.5511151e-17 -0.10106869 -0.011602171 ;
	setAttr -s 16 ".vt[0:15]"  -0.5 -0.19228154 0.5831573 0.5 -0.19228154 0.5831573
		 -0.5 0.1003958 0.072429478 0.5 0.1003958 0.072429478 -0.5 0.20285535 -0.51591229
		 0.5 0.20285535 -0.51591229 -0.5 -0.095532835 -0.20146108 0.5 -0.095532835 -0.20146108
		 0.5 -0.091860302 0.40792024 -0.5 -0.091860302 0.40792024 -0.5 0.0068478733 -0.30935323
		 0.5 0.0068478733 -0.30935323 0.5 0.015852071 0.21995999 -0.5 0.015852071 0.21995999
		 -0.5 0.11666197 -0.42507893 0.5 0.11666197 -0.42507893;
	setAttr -s 28 ".ed[0:27]"  0 1 0 2 3 0 4 5 0 6 7 0 0 9 0 1 8 0 2 4 0
		 3 5 0 4 14 0 5 15 0 6 0 0 7 1 0 8 12 0 9 13 0 8 9 1 10 6 0 9 10 1 11 7 0 10 11 1
		 11 8 1 12 3 0 13 2 0 12 13 1 14 10 0 13 14 1 15 11 0 14 15 1 15 12 1;
	setAttr -s 14 -ch 56 ".fc[0:13]" -type "polyFaces" 
		f 4 0 5 14 -5
		mu 0 4 0 1 14 15
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 18 17 -4 -16
		mu 0 4 17 18 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -18 19 -6
		mu 0 4 1 10 19 14
		f 4 10 4 16 15
		mu 0 4 12 0 15 16
		f 4 -15 12 22 -14
		mu 0 4 15 14 20 21
		f 4 -17 13 24 23
		mu 0 4 16 15 21 22
		f 4 26 25 -19 -24
		mu 0 4 23 24 18 17
		f 4 -20 -26 27 -13
		mu 0 4 14 19 25 20
		f 4 -23 20 -2 -22
		mu 0 4 21 20 3 2
		f 4 -25 21 6 8
		mu 0 4 22 21 2 13
		f 4 2 9 -27 -9
		mu 0 4 4 5 24 23
		f 4 -28 -10 -8 -21
		mu 0 4 20 25 11 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube22";
	rename -uid "8F2A58D3-457F-1697-7A2F-78B2974ABEC4";
	setAttr ".t" -type "double3" 0 -0.6681166820844836 -4.8026568642437608 ;
	setAttr ".r" -type "double3" -37.586874782320805 0 180 ;
	setAttr ".s" -type "double3" 0.023012869104460574 1.2615368486637675 0.32458073594192532 ;
createNode mesh -n "pCubeShape22" -p "pCube22";
	rename -uid "F32E4B19-4A4E-0BC5-C062-138ACDB015A4";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.085778117179870605 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 26 ".uvst[0].uvsp[0:25]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.625 0.085778117 0.375 0.085778117 0.125 0.085778117
		 0.375 0.66422188 0.625 0.66422188 0.875 0.085778117 0.625 0.17778419 0.375 0.17778419
		 0.125 0.17778419 0.375 0.5722158 0.625 0.5722158 0.875 0.17778419;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 14 ".pt";
	setAttr ".pt[0]" -type "float3" 0 -0.011419426 0.14515497 ;
	setAttr ".pt[1]" -type "float3" 0 -0.011419426 0.14515497 ;
	setAttr ".pt[2]" -type "float3" 0 0.015012002 -0.060085893 ;
	setAttr ".pt[3]" -type "float3" 0 0.015012002 -0.060085893 ;
	setAttr ".pt[4]" -type "float3" 3.3306691e-16 -0.12453522 -0.12309758 ;
	setAttr ".pt[5]" -type "float3" -3.3306691e-16 -0.12453522 -0.12309758 ;
	setAttr ".pt[8]" -type "float3" 0 0.012905118 -0.12358664 ;
	setAttr ".pt[9]" -type "float3" 0 0.012905118 -0.12358664 ;
	setAttr ".pt[10]" -type "float3" 0 -0.046586595 -0.00020243879 ;
	setAttr ".pt[11]" -type "float3" 0 -0.046586595 -0.00020243879 ;
	setAttr ".pt[12]" -type "float3" -5.5511151e-17 0.0027553423 -0.098246261 ;
	setAttr ".pt[13]" -type "float3" 5.5511151e-17 0.0027553423 -0.098246261 ;
	setAttr ".pt[14]" -type "float3" 5.5511151e-17 -0.10106869 -0.011602171 ;
	setAttr ".pt[15]" -type "float3" -5.5511151e-17 -0.10106869 -0.011602171 ;
	setAttr -s 16 ".vt[0:15]"  -0.5 -0.19228154 0.5831573 0.5 -0.19228154 0.5831573
		 -0.5 0.1003958 0.072429478 0.5 0.1003958 0.072429478 -0.5 0.20285535 -0.51591229
		 0.5 0.20285535 -0.51591229 -0.5 -0.095532835 -0.20146108 0.5 -0.095532835 -0.20146108
		 0.5 -0.091860302 0.40792024 -0.5 -0.091860302 0.40792024 -0.5 0.0068478733 -0.30935323
		 0.5 0.0068478733 -0.30935323 0.5 0.015852071 0.21995999 -0.5 0.015852071 0.21995999
		 -0.5 0.11666197 -0.42507893 0.5 0.11666197 -0.42507893;
	setAttr -s 28 ".ed[0:27]"  0 1 0 2 3 0 4 5 0 6 7 0 0 9 0 1 8 0 2 4 0
		 3 5 0 4 14 0 5 15 0 6 0 0 7 1 0 8 12 0 9 13 0 8 9 1 10 6 0 9 10 1 11 7 0 10 11 1
		 11 8 1 12 3 0 13 2 0 12 13 1 14 10 0 13 14 1 15 11 0 14 15 1 15 12 1;
	setAttr -s 14 -ch 56 ".fc[0:13]" -type "polyFaces" 
		f 4 0 5 14 -5
		mu 0 4 0 1 14 15
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 18 17 -4 -16
		mu 0 4 17 18 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -18 19 -6
		mu 0 4 1 10 19 14
		f 4 10 4 16 15
		mu 0 4 12 0 15 16
		f 4 -15 12 22 -14
		mu 0 4 15 14 20 21
		f 4 -17 13 24 23
		mu 0 4 16 15 21 22
		f 4 26 25 -19 -24
		mu 0 4 23 24 18 17
		f 4 -20 -26 27 -13
		mu 0 4 14 19 25 20
		f 4 -23 20 -2 -22
		mu 0 4 21 20 3 2
		f 4 -25 21 6 8
		mu 0 4 22 21 2 13
		f 4 2 9 -27 -9
		mu 0 4 4 5 24 23
		f 4 -28 -10 -8 -21
		mu 0 4 20 25 11 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube23";
	rename -uid "63195AFE-4C40-E943-2E4D-498507C8C009";
	setAttr ".t" -type "double3" 0 -0.50930211885774601 -5.317731123357504 ;
	setAttr ".r" -type "double3" -37.586874782320805 0 180 ;
	setAttr ".s" -type "double3" 0.023012869104460574 1.2615368486637675 0.32458073594192532 ;
createNode mesh -n "pCubeShape23" -p "pCube23";
	rename -uid "EA889999-4E44-5F63-7391-EBAC50B9D57E";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.085778117179870605 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 26 ".uvst[0].uvsp[0:25]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.625 0.085778117 0.375 0.085778117 0.125 0.085778117
		 0.375 0.66422188 0.625 0.66422188 0.875 0.085778117 0.625 0.17778419 0.375 0.17778419
		 0.125 0.17778419 0.375 0.5722158 0.625 0.5722158 0.875 0.17778419;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 14 ".pt";
	setAttr ".pt[0]" -type "float3" 0 -0.011419426 0.14515497 ;
	setAttr ".pt[1]" -type "float3" 0 -0.011419426 0.14515497 ;
	setAttr ".pt[2]" -type "float3" 0 0.015012002 -0.060085893 ;
	setAttr ".pt[3]" -type "float3" 0 0.015012002 -0.060085893 ;
	setAttr ".pt[4]" -type "float3" 3.3306691e-16 -0.12453522 -0.12309758 ;
	setAttr ".pt[5]" -type "float3" -3.3306691e-16 -0.12453522 -0.12309758 ;
	setAttr ".pt[8]" -type "float3" 0 0.012905118 -0.12358664 ;
	setAttr ".pt[9]" -type "float3" 0 0.012905118 -0.12358664 ;
	setAttr ".pt[10]" -type "float3" 0 -0.046586595 -0.00020243879 ;
	setAttr ".pt[11]" -type "float3" 0 -0.046586595 -0.00020243879 ;
	setAttr ".pt[12]" -type "float3" -5.5511151e-17 0.0027553423 -0.098246261 ;
	setAttr ".pt[13]" -type "float3" 5.5511151e-17 0.0027553423 -0.098246261 ;
	setAttr ".pt[14]" -type "float3" 5.5511151e-17 -0.10106869 -0.011602171 ;
	setAttr ".pt[15]" -type "float3" -5.5511151e-17 -0.10106869 -0.011602171 ;
	setAttr -s 16 ".vt[0:15]"  -0.5 -0.19228154 0.5831573 0.5 -0.19228154 0.5831573
		 -0.5 0.1003958 0.072429478 0.5 0.1003958 0.072429478 -0.5 0.20285535 -0.51591229
		 0.5 0.20285535 -0.51591229 -0.5 -0.095532835 -0.20146108 0.5 -0.095532835 -0.20146108
		 0.5 -0.091860302 0.40792024 -0.5 -0.091860302 0.40792024 -0.5 0.0068478733 -0.30935323
		 0.5 0.0068478733 -0.30935323 0.5 0.015852071 0.21995999 -0.5 0.015852071 0.21995999
		 -0.5 0.11666197 -0.42507893 0.5 0.11666197 -0.42507893;
	setAttr -s 28 ".ed[0:27]"  0 1 0 2 3 0 4 5 0 6 7 0 0 9 0 1 8 0 2 4 0
		 3 5 0 4 14 0 5 15 0 6 0 0 7 1 0 8 12 0 9 13 0 8 9 1 10 6 0 9 10 1 11 7 0 10 11 1
		 11 8 1 12 3 0 13 2 0 12 13 1 14 10 0 13 14 1 15 11 0 14 15 1 15 12 1;
	setAttr -s 14 -ch 56 ".fc[0:13]" -type "polyFaces" 
		f 4 0 5 14 -5
		mu 0 4 0 1 14 15
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 18 17 -4 -16
		mu 0 4 17 18 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -18 19 -6
		mu 0 4 1 10 19 14
		f 4 10 4 16 15
		mu 0 4 12 0 15 16
		f 4 -15 12 22 -14
		mu 0 4 15 14 20 21
		f 4 -17 13 24 23
		mu 0 4 16 15 21 22
		f 4 26 25 -19 -24
		mu 0 4 23 24 18 17
		f 4 -20 -26 27 -13
		mu 0 4 14 19 25 20
		f 4 -23 20 -2 -22
		mu 0 4 21 20 3 2
		f 4 -25 21 6 8
		mu 0 4 22 21 2 13
		f 4 2 9 -27 -9
		mu 0 4 4 5 24 23
		f 4 -28 -10 -8 -21
		mu 0 4 20 25 11 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube24";
	rename -uid "9DBADA24-4F6D-6983-018F-DDB4CCCC3455";
	setAttr ".t" -type "double3" 0 -0.35907212661623766 -5.7898825275451022 ;
	setAttr ".r" -type "double3" -37.586874782320805 0 180 ;
	setAttr ".s" -type "double3" 0.023012869104460574 1.2615368486637675 0.32458073594192532 ;
createNode mesh -n "pCubeShape24" -p "pCube24";
	rename -uid "7D0702DE-4CCE-3798-BAB4-D5B4F3DF0CEA";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.085778117179870605 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 26 ".uvst[0].uvsp[0:25]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.625 0.085778117 0.375 0.085778117 0.125 0.085778117
		 0.375 0.66422188 0.625 0.66422188 0.875 0.085778117 0.625 0.17778419 0.375 0.17778419
		 0.125 0.17778419 0.375 0.5722158 0.625 0.5722158 0.875 0.17778419;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 14 ".pt";
	setAttr ".pt[0]" -type "float3" 0 -0.011419426 0.14515497 ;
	setAttr ".pt[1]" -type "float3" 0 -0.011419426 0.14515497 ;
	setAttr ".pt[2]" -type "float3" 0 0.015012002 -0.060085893 ;
	setAttr ".pt[3]" -type "float3" 0 0.015012002 -0.060085893 ;
	setAttr ".pt[4]" -type "float3" 3.3306691e-16 -0.12453522 -0.12309758 ;
	setAttr ".pt[5]" -type "float3" -3.3306691e-16 -0.12453522 -0.12309758 ;
	setAttr ".pt[8]" -type "float3" 0 0.012905118 -0.12358664 ;
	setAttr ".pt[9]" -type "float3" 0 0.012905118 -0.12358664 ;
	setAttr ".pt[10]" -type "float3" 0 -0.046586595 -0.00020243879 ;
	setAttr ".pt[11]" -type "float3" 0 -0.046586595 -0.00020243879 ;
	setAttr ".pt[12]" -type "float3" -5.5511151e-17 0.0027553423 -0.098246261 ;
	setAttr ".pt[13]" -type "float3" 5.5511151e-17 0.0027553423 -0.098246261 ;
	setAttr ".pt[14]" -type "float3" 5.5511151e-17 -0.10106869 -0.011602171 ;
	setAttr ".pt[15]" -type "float3" -5.5511151e-17 -0.10106869 -0.011602171 ;
	setAttr -s 16 ".vt[0:15]"  -0.5 -0.19228154 0.5831573 0.5 -0.19228154 0.5831573
		 -0.5 0.1003958 0.072429478 0.5 0.1003958 0.072429478 -0.5 0.20285535 -0.51591229
		 0.5 0.20285535 -0.51591229 -0.5 -0.095532835 -0.20146108 0.5 -0.095532835 -0.20146108
		 0.5 -0.091860302 0.40792024 -0.5 -0.091860302 0.40792024 -0.5 0.0068478733 -0.30935323
		 0.5 0.0068478733 -0.30935323 0.5 0.015852071 0.21995999 -0.5 0.015852071 0.21995999
		 -0.5 0.11666197 -0.42507893 0.5 0.11666197 -0.42507893;
	setAttr -s 28 ".ed[0:27]"  0 1 0 2 3 0 4 5 0 6 7 0 0 9 0 1 8 0 2 4 0
		 3 5 0 4 14 0 5 15 0 6 0 0 7 1 0 8 12 0 9 13 0 8 9 1 10 6 0 9 10 1 11 7 0 10 11 1
		 11 8 1 12 3 0 13 2 0 12 13 1 14 10 0 13 14 1 15 11 0 14 15 1 15 12 1;
	setAttr -s 14 -ch 56 ".fc[0:13]" -type "polyFaces" 
		f 4 0 5 14 -5
		mu 0 4 0 1 14 15
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 18 17 -4 -16
		mu 0 4 17 18 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -18 19 -6
		mu 0 4 1 10 19 14
		f 4 10 4 16 15
		mu 0 4 12 0 15 16
		f 4 -15 12 22 -14
		mu 0 4 15 14 20 21
		f 4 -17 13 24 23
		mu 0 4 16 15 21 22
		f 4 26 25 -19 -24
		mu 0 4 23 24 18 17
		f 4 -20 -26 27 -13
		mu 0 4 14 19 25 20
		f 4 -23 20 -2 -22
		mu 0 4 21 20 3 2
		f 4 -25 21 6 8
		mu 0 4 22 21 2 13
		f 4 2 9 -27 -9
		mu 0 4 4 5 24 23
		f 4 -28 -10 -8 -21
		mu 0 4 20 25 11 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode lightLinker -s -n "lightLinker1";
	rename -uid "2D299EEB-4B09-3410-ACCD-80AFC50468D6";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "85EA4012-46A2-D20A-E89A-22ACB1B80D47";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "8B85E393-42C5-54C1-1424-8F9290D8E25D";
createNode displayLayerManager -n "layerManager";
	rename -uid "B2703906-4D29-342D-886F-46B2BEC6715D";
createNode displayLayer -n "defaultLayer";
	rename -uid "72928F28-43F1-0126-59A7-D08DB4B4A83A";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "BBA5E13C-46AF-D5D4-3001-549D33DAE495";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "B1E8F27C-411B-2050-0DC8-6A8C202DF0AC";
	setAttr ".g" yes;
createNode polyCube -n "polyCube1";
	rename -uid "38AE9222-43BC-35D5-2ABA-3582F000E606";
	setAttr ".cuv" 4;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "47114EB1-4BAC-882E-9E31-C48CF33AE9CB";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n"
		+ "            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n"
		+ "            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n"
		+ "            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 602\n            -height 336\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n"
		+ "            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 620\n            -height 336\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n"
		+ "            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n"
		+ "            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 583\n            -height 336\n"
		+ "            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n"
		+ "            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n"
		+ "            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n"
		+ "            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 601\n            -height 336\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -docTag \"isolOutln_fromSeln\" \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n"
		+ "            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n"
		+ "            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n"
		+ "            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n"
		+ "                -showParentContainers 1\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n"
		+ "                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 1\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n"
		+ "                -showCurveNames 0\n                -showActiveCurveNames 0\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                -valueLinesToggle 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n"
		+ "                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 1\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n"
		+ "                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n"
		+ "                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n"
		+ "                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n"
		+ "                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -highlightConnections 0\n                -copyConnectionsOnPaste 0\n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n"
		+ "                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"quad\\\" -ps 1 50 50 -ps 2 50 50 -ps 3 51 50 -ps 4 49 50 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Top View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Top View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera top` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 602\\n    -height 336\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Top View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera top` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 602\\n    -height 336\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 601\\n    -height 336\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 601\\n    -height 336\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Side View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Side View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera side` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"wireframe\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 620\\n    -height 336\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Side View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera side` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"wireframe\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 620\\n    -height 336\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Front View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Front View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera front` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 583\\n    -height 336\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Front View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera front` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 583\\n    -height 336\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "F867B716-4A27-EB07-DC4D-C4B521C055B0";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode polySplitRing -n "polySplitRing1";
	rename -uid "728C1B00-4C29-7D74-D64C-BF863C151914";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[6:7]" "e[10:11]";
	setAttr ".ix" -type "matrix" 4.3541510445179226 0 0 0 0 4.3541510445179226 0 0 0 0 15.384666766119331 0
		 0 0 0.084883071105291502 1;
	setAttr ".wt" 0.075710885226726532;
	setAttr ".re" 7;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak1";
	rename -uid "25716F0E-488B-5011-5467-488A6F8D581E";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk[0:7]" -type "float3"  0 -0.062332969 -0.097615786
		 0 -0.062332969 -0.097615786 0 -0.054021902 -0.097615786 0 -0.054021902 -0.097615786
		 0 -0.054021902 0.060691189 0 -0.054021902 0.060691189 0 -0.062332969 0.060691189
		 0 -0.062332969 0.060691189;
createNode polySplitRing -n "polySplitRing2";
	rename -uid "2DE003AD-4F14-4A27-1DD9-A2961A524FF5";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[10:13]";
	setAttr ".ix" -type "matrix" 4.3541510445179226 0 0 0 0 4.3541510445179226 0 0 0 0 15.384666766119331 0
		 0 0 0.084883071105291502 1;
	setAttr ".wt" 0.10899649560451508;
	setAttr ".re" 12;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing3";
	rename -uid "EBE54691-4524-FA85-5B46-BFBFB32CA794";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[10:11]" "e[20:21]";
	setAttr ".ix" -type "matrix" 4.3541510445179226 0 0 0 0 4.3541510445179226 0 0 0 0 15.384666766119331 0
		 0 0 0.084883071105291502 1;
	setAttr ".wt" 0.095417387783527374;
	setAttr ".re" 20;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing4";
	rename -uid "62CC3669-4D50-EBEF-0629-9D801AF26853";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[10:11]" "e[28:29]";
	setAttr ".ix" -type "matrix" 4.3541510445179226 0 0 0 0 4.3541510445179226 0 0 0 0 15.384666766119331 0
		 0 0 0.084883071105291502 1;
	setAttr ".wt" 0.10818695276975632;
	setAttr ".re" 28;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing5";
	rename -uid "FC469DB3-43B7-2E50-D911-93847B575128";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[10:11]" "e[36:37]";
	setAttr ".ix" -type "matrix" 4.3541510445179226 0 0 0 0 4.3541510445179226 0 0 0 0 15.384666766119331 0
		 0 0 0.084883071105291502 1;
	setAttr ".wt" 0.11827845871448517;
	setAttr ".re" 36;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing6";
	rename -uid "8F593B08-4AAA-45BB-A578-508D9E507AD5";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[10:11]" "e[44:45]";
	setAttr ".ix" -type "matrix" 4.3541510445179226 0 0 0 0 4.3541510445179226 0 0 0 0 15.384666766119331 0
		 0 0 0.084883071105291502 1;
	setAttr ".wt" 0.12038645893335342;
	setAttr ".re" 44;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing7";
	rename -uid "41C4FBCE-4A9A-104C-05F1-729A1F5CD393";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[10:11]" "e[52:53]";
	setAttr ".ix" -type "matrix" 4.3541510445179226 0 0 0 0 4.3541510445179226 0 0 0 0 15.384666766119331 0
		 0 0 0.084883071105291502 1;
	setAttr ".wt" 0.25808429718017578;
	setAttr ".re" 52;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing8";
	rename -uid "03922440-4070-AA71-B0C1-CF8E1DEE9B5A";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[10:11]" "e[60:61]";
	setAttr ".ix" -type "matrix" 4.3541510445179226 0 0 0 0 4.3541510445179226 0 0 0 0 15.384666766119331 0
		 0 0 0.084883071105291502 1;
	setAttr ".wt" 0.23190802335739136;
	setAttr ".re" 60;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing9";
	rename -uid "D46050F6-4F1F-AB69-0DFE-F6A5B076AF94";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[10:11]" "e[68:69]";
	setAttr ".ix" -type "matrix" 4.3541510445179226 0 0 0 0 4.3541510445179226 0 0 0 0 15.384666766119331 0
		 0 0 0.084883071105291502 1;
	setAttr ".wt" 0.50778704881668091;
	setAttr ".dr" no;
	setAttr ".re" 68;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing10";
	rename -uid "D0844181-44F0-C0F6-8949-209385C7C55A";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[10:11]" "e[76:77]";
	setAttr ".ix" -type "matrix" 4.3541510445179226 0 0 0 0 4.3541510445179226 0 0 0 0 15.384666766119331 0
		 0 0 0.084883071105291502 1;
	setAttr ".wt" 0.7488715648651123;
	setAttr ".dr" no;
	setAttr ".re" 76;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak2";
	rename -uid "1D61413E-4979-B30C-E2B1-87B7CD2EDFFF";
	setAttr ".uopa" yes;
	setAttr -s 44 ".tk[0:43]" -type "float3"  0 0.14926326 0 0 0.14926326
		 0 0 -0.27344719 0 0 -0.27344719 0 0 -0.45332849 -0.0061859521 0 -0.45332849 -0.0061859521
		 0 0.54045963 -0.0061859521 0 0.54045963 -0.0061859521 0 -0.1616759 0 0 -0.1616759
		 0 0 0.084800169 0 0 0.084800169 0 0 -0.059597615 0 0 -0.059597615 0 0 0.030030023
		 0 0 0.030030023 0 0 -0.01872598 0 0 -0.01872598 0 0 0.0036809323 0 0 0.0036809323
		 0 0 0.0019207937 0 0 0.0019207937 0 0 0.01312425 0 0 0.01312425 0 0 -0.018645652
		 0 0 -0.018645652 0 0 0.026168175 0 0 0.026168175 0 0 -0.046573959 0 0 -0.046573959
		 0 0 0.07666406 0 0 0.07666406 0 0 -0.13980222 0 0 -0.13980222 0 0 0.20750493 0 0
		 0.20750493 0 0 -0.20883667 0 0 -0.20883667 0 0 0.29158443 0 0 0.29158443 0 0 -0.34138411
		 0 0 -0.34138411 0 0 0.43165439 0 0 0.43165439 0;
createNode polySplitRing -n "polySplitRing11";
	rename -uid "E251B93E-4293-CD9A-4A91-29837269446F";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[76:77]" "e[87]" "e[89]";
	setAttr ".ix" -type "matrix" 4.3541510445179226 0 0 0 0 4.3541510445179226 0 0 0 0 15.384666766119331 0
		 0 0 0.084883071105291502 1;
	setAttr ".wt" 0.66154932975769043;
	setAttr ".dr" no;
	setAttr ".re" 76;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak3";
	rename -uid "D0320480-49CB-4F79-35DF-618C4535C119";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[44:47]" -type "float3"  0 0.016613605 0 0 0.016613605
		 0 0 -0.0020422544 0 0 -0.0020422544 0;
createNode polySplitRing -n "polySplitRing12";
	rename -uid "F7657196-482A-C554-B2BE-A9B35B13646E";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[87]" "e[89]" "e[92:93]";
	setAttr ".ix" -type "matrix" 4.3541510445179226 0 0 0 0 4.3541510445179226 0 0 0 0 15.384666766119331 0
		 0 0 0.084883071105291502 1;
	setAttr ".wt" 0.48138365149497986;
	setAttr ".re" 92;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak4";
	rename -uid "C0A306E4-481E-329D-58AA-B2BEC4CAE259";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[48:51]" -type "float3"  0 -0.020737737 0 0 -0.020737737
		 0 0 0.020737734 0 0 0.020737734 0;
createNode polySplitRing -n "polySplitRing13";
	rename -uid "E4615537-4BD6-6090-8087-8F844E71BA8D";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[10:11]" "e[84:85]";
	setAttr ".ix" -type "matrix" 4.3541510445179226 0 0 0 0 4.3541510445179226 0 0 0 0 15.384666766119331 0
		 0 0 0.084883071105291502 1;
	setAttr ".wt" 0.46884018182754517;
	setAttr ".re" 84;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak5";
	rename -uid "54EA4611-4C9B-1EE1-725E-3B89D93F858D";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[52:55]" -type "float3"  0 -0.0082531245 0 0 -0.0082531245
		 0 0 0.0082531245 0 0 0.0082531245 0;
createNode polySplitRing -n "polySplitRing14";
	rename -uid "C643B363-4929-2620-6232-6F9555474E22";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 28 "e[4:5]" "e[8:9]" "e[16]" "e[19]" "e[24]" "e[27]" "e[32]" "e[35]" "e[40]" "e[43]" "e[48]" "e[51]" "e[56]" "e[59]" "e[64]" "e[67]" "e[72]" "e[75]" "e[80]" "e[83]" "e[88]" "e[91]" "e[96]" "e[99]" "e[104]" "e[107]" "e[112]" "e[115]";
	setAttr ".ix" -type "matrix" 4.3541510445179226 0 0 0 0 4.3541510445179226 0 0 0 0 15.384666766119331 0
		 0 0 0.084883071105291502 1;
	setAttr ".wt" 0.32831317186355591;
	setAttr ".re" 5;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak6";
	rename -uid "234630CB-4544-9948-8230-999EE22EF945";
	setAttr ".uopa" yes;
	setAttr -s 60 ".tk[0:59]" -type "float3"  0.2888889 0 0 -0.2888889 0
		 0 0.28888893 0 0 -0.28888893 0 0 0.49416724 0 0 -0.49416724 0 0 0.49416724 0 0 -0.49416724
		 0 0 -0.22772436 0 0 0.22772436 0 0 0.22772436 0 0 -0.22772436 0 0 -0.13499998 0 0
		 0.13499998 0 0 0.13499998 0 0 -0.13499998 0 0 -0.095246911 0 0 0.095246911 0 0 0.095246911
		 0 0 -0.095246911 0 0 -0.049999997 0 0 0.049999997 0 0 0.049999997 0 0 -0.049999997
		 0 0 -0.059629638 0 0 0.059629638 0 0 0.059629638 0 0 -0.059629638 0 0 -0.088700287
		 0 0 0.088700287 0 0 0.088700287 0 0 -0.088700287 0 0 -0.18566671 0 0 0.18566671 0
		 0 0.1856667 0 0 -0.1856667 0 0 -0.27222225 0 0 0.27222225 0 0 0.27222225 0 0 -0.27222225
		 0 0 -0.39728415 0 0 0.39728415 0 0 0.39728415 0 0 -0.39728415 0 0 -0.45558789 0 0
		 0.45558789 0 0 0.45558789 0 0 -0.45558789 0 0 -0.47777778 0 0 0.47777778 0 0 0.47777778
		 0 0 -0.47777778 0 0 -0.45876554 0 0 0.45876554 0 0 0.45876554 0 0 -0.45876554 0 0
		 -0.46548504 0.010775572 0 0.46548504 0.010775572 0 0.46548504 -0.010775574 0 -0.46548504
		 -0.010775574 0;
createNode polyExtrudeFace -n "polyExtrudeFace1";
	rename -uid "508C05D3-41FB-D8F6-B9EB-86BC290CF16F";
	setAttr ".ics" -type "componentList" 1 "f[58]";
	setAttr ".ix" -type "matrix" 4.3541510445179226 0 0 0 0 4.3541510445179226 0 0 0 0 15.384666766119331 0
		 0 0 0.084883071105291502 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0 -0.10510571 6.2754302 ;
	setAttr ".rs" 63161;
	setAttr ".lt" -type "double3" 0 0 1.5209098275711241 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.91920954528271148 -0.96143709071338534 6.2754302316548749 ;
	setAttr ".cbx" -type "double3" 0.91920954528271148 0.75122568254021205 6.2754302316548749 ;
createNode polyExtrudeFace -n "polyExtrudeFace2";
	rename -uid "78F58CCF-4553-A7AE-B440-4AA9E5003B9B";
	setAttr ".ics" -type "componentList" 1 "f[0]";
	setAttr ".ix" -type "matrix" 4.3541510445179226 0 0 0 0 4.3541510445179226 0 0 0 0 15.384666766119331 0
		 0 0 0.084883071105291502 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0 -1.3800025 6.2754297 ;
	setAttr ".rs" 42031;
	setAttr ".lt" -type "double3" 0 0 1.2765736910309906 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.91920961016461811 -1.7985679747269347 6.275429773156076 ;
	setAttr ".cbx" -type "double3" 0.91920961016461811 -0.96143709071338534 6.275429773156076 ;
createNode polyTweak -n "polyTweak7";
	rename -uid "40F77D1F-4DED-5A89-17FC-258AB3323647";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[90:93]" -type "float3"  0 0.12856005 -0.0052156989
		 0 0.12856005 -0.0052156989 0 -0.20897678 -0.0052156989 0 -0.20897678 -0.0052156989;
createNode polySplitRing -n "polySplitRing15";
	rename -uid "327AFEC2-404A-CC61-6B21-5B9E416185AD";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 30 "e[8:9]" "e[16]" "e[24]" "e[32]" "e[40]" "e[48]" "e[56]" "e[64]" "e[72]" "e[80]" "e[88]" "e[96]" "e[104]" "e[112]" "e[116:117]" "e[149]" "e[151]" "e[153]" "e[155]" "e[157]" "e[159]" "e[161]" "e[163]" "e[165]" "e[167]" "e[169]" "e[171]" "e[173]" "e[180]" "e[183]";
	setAttr ".ix" -type "matrix" 4.3541510445179226 0 0 0 0 4.3541510445179226 0 0 0 0 15.384666766119331 0
		 0 0 0.084883071105291502 1;
	setAttr ".wt" 0.22686296701431274;
	setAttr ".re" 167;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak8";
	rename -uid "814307BE-4BAB-9959-62E0-9AA0C4D1A725";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk[90:97]" -type "float3"  -0.13486753 0 0 0.13486753
		 0 0 -0.13486753 0 0 0.13486753 0 0 0.157425 0.16722648 0.012802169 -0.157425 0.16722648
		 0.012802169 -0.15742499 0.007009455 0.012802169 0.15742499 0.007009455 0.012802169;
createNode polySplitRing -n "polySplitRing16";
	rename -uid "4F434939-4B7B-A823-7716-F0B23C28444E";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 30 "e[8:9]" "e[16]" "e[24]" "e[32]" "e[40]" "e[48]" "e[56]" "e[64]" "e[72]" "e[80]" "e[88]" "e[96]" "e[104]" "e[112]" "e[192:193]" "e[195]" "e[197]" "e[199]" "e[201]" "e[203]" "e[205]" "e[237]" "e[239]" "e[241]" "e[243]" "e[245]" "e[247]" "e[249]" "e[251]" "e[253]";
	setAttr ".ix" -type "matrix" 4.3541510445179226 0 0 0 0 4.3541510445179226 0 0 0 0 15.384666766119331 0
		 0 0 0.084883071105291502 1;
	setAttr ".wt" 0.56455534696578979;
	setAttr ".dr" no;
	setAttr ".re" 195;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing17";
	rename -uid "EB5FE2BC-4BA8-9D12-EFB1-01A29733850D";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 31 "e[4:5]" "e[19]" "e[27]" "e[35]" "e[43]" "e[51]" "e[59]" "e[67]" "e[75]" "e[83]" "e[91]" "e[99]" "e[107]" "e[115]" "e[119]" "e[121]" "e[123]" "e[125]" "e[127]" "e[129]" "e[131]" "e[133]" "e[135]" "e[137]" "e[139]" "e[141]" "e[143]" "e[145]" "e[147]" "e[188]" "e[191]";
	setAttr ".ix" -type "matrix" 4.3541510445179226 0 0 0 0 4.3541510445179226 0 0 0 0 15.384666766119331 0
		 0 0 0.084883071105291502 1;
	setAttr ".wt" 0.39062073826789856;
	setAttr ".re" 27;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing18";
	rename -uid "7DE5AF3C-43FD-AC60-78B8-B6A2BAF97F28";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 39 "e[0:3]" "e[14]" "e[18]" "e[22]" "e[26]" "e[30]" "e[34]" "e[38]" "e[42]" "e[46]" "e[50]" "e[54]" "e[58]" "e[62]" "e[66]" "e[70]" "e[74]" "e[78]" "e[82]" "e[86]" "e[90]" "e[94]" "e[98]" "e[102]" "e[106]" "e[110]" "e[114]" "e[118]" "e[148]" "e[178]" "e[182]" "e[186]" "e[190]" "e[204]" "e[236]" "e[264]" "e[296]" "e[328]" "e[360]";
	setAttr ".ix" -type "matrix" 4.3541510445179226 0 0 0 0 4.3541510445179226 0 0 0 0 15.384666766119331 0
		 0 0 0.084883071105291502 1;
	setAttr ".wt" 0.51148068904876709;
	setAttr ".dr" no;
	setAttr ".re" 1;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak9";
	rename -uid "2831A1C6-4036-6E2F-F785-0B9B1F2016F7";
	setAttr ".uopa" yes;
	setAttr -s 128 ".tk";
	setAttr ".tk[0]" -type "float3" 0.13319151 0 0 ;
	setAttr ".tk[1]" -type "float3" -0.13319151 0 0 ;
	setAttr ".tk[2]" -type "float3" 0.14364691 0 0 ;
	setAttr ".tk[3]" -type "float3" -0.14364688 0 0 ;
	setAttr ".tk[4]" -type "float3" 0.0039688372 0 0 ;
	setAttr ".tk[5]" -type "float3" -0.0039687958 0 0 ;
	setAttr ".tk[6]" -type "float3" 0.0036799323 0 0 ;
	setAttr ".tk[7]" -type "float3" -0.0036799323 0 0 ;
	setAttr ".tk[8]" -type "float3" -0.18526527 0 0 ;
	setAttr ".tk[9]" -type "float3" 0.18526526 0 0 ;
	setAttr ".tk[10]" -type "float3" 0.17178062 0 0 ;
	setAttr ".tk[11]" -type "float3" -0.17178062 0 0 ;
	setAttr ".tk[12]" -type "float3" -0.24835795 0 0 ;
	setAttr ".tk[13]" -type "float3" 0.24835801 0 0 ;
	setAttr ".tk[14]" -type "float3" 0.23028111 0 0 ;
	setAttr ".tk[15]" -type "float3" -0.23028111 0 0 ;
	setAttr ".tk[16]" -type "float3" -0.27540725 0 0 ;
	setAttr ".tk[17]" -type "float3" 0.27540728 0 0 ;
	setAttr ".tk[18]" -type "float3" 0.25536168 0 0 ;
	setAttr ".tk[19]" -type "float3" -0.25536168 0 0 ;
	setAttr ".tk[20]" -type "float3" -0.3061946 0 0 ;
	setAttr ".tk[21]" -type "float3" 0.30619463 0 0 ;
	setAttr ".tk[22]" -type "float3" 0.28390825 0 0 ;
	setAttr ".tk[23]" -type "float3" -0.28390825 0 0 ;
	setAttr ".tk[24]" -type "float3" -0.29964232 0 0 ;
	setAttr ".tk[25]" -type "float3" 0.29964235 0 0 ;
	setAttr ".tk[26]" -type "float3" 0.27783293 0 0 ;
	setAttr ".tk[27]" -type "float3" -0.27783293 0 0 ;
	setAttr ".tk[28]" -type "float3" -0.27986166 0 0 ;
	setAttr ".tk[29]" -type "float3" 0.27986178 0 0 ;
	setAttr ".tk[30]" -type "float3" 0.25949192 0 0 ;
	setAttr ".tk[31]" -type "float3" -0.25949192 0 0 ;
	setAttr ".tk[32]" -type "float3" -0.21388267 0 0 ;
	setAttr ".tk[33]" -type "float3" 0.2138827 0 0 ;
	setAttr ".tk[34]" -type "float3" 0.19831517 0 0 ;
	setAttr ".tk[35]" -type "float3" -0.19831517 0 0 ;
	setAttr ".tk[36]" -type "float3" -0.15498744 0 0 ;
	setAttr ".tk[37]" -type "float3" 0.15498737 0 0 ;
	setAttr ".tk[38]" -type "float3" 0.14370663 0 0 ;
	setAttr ".tk[39]" -type "float3" -0.14370663 0 0 ;
	setAttr ".tk[40]" -type "float3" -0.069891199 0 0 ;
	setAttr ".tk[41]" -type "float3" 0.069891192 0 0 ;
	setAttr ".tk[42]" -type "float3" 0.064804189 0 0 ;
	setAttr ".tk[43]" -type "float3" -0.064804189 0 0 ;
	setAttr ".tk[44]" -type "float3" -0.030219471 0 0 ;
	setAttr ".tk[45]" -type "float3" 0.030219467 0 0 ;
	setAttr ".tk[46]" -type "float3" 0.028019926 0 0 ;
	setAttr ".tk[47]" -type "float3" -0.028019926 0 0 ;
	setAttr ".tk[48]" -type "float3" -0.01512072 0 0 ;
	setAttr ".tk[49]" -type "float3" 0.01512075 0 0 ;
	setAttr ".tk[50]" -type "float3" 0.014020158 0 0 ;
	setAttr ".tk[51]" -type "float3" -0.014020158 0 0 ;
	setAttr ".tk[52]" -type "float3" -0.028057272 0 0 ;
	setAttr ".tk[53]" -type "float3" 0.028057279 0 0 ;
	setAttr ".tk[54]" -type "float3" 0.026015123 0 0 ;
	setAttr ".tk[55]" -type "float3" -0.026015123 0 0 ;
	setAttr ".tk[56]" -type "float3" -0.023485111 0 0 ;
	setAttr ".tk[57]" -type "float3" 0.023485132 0 0 ;
	setAttr ".tk[58]" -type "float3" 0.021775747 0 0 ;
	setAttr ".tk[59]" -type "float3" -0.021775747 0 0 ;
	setAttr ".tk[92]" -type "float3" -0.051878624 0 0 ;
	setAttr ".tk[93]" -type "float3" 0.051878665 0 0 ;
	setAttr ".tk[94]" -type "float3" 0.033870943 0 0 ;
	setAttr ".tk[95]" -type "float3" -0.033870943 0 0 ;
	setAttr ".tk[130]" -type "float3" -0.09641172 0 0 ;
	setAttr ".tk[131]" -type "float3" -0.071919315 0 0 ;
	setAttr ".tk[132]" -type "float3" -0.055763207 0 0 ;
	setAttr ".tk[133]" -type "float3" -0.020139093 0 0 ;
	setAttr ".tk[134]" -type "float3" 0.020139093 0 0 ;
	setAttr ".tk[135]" -type "float3" 0.055763207 0 0 ;
	setAttr ".tk[136]" -type "float3" 0.071919315 0 0 ;
	setAttr ".tk[137]" -type "float3" 0.09641172 0 0 ;
	setAttr ".tk[138]" -type "float3" 0.10691212 0 0 ;
	setAttr ".tk[139]" -type "float3" 0.11886373 0 0 ;
	setAttr ".tk[140]" -type "float3" 0.1163201 0 0 ;
	setAttr ".tk[141]" -type "float3" 0.10864134 0 0 ;
	setAttr ".tk[142]" -type "float3" 0.083028488 0 0 ;
	setAttr ".tk[143]" -type "float3" 0.06016558 0 0 ;
	setAttr ".tk[144]" -type "float3" 0.027131531 0 0 ;
	setAttr ".tk[145]" -type "float3" 0.0058698133 0 0 ;
	setAttr ".tk[146]" -type "float3" 0.010891743 0 0 ;
	setAttr ".tk[147]" -type "float3" 0.011731084 0 0 ;
	setAttr ".tk[148]" -type "float3" 0.009116835 0 0 ;
	setAttr ".tk[149]" -type "float3" 0.0015406747 0 0 ;
	setAttr ".tk[150]" -type "float3" -0.0015406747 0 0 ;
	setAttr ".tk[151]" -type "float3" -0.009116835 0 0 ;
	setAttr ".tk[152]" -type "float3" -0.011731084 0 0 ;
	setAttr ".tk[153]" -type "float3" -0.010891743 0 0 ;
	setAttr ".tk[154]" -type "float3" -0.0058698133 0 0 ;
	setAttr ".tk[155]" -type "float3" -0.027131531 0 0 ;
	setAttr ".tk[156]" -type "float3" -0.06016558 0 0 ;
	setAttr ".tk[157]" -type "float3" -0.083028488 0 0 ;
	setAttr ".tk[158]" -type "float3" -0.10864134 0 0 ;
	setAttr ".tk[159]" -type "float3" -0.1163201 0 0 ;
	setAttr ".tk[160]" -type "float3" -0.11886373 0 0 ;
	setAttr ".tk[161]" -type "float3" -0.10691212 0 0 ;
	setAttr ".tk[162]" -type "float3" -0.087838732 0 0 ;
	setAttr ".tk[163]" -type "float3" -0.065524243 0 0 ;
	setAttr ".tk[164]" -type "float3" -0.050804742 0 0 ;
	setAttr ".tk[165]" -type "float3" -0.012919779 0 0 ;
	setAttr ".tk[166]" -type "float3" 0.012919779 0 0 ;
	setAttr ".tk[167]" -type "float3" 0.050804742 0 0 ;
	setAttr ".tk[168]" -type "float3" 0.065524243 0 0 ;
	setAttr ".tk[169]" -type "float3" 0.087838732 0 0 ;
	setAttr ".tk[170]" -type "float3" 0.097405471 0 0 ;
	setAttr ".tk[171]" -type "float3" 0.10829431 0 0 ;
	setAttr ".tk[172]" -type "float3" 0.1059769 0 0 ;
	setAttr ".tk[173]" -type "float3" 0.098980948 0 0 ;
	setAttr ".tk[174]" -type "float3" 0.075645588 0 0 ;
	setAttr ".tk[175]" -type "float3" 0.054815635 0 0 ;
	setAttr ".tk[176]" -type "float3" 0.024718989 0 0 ;
	setAttr ".tk[177]" -type "float3" 0.005347868 0 0 ;
	setAttr ".tk[178]" -type "float3" 0.0099232411 0 0 ;
	setAttr ".tk[179]" -type "float3" 0.010687955 0 0 ;
	setAttr ".tk[180]" -type "float3" 0.0083061671 0 0 ;
	setAttr ".tk[181]" -type "float3" 0.0014036774 0 0 ;
	setAttr ".tk[182]" -type "float3" -0.0014036774 0 0 ;
	setAttr ".tk[183]" -type "float3" -0.0083061671 0 0 ;
	setAttr ".tk[184]" -type "float3" -0.010687957 0 0 ;
	setAttr ".tk[185]" -type "float3" -0.0099232411 0 0 ;
	setAttr ".tk[186]" -type "float3" -0.005347868 0 0 ;
	setAttr ".tk[187]" -type "float3" -0.024718994 0 0 ;
	setAttr ".tk[188]" -type "float3" -0.054815635 0 0 ;
	setAttr ".tk[189]" -type "float3" -0.075645588 0 0 ;
	setAttr ".tk[190]" -type "float3" -0.098980948 0 0 ;
	setAttr ".tk[191]" -type "float3" -0.10597693 0 0 ;
	setAttr ".tk[192]" -type "float3" -0.10829431 0 0 ;
	setAttr ".tk[193]" -type "float3" -0.097405471 0 0 ;
createNode polySoftEdge -n "polySoftEdge1";
	rename -uid "A97320F5-492C-B3D6-7E9D-B696606DA17A";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[*]";
	setAttr ".ix" -type "matrix" 4.3541510445179226 0 0 0 0 4.3541510445179226 0 0 0 0 15.384666766119331 0
		 0 0 0.084883071105291502 1;
	setAttr ".a" 180;
createNode polyTweak -n "polyTweak10";
	rename -uid "8BB00BF2-44F2-0BE9-CEB1-63B9C4C2A71C";
	setAttr ".uopa" yes;
	setAttr -s 32 ".tk";
	setAttr ".tk[2]" -type "float3" 0 -0.01542067 -1.8626451e-09 ;
	setAttr ".tk[3]" -type "float3" 0 -0.01542067 -1.8626451e-09 ;
	setAttr ".tk[4]" -type "float3" 0 -0.0075687813 -1.8626451e-09 ;
	setAttr ".tk[5]" -type "float3" 0 -0.0075687813 -1.8626451e-09 ;
	setAttr ".tk[8]" -type "float3" 0 -0.020299528 -1.8626451e-09 ;
	setAttr ".tk[9]" -type "float3" 0 -0.020299528 -1.8626451e-09 ;
	setAttr ".tk[12]" -type "float3" 0 -0.024755288 0 ;
	setAttr ".tk[13]" -type "float3" 0 -0.024755288 0 ;
	setAttr ".tk[16]" -type "float3" 0 -0.02653935 0 ;
	setAttr ".tk[17]" -type "float3" 0 -0.02653935 0 ;
	setAttr ".tk[20]" -type "float3" 0 -0.027440589 0 ;
	setAttr ".tk[21]" -type "float3" 0 -0.027440589 0 ;
	setAttr ".tk[24]" -type "float3" 0 -0.026542854 -1.1641532e-10 ;
	setAttr ".tk[25]" -type "float3" 0 -0.026542854 -1.1641532e-10 ;
	setAttr ".tk[28]" -type "float3" 0 -0.025323778 1.1641532e-10 ;
	setAttr ".tk[29]" -type "float3" 0 -0.025323778 1.1641532e-10 ;
	setAttr ".tk[32]" -type "float3" 0 -0.021254323 0 ;
	setAttr ".tk[33]" -type "float3" 0 -0.021254323 0 ;
	setAttr ".tk[36]" -type "float3" 0 -0.01824094 -9.3132257e-10 ;
	setAttr ".tk[37]" -type "float3" 0 -0.01824094 -9.3132257e-10 ;
	setAttr ".tk[40]" -type "float3" 0 -0.012455199 0 ;
	setAttr ".tk[41]" -type "float3" 0 -0.012455199 0 ;
	setAttr ".tk[44]" -type "float3" 0 -0.0095210848 1.8626451e-09 ;
	setAttr ".tk[45]" -type "float3" 0 -0.0095210848 1.8626451e-09 ;
	setAttr ".tk[48]" -type "float3" 0 -0.0096089281 0 ;
	setAttr ".tk[49]" -type "float3" 0 -0.0096089281 0 ;
	setAttr ".tk[52]" -type "float3" 0 -0.0092063956 0 ;
	setAttr ".tk[53]" -type "float3" 0 -0.0092063956 0 ;
	setAttr ".tk[56]" -type "float3" 0 -0.0090761296 0 ;
	setAttr ".tk[57]" -type "float3" 0 -0.0090761296 0 ;
	setAttr ".tk[92]" -type "float3" 0 -0.0062987544 0 ;
	setAttr ".tk[93]" -type "float3" 0 -0.0062987544 0 ;
createNode polySplitRing -n "polySplitRing19";
	rename -uid "D45B6D62-47D1-A86A-D8E0-E79FF2B2EF39";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 7 "e[184:185]" "e[187]" "e[189]" "e[326]" "e[330]" "e[452]" "e[458]";
	setAttr ".ix" -type "matrix" 4.3541510445179226 0 0 0 0 4.3541510445179226 0 0 0 0 15.384666766119331 0
		 0 0 0.084883071105291502 1;
	setAttr ".wt" 0.95304489135742188;
	setAttr ".dr" no;
	setAttr ".re" 187;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak11";
	rename -uid "0B0C7D17-4B07-9B63-8C1F-018A3368DCFC";
	setAttr ".uopa" yes;
	setAttr -s 164 ".tk";
	setAttr ".tk[0]" -type "float3" 0.0096597485 0 0 ;
	setAttr ".tk[1]" -type "float3" -0.0096597485 0 0 ;
	setAttr ".tk[2]" -type "float3" 0.0083635822 0 0 ;
	setAttr ".tk[3]" -type "float3" -0.0083635859 0 0 ;
	setAttr ".tk[8]" -type "float3" -0.0069301738 0 0 ;
	setAttr ".tk[9]" -type "float3" 0.0069301757 0 0 ;
	setAttr ".tk[10]" -type "float3" 0.008004196 0 0 ;
	setAttr ".tk[11]" -type "float3" -0.008004196 0 0 ;
	setAttr ".tk[12]" -type "float3" -0.015320793 0 0 ;
	setAttr ".tk[13]" -type "float3" 0.015320788 0 0 ;
	setAttr ".tk[14]" -type "float3" 0.017723694 0 0 ;
	setAttr ".tk[15]" -type "float3" -0.017723694 0 0 ;
	setAttr ".tk[16]" -type "float3" -0.014032951 0 0 ;
	setAttr ".tk[17]" -type "float3" 0.014032949 0 0 ;
	setAttr ".tk[18]" -type "float3" 0.016216295 0 0 ;
	setAttr ".tk[19]" -type "float3" -0.016216297 0 0 ;
	setAttr ".tk[20]" -type "float3" -0.024698621 0 0 ;
	setAttr ".tk[21]" -type "float3" 0.024698619 0 0 ;
	setAttr ".tk[22]" -type "float3" 0.028526314 0 0 ;
	setAttr ".tk[23]" -type "float3" -0.028526317 0 0 ;
	setAttr ".tk[24]" -type "float3" -0.026607953 0 0 ;
	setAttr ".tk[25]" -type "float3" 0.026607951 0 0 ;
	setAttr ".tk[26]" -type "float3" 0.030656178 0 0 ;
	setAttr ".tk[27]" -type "float3" -0.030656178 0 0 ;
	setAttr ".tk[28]" -type "float3" -0.025071803 0 0 ;
	setAttr ".tk[29]" -type "float3" 0.025071781 0 0 ;
	setAttr ".tk[30]" -type "float3" 0.028933698 0 0 ;
	setAttr ".tk[31]" -type "float3" -0.028933698 0 0 ;
	setAttr ".tk[32]" -type "float3" -0.014508862 0 0 ;
	setAttr ".tk[33]" -type "float3" 0.01450886 0 0 ;
	setAttr ".tk[34]" -type "float3" 0.016757395 0 0 ;
	setAttr ".tk[35]" -type "float3" -0.016757395 0 0 ;
	setAttr ".tk[36]" -type "float3" -0.0065116538 0 0 ;
	setAttr ".tk[37]" -type "float3" 0.0065116603 0 0 ;
	setAttr ".tk[38]" -type "float3" 0.007520807 0 0 ;
	setAttr ".tk[39]" -type "float3" -0.007520807 0 0 ;
	setAttr ".tk[60]" -type "float3" -0.026171591 0 0 ;
	setAttr ".tk[61]" -type "float3" 0.026171591 0 0 ;
	setAttr ".tk[62]" -type "float3" 0.021686127 0 0 ;
	setAttr ".tk[63]" -type "float3" 0.04791319 0 0 ;
	setAttr ".tk[64]" -type "float3" 0.043912362 0 0 ;
	setAttr ".tk[65]" -type "float3" 0.077287644 0 0 ;
	setAttr ".tk[66]" -type "float3" 0.083263017 0 0 ;
	setAttr ".tk[67]" -type "float3" 0.078448668 0 0 ;
	setAttr ".tk[68]" -type "float3" 0.045401592 0 0 ;
	setAttr ".tk[69]" -type "float3" 0.020376468 0 0 ;
	setAttr ".tk[82]" -type "float3" -0.020376468 0 0 ;
	setAttr ".tk[83]" -type "float3" -0.045401592 0 0 ;
	setAttr ".tk[84]" -type "float3" -0.078448668 0 0 ;
	setAttr ".tk[85]" -type "float3" -0.083263017 0 0 ;
	setAttr ".tk[86]" -type "float3" -0.077287652 0 0 ;
	setAttr ".tk[87]" -type "float3" -0.043912366 0 0 ;
	setAttr ".tk[88]" -type "float3" -0.04791319 0 0 ;
	setAttr ".tk[89]" -type "float3" -0.021686127 0 0 ;
	setAttr ".tk[90]" -type "float3" 0 -0.019568186 -0.023703344 ;
	setAttr ".tk[91]" -type "float3" 0 -0.019568186 -0.023703344 ;
	setAttr ".tk[92]" -type "float3" 0 0.01643727 -0.013734649 ;
	setAttr ".tk[93]" -type "float3" 0 0.01643727 -0.013734649 ;
	setAttr ".tk[94]" -type "float3" 0 -0.0065321741 -0.0046953792 ;
	setAttr ".tk[95]" -type "float3" 0 -0.0065321741 -0.0046953792 ;
	setAttr ".tk[96]" -type "float3" 0 0 -0.0054820771 ;
	setAttr ".tk[97]" -type "float3" 0 0 -0.0054820771 ;
	setAttr ".tk[98]" -type "float3" -0.077287652 0 0 ;
	setAttr ".tk[99]" -type "float3" -0.043912366 0 0 ;
	setAttr ".tk[100]" -type "float3" -0.04791319 0 0 ;
	setAttr ".tk[101]" -type "float3" -0.021686127 0 0 ;
	setAttr ".tk[102]" -type "float3" -0.026171591 0 0 ;
	setAttr ".tk[103]" -type "float3" 0 2.3283064e-10 -0.0093041202 ;
	setAttr ".tk[104]" -type "float3" 0 2.3283064e-10 -0.0093041202 ;
	setAttr ".tk[105]" -type "float3" 0.026171591 0 0 ;
	setAttr ".tk[106]" -type "float3" 0.021686127 0 0 ;
	setAttr ".tk[107]" -type "float3" 0.04791319 0 0 ;
	setAttr ".tk[108]" -type "float3" 0.043912362 0 0 ;
	setAttr ".tk[109]" -type "float3" 0.077287644 0 0 ;
	setAttr ".tk[110]" -type "float3" 0.083317868 0 0 ;
	setAttr ".tk[111]" -type "float3" 0.078469262 0 0 ;
	setAttr ".tk[112]" -type "float3" 0.045401592 0 0 ;
	setAttr ".tk[113]" -type "float3" 0.020376468 0 0 ;
	setAttr ".tk[126]" -type "float3" -0.020376468 0 0 ;
	setAttr ".tk[127]" -type "float3" -0.045401592 0 0 ;
	setAttr ".tk[128]" -type "float3" -0.078469262 0 0 ;
	setAttr ".tk[129]" -type "float3" -0.083317868 0 0 ;
	setAttr ".tk[130]" -type "float3" -0.035266176 0 0 ;
	setAttr ".tk[131]" -type "float3" -0.015957918 0 0 ;
	setAttr ".tk[132]" -type "float3" -0.019258592 0 0 ;
	setAttr ".tk[133]" -type "float3" 0 0.00391364 -0.009082594 ;
	setAttr ".tk[134]" -type "float3" 0 0.00391364 -0.009082594 ;
	setAttr ".tk[135]" -type "float3" 0.019258592 0 0 ;
	setAttr ".tk[136]" -type "float3" 0.015957918 0 0 ;
	setAttr ".tk[137]" -type "float3" 0.035266176 0 0 ;
	setAttr ".tk[138]" -type "float3" 0.032313276 0 0 ;
	setAttr ".tk[139]" -type "float3" 0.056872763 0 0 ;
	setAttr ".tk[140]" -type "float3" 0.061326336 0 0 ;
	setAttr ".tk[141]" -type "float3" 0.057742272 0 0 ;
	setAttr ".tk[142]" -type "float3" 0.033409141 0 0 ;
	setAttr ".tk[143]" -type "float3" 0.014994196 0 0 ;
	setAttr ".tk[156]" -type "float3" -0.014994196 0 0 ;
	setAttr ".tk[157]" -type "float3" -0.033409141 0 0 ;
	setAttr ".tk[158]" -type "float3" -0.057742272 0 0 ;
	setAttr ".tk[159]" -type "float3" -0.061326336 0 0 ;
	setAttr ".tk[160]" -type "float3" -0.056872755 0 0 ;
	setAttr ".tk[161]" -type "float3" -0.032313272 0 0 ;
	setAttr ".tk[162]" -type "float3" -0.036424566 0 0 ;
	setAttr ".tk[163]" -type "float3" -0.016467271 0 0 ;
	setAttr ".tk[164]" -type "float3" -0.019873295 0 0 ;
	setAttr ".tk[165]" -type "float3" 0 0 -0.0034051596 ;
	setAttr ".tk[166]" -type "float3" 0 0 -0.0034051596 ;
	setAttr ".tk[167]" -type "float3" 0.019873295 0 0 ;
	setAttr ".tk[168]" -type "float3" 0.016467271 0 0 ;
	setAttr ".tk[169]" -type "float3" 0.036424566 0 0 ;
	setAttr ".tk[170]" -type "float3" 0.033351872 0 0 ;
	setAttr ".tk[171]" -type "float3" 0.058688071 0 0 ;
	setAttr ".tk[172]" -type "float3" 0.06314186 0 0 ;
	setAttr ".tk[173]" -type "float3" 0.059546452 0 0 ;
	setAttr ".tk[174]" -type "float3" 0.034475513 0 0 ;
	setAttr ".tk[175]" -type "float3" 0.015472787 0 0 ;
	setAttr ".tk[188]" -type "float3" -0.015472787 0 0 ;
	setAttr ".tk[189]" -type "float3" -0.034475513 0 0 ;
	setAttr ".tk[190]" -type "float3" -0.059546452 0 0 ;
	setAttr ".tk[191]" -type "float3" -0.063141853 0 0 ;
	setAttr ".tk[192]" -type "float3" -0.058688071 0 0 ;
	setAttr ".tk[193]" -type "float3" -0.033351876 0 0 ;
	setAttr ".tk[194]" -type "float3" -0.0026603779 0 0 ;
	setAttr ".tk[195]" -type "float3" -0.0026274626 0 0 ;
	setAttr ".tk[196]" -type "float3" -0.0028051673 0 0 ;
	setAttr ".tk[197]" -type "float3" -0.0027159047 0 0 ;
	setAttr ".tk[198]" -type "float3" -0.0029257489 0 0 ;
	setAttr ".tk[199]" -type "float3" -0.0029812776 0 0 ;
	setAttr ".tk[200]" -type "float3" -0.0029411125 0 0 ;
	setAttr ".tk[201]" -type "float3" -0.0028014816 0 0 ;
	setAttr ".tk[202]" -type "float3" -0.0026178495 0 0 ;
	setAttr ".tk[203]" -type "float3" -0.0024683364 0 0 ;
	setAttr ".tk[204]" -type "float3" -0.0024683364 0 0 ;
	setAttr ".tk[205]" -type "float3" -0.0024683364 0 0 ;
	setAttr ".tk[206]" -type "float3" -0.0024683364 0 0 ;
	setAttr ".tk[207]" -type "float3" -0.0024683364 0 0 ;
	setAttr ".tk[208]" -type "float3" -0.0024683364 0 0 ;
	setAttr ".tk[209]" -type "float3" -0.0024683364 0 0 ;
	setAttr ".tk[210]" -type "float3" -0.0024683364 0 0 ;
	setAttr ".tk[211]" -type "float3" -0.0024683364 0 0 ;
	setAttr ".tk[212]" -type "float3" -0.0024683364 0 0 ;
	setAttr ".tk[213]" -type "float3" -0.0024683364 0 0 ;
	setAttr ".tk[214]" -type "float3" -0.0024683364 0 0 ;
	setAttr ".tk[215]" -type "float3" -0.0024683364 0 0 ;
	setAttr ".tk[216]" -type "float3" -0.0024683364 0 0 ;
	setAttr ".tk[217]" -type "float3" -0.0024683364 0 0 ;
	setAttr ".tk[218]" -type "float3" -0.0024683364 0 0 ;
	setAttr ".tk[219]" -type "float3" -0.0026410241 0 0 ;
	setAttr ".tk[220]" -type "float3" -0.00285311 0 0 ;
	setAttr ".tk[221]" -type "float3" -0.0030302417 0 0 ;
	setAttr ".tk[222]" -type "float3" -0.0030755899 0 0 ;
	setAttr ".tk[223]" -type "float3" -0.0030136374 0 0 ;
	setAttr ".tk[224]" -type "float3" -0.0027658809 0 0 ;
	setAttr ".tk[225]" -type "float3" -0.0028597554 0 0 ;
	setAttr ".tk[226]" -type "float3" -0.002652124 0 0 ;
	setAttr ".tk[227]" -type "float3" -0.0026901374 0 0 ;
	setAttr ".tk[228]" -type "float3" -0.0024683364 -0.0074736597 -0.002401941 ;
	setAttr ".tk[229]" -type "float3" -0.0024683364 0 0 ;
	setAttr ".tk[230]" -type "float3" -0.0024683364 0 0 ;
	setAttr ".tk[231]" -type "float3" -0.0024683364 0 0 ;
	setAttr ".tk[232]" -type "float3" -0.0024683364 0 0 ;
	setAttr ".tk[233]" -type "float3" -0.0024683364 0.0070445463 0.0026583201 ;
	setAttr ".tk[234]" -type "float3" -0.0024683364 0.015654549 -0.00022152695 ;
	setAttr ".tk[235]" -type "float3" -0.0024683364 0.036788203 -0.015506865 ;
createNode polyCube -n "polyCube2";
	rename -uid "32EEF1AA-4F4E-184B-9810-65B0E6955FC0";
	setAttr ".cuv" 4;
createNode polySplitRing -n "polySplitRing20";
	rename -uid "5DE6ED2D-4AF2-C9CE-82D6-E089248A397E";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[6:7]" "e[10:11]";
	setAttr ".ix" -type "matrix" 0.054791416844971783 0 0 0 0 1 0 0 0 0 1 0 0 2.2563016321647891 2.8101931747067881 1;
	setAttr ".wt" 0.80202829837799072;
	setAttr ".dr" no;
	setAttr ".re" 7;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak12";
	rename -uid "D9D96523-47EC-0EE0-2451-7B9149F9CD7A";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[4:7]" -type "float3"  0 -0.98567581 -2.55173516
		 0 -0.98567581 -2.55173516 0 -0.0078981584 -2.55173516 0 -0.0078981584 -2.55173516;
createNode polySplitRing -n "polySplitRing21";
	rename -uid "01C83D05-403D-E7F4-A78E-AF831DAEAA83";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[10:13]";
	setAttr ".ix" -type "matrix" 0.054791416844971783 0 0 0 0 1 0 0 0 0 1 0 0 2.2563016321647891 2.8101931747067881 1;
	setAttr ".wt" 0.35153409838676453;
	setAttr ".re" 12;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing22";
	rename -uid "ABAE5A18-4FD3-8949-479C-81BCBD55F2F4";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[6:7]" "e[15]" "e[17]";
	setAttr ".ix" -type "matrix" 0.054791416844971783 0 0 0 0 1 0 0 0 0 1 0 0 2.2563016321647891 2.8101931747067881 1;
	setAttr ".wt" 0.91635185480117798;
	setAttr ".dr" no;
	setAttr ".re" 7;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing23";
	rename -uid "E6323A66-49C1-6E9D-3E8B-BF949BC9CAD8";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[6:7]" "e[31]" "e[33]";
	setAttr ".ix" -type "matrix" 0.054791416844971783 0 0 0 0 1 0 0 0 0 1 0 0 2.2563016321647891 2.8101931747067881 1;
	setAttr ".wt" 0.89830482006072998;
	setAttr ".dr" no;
	setAttr ".re" 7;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing24";
	rename -uid "13680E19-4583-D690-E21E-E79805C06EF9";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[6:7]" "e[39]" "e[41]";
	setAttr ".ix" -type "matrix" 0.054791416844971783 0 0 0 0 1 0 0 0 0 1 0 0 2.2563016321647891 2.8101931747067881 1;
	setAttr ".wt" 0.89580708742141724;
	setAttr ".dr" no;
	setAttr ".re" 7;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing25";
	rename -uid "F445E5AC-48D9-8158-7A1B-A8B54D320A32";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[6:7]" "e[47]" "e[49]";
	setAttr ".ix" -type "matrix" 0.054791416844971783 0 0 0 0 1 0 0 0 0 1 0 0 2.2563016321647891 2.8101931747067881 1;
	setAttr ".wt" 0.89450806379318237;
	setAttr ".dr" no;
	setAttr ".re" 7;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing26";
	rename -uid "9FC2B5F2-461F-5EE5-2C17-BB9C65EE08A0";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[6:7]" "e[55]" "e[57]";
	setAttr ".ix" -type "matrix" 0.054791416844971783 0 0 0 0 1 0 0 0 0 1 0 0 2.2563016321647891 2.8101931747067881 1;
	setAttr ".wt" 0.87661439180374146;
	setAttr ".dr" no;
	setAttr ".re" 7;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak13";
	rename -uid "7649953E-4C66-7919-80E3-17919A38825A";
	setAttr ".uopa" yes;
	setAttr -s 27 ".tk";
	setAttr ".tk[2]" -type "float3" 0 0.80069464 -0.55481207 ;
	setAttr ".tk[3]" -type "float3" 0 0.80069464 -0.55481207 ;
	setAttr ".tk[8]" -type "float3" 0 0.16670747 -0.0193846 ;
	setAttr ".tk[9]" -type "float3" 0 0.16670747 -0.0193846 ;
	setAttr ".tk[10]" -type "float3" 0 0.050399952 0.019384596 ;
	setAttr ".tk[11]" -type "float3" 0 0.050399952 0.019384596 ;
	setAttr ".tk[12]" -type "float3" 0 0.12793832 -0.023261515 ;
	setAttr ".tk[13]" -type "float3" 0 0.12793832 -0.023261515 ;
	setAttr ".tk[14]" -type "float3" 0 0.034892272 0 ;
	setAttr ".tk[15]" -type "float3" 0 0.034892272 0 ;
	setAttr ".tk[16]" -type "float3" 0 0.13569218 -0.027138434 ;
	setAttr ".tk[17]" -type "float3" 0 0.13569218 -0.027138434 ;
	setAttr ".tk[18]" -type "float3" 0 0.081415303 0.023261515 ;
	setAttr ".tk[19]" -type "float3" 0 0.081415303 0.023261515 ;
	setAttr ".tk[20]" -type "float3" 0 0.15119985 -0.08529222 ;
	setAttr ".tk[21]" -type "float3" 0 0.15119985 -0.08529222 ;
	setAttr ".tk[22]" -type "float3" 0 0.093046069 -0.011630762 ;
	setAttr ".tk[23]" -type "float3" 0 0.093046069 -0.011630762 ;
	setAttr ".tk[24]" -type "float3" 0 0.19772288 -0.050399955 ;
	setAttr ".tk[25]" -type "float3" 0 0.19772288 -0.050399955 ;
	setAttr ".tk[26]" -type "float3" 0 0.10855374 6.9849193e-10 ;
	setAttr ".tk[27]" -type "float3" 0 0.10855374 6.9849193e-10 ;
	setAttr ".tk[28]" -type "float3" 0 0.22873826 -0.023261515 ;
	setAttr ".tk[29]" -type "float3" 0 0.22873826 -0.023261515 ;
	setAttr ".tk[30]" -type "float3" 0 0.13181524 0.03101535 ;
	setAttr ".tk[31]" -type "float3" 0 0.13181524 0.03101535 ;
createNode polySplitRing -n "polySplitRing27";
	rename -uid "3C222115-446D-F8C3-D323-DEA12E07076D";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[6:7]" "e[63]" "e[65]";
	setAttr ".ix" -type "matrix" 0.054791416844971783 0 0 0 0 1 0 0 0 0 1 0 0 2.2563016321647891 2.8101931747067881 1;
	setAttr ".wt" 0.86467218399047852;
	setAttr ".dr" no;
	setAttr ".re" 7;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing28";
	rename -uid "20CA6A30-4767-58A4-D818-2696D6BA4358";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[6:7]" "e[71]" "e[73]";
	setAttr ".ix" -type "matrix" 0.054791416844971783 0 0 0 0 1 0 0 0 0 1 0 0 2.2563016321647891 2.8101931747067881 1;
	setAttr ".wt" 0.89057052135467529;
	setAttr ".dr" no;
	setAttr ".re" 7;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing29";
	rename -uid "4184CC55-4034-D30E-B23A-BF812BC7A705";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[6:7]" "e[79]" "e[81]";
	setAttr ".ix" -type "matrix" 0.054791416844971783 0 0 0 0 1 0 0 0 0 1 0 0 2.2563016321647891 2.8101931747067881 1;
	setAttr ".wt" 0.76410400867462158;
	setAttr ".dr" no;
	setAttr ".re" 7;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing30";
	rename -uid "07033D06-4B9E-7850-0ED8-ABAD2F0B0D85";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[6:7]" "e[87]" "e[89]";
	setAttr ".ix" -type "matrix" 0.054791416844971783 0 0 0 0 1 0 0 0 0 1 0 0 2.2563016321647891 2.8101931747067881 1;
	setAttr ".wt" 0.6254468560218811;
	setAttr ".dr" no;
	setAttr ".re" 7;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing31";
	rename -uid "A2388E61-4A1E-70FA-CEA7-E9BF4884FA40";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[6:7]" "e[95]" "e[97]";
	setAttr ".ix" -type "matrix" 0.054791416844971783 0 0 0 0 1 0 0 0 0 1 0 0 2.2563016321647891 2.8101931747067881 1;
	setAttr ".wt" 0.56649702787399292;
	setAttr ".dr" no;
	setAttr ".re" 7;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing32";
	rename -uid "CD32CC85-4D85-338F-8CD0-F189A125F888";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[6:7]" "e[103]" "e[105]";
	setAttr ".ix" -type "matrix" 0.054791416844971783 0 0 0 0 1 0 0 0 0 1 0 0 2.2563016321647891 2.8101931747067881 1;
	setAttr ".wt" 0.99743980169296265;
	setAttr ".dr" no;
	setAttr ".re" 7;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing33";
	rename -uid "F898F8EE-4659-27FB-BCD5-FE95A72CCDCA";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[6:7]" "e[111]" "e[113]";
	setAttr ".ix" -type "matrix" 0.054791416844971783 0 0 0 0 1 0 0 0 0 1 0 0 2.2563016321647891 2.8101931747067881 1;
	setAttr ".wt" 0.5621563196182251;
	setAttr ".dr" no;
	setAttr ".re" 7;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak14";
	rename -uid "82801451-4ECC-F70E-625B-E392A808D945";
	setAttr ".uopa" yes;
	setAttr -s 29 ".tk";
	setAttr ".tk[32]" -type "float3" 1.110223e-16 -0.051947255 -0.017315753 ;
	setAttr ".tk[33]" -type "float3" -1.110223e-16 -0.051947255 -0.017315753 ;
	setAttr ".tk[34]" -type "float3" 0 0.020552574 0.01468041 ;
	setAttr ".tk[35]" -type "float3" 0 0.020552574 0.01468041 ;
	setAttr ".tk[36]" -type "float3" 0 -0.12630315 -0.042780094 ;
	setAttr ".tk[37]" -type "float3" 0 -0.12630315 -0.042780094 ;
	setAttr ".tk[38]" -type "float3" 0 0.032296903 -0.002936082 ;
	setAttr ".tk[39]" -type "float3" 0 0.032296903 -0.002936082 ;
	setAttr ".tk[40]" -type "float3" 0 -0.072318748 0.074355893 ;
	setAttr ".tk[41]" -type "float3" 0 -0.072318748 0.074355893 ;
	setAttr ".tk[42]" -type "float3" 0 0.035232984 0.026424738 ;
	setAttr ".tk[43]" -type "float3" 0 0.035232984 0.026424738 ;
	setAttr ".tk[44]" -type "float3" 0 -0.14667462 0.049910121 ;
	setAttr ".tk[45]" -type "float3" 0 -0.14667462 0.049910121 ;
	setAttr ".tk[46]" -type "float3" 0 0.046977311 -0.020552574 ;
	setAttr ".tk[47]" -type "float3" 0 0.046977311 -0.020552574 ;
	setAttr ".tk[48]" -type "float3" 0 -0.11815456 0.044817254 ;
	setAttr ".tk[49]" -type "float3" 0 -0.11815456 0.044817254 ;
	setAttr ".tk[50]" -type "float3" 0 0.029360821 -0.07046596 ;
	setAttr ".tk[51]" -type "float3" 0 0.029360821 -0.07046596 ;
	setAttr ".tk[52]" -type "float3" 0 0.012222885 0.065188713 ;
	setAttr ".tk[53]" -type "float3" 0 0.012222885 0.065188713 ;
	setAttr ".tk[54]" -type "float3" 0 0.038169071 -0.026424738 ;
	setAttr ".tk[55]" -type "float3" 0 0.038169071 -0.026424738 ;
	setAttr ".tk[56]" -type "float3" 0 0.012222883 0.065188713 ;
	setAttr ".tk[57]" -type "float3" 0 0.012222883 0.065188713 ;
	setAttr ".tk[58]" -type "float3" 0 0.038169071 -0.026424738 ;
	setAttr ".tk[59]" -type "float3" 0 0.038169071 -0.026424738 ;
createNode polySplitRing -n "polySplitRing34";
	rename -uid "AC0B926D-4CE4-4621-EF4E-67A158728C2A";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[95]" "e[97]" "e[100:101]";
	setAttr ".ix" -type "matrix" 0.054791416844971783 0 0 0 0 1 0 0 0 0 1 0 0 2.2563016321647891 2.8101931747067881 1;
	setAttr ".wt" 0.60645347833633423;
	setAttr ".dr" no;
	setAttr ".re" 100;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing35";
	rename -uid "E6EFEF3B-4D40-A7D8-1814-52A8C38E9B74";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[87]" "e[89]" "e[92:93]";
	setAttr ".ix" -type "matrix" 0.054791416844971783 0 0 0 0 1 0 0 0 0 1 0 0 2.2563016321647891 2.8101931747067881 1;
	setAttr ".wt" 0.49893778562545776;
	setAttr ".re" 92;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing36";
	rename -uid "FA11212D-42D2-DF21-13A8-6AA4E5558491";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[79]" "e[81]" "e[84:85]";
	setAttr ".ix" -type "matrix" 0.054791416844971783 0 0 0 0 1 0 0 0 0 1 0 0 2.2563016321647891 2.8101931747067881 1;
	setAttr ".wt" 0.56329840421676636;
	setAttr ".dr" no;
	setAttr ".re" 84;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing37";
	rename -uid "11FEA745-4C2F-3F40-4E24-10A28A4D2EFF";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[71]" "e[73]" "e[76:77]";
	setAttr ".ix" -type "matrix" 0.054791416844971783 0 0 0 0 1 0 0 0 0 1 0 0 2.2563016321647891 2.8101931747067881 1;
	setAttr ".wt" 0.50282055139541626;
	setAttr ".dr" no;
	setAttr ".re" 76;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing38";
	rename -uid "D86ACE73-4D5F-02DE-2B64-25906531E32C";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[63]" "e[65]" "e[68:69]";
	setAttr ".ix" -type "matrix" 0.054791416844971783 0 0 0 0 1 0 0 0 0 1 0 0 2.2563016321647891 2.8101931747067881 1;
	setAttr ".wt" 0.62851065397262573;
	setAttr ".dr" no;
	setAttr ".re" 68;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing39";
	rename -uid "0572C6B0-41A9-E99E-ADC9-54B6E9CE94C1";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[55]" "e[57]" "e[60:61]";
	setAttr ".ix" -type "matrix" 0.054791416844971783 0 0 0 0 1 0 0 0 0 1 0 0 2.2563016321647891 2.8101931747067881 1;
	setAttr ".wt" 0.50307893753051758;
	setAttr ".dr" no;
	setAttr ".re" 60;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing40";
	rename -uid "9C4458F3-4C0F-0CB1-8F2A-66BFCDA2036B";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[47]" "e[49]" "e[52:53]";
	setAttr ".ix" -type "matrix" 0.054791416844971783 0 0 0 0 1 0 0 0 0 1 0 0 2.2563016321647891 2.8101931747067881 1;
	setAttr ".wt" 0.55892115831375122;
	setAttr ".dr" no;
	setAttr ".re" 52;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing41";
	rename -uid "C1E38FCC-418D-F785-566D-41AF4437FFBF";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[39]" "e[41]" "e[44:45]";
	setAttr ".ix" -type "matrix" 0.054791416844971783 0 0 0 0 1 0 0 0 0 1 0 0 2.2563016321647891 2.8101931747067881 1;
	setAttr ".wt" 0.62615346908569336;
	setAttr ".dr" no;
	setAttr ".re" 44;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing42";
	rename -uid "22DF503F-4140-6982-515A-16A27BE3FA41";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[31]" "e[33]" "e[36:37]";
	setAttr ".ix" -type "matrix" 0.054791416844971783 0 0 0 0 1 0 0 0 0 1 0 0 2.2563016321647891 2.8101931747067881 1;
	setAttr ".wt" 0.48135608434677124;
	setAttr ".re" 36;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing43";
	rename -uid "E1618961-424A-D4D0-F4CB-1690DF03CC4E";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[15]" "e[17]" "e[28:29]";
	setAttr ".ix" -type "matrix" 0.054791416844971783 0 0 0 0 1 0 0 0 0 1 0 0 2.2563016321647891 2.8101931747067881 1;
	setAttr ".wt" 0.49072003364562988;
	setAttr ".re" 28;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing44";
	rename -uid "40BF9FE1-45B8-9EEB-9958-0E998D28BE2E";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[12:13]" "e[23]" "e[25]";
	setAttr ".ix" -type "matrix" 0.054791416844971783 0 0 0 0 1 0 0 0 0 1 0 0 2.2563016321647891 2.8101931747067881 1;
	setAttr ".wt" 0.41376578807830811;
	setAttr ".re" 12;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing45";
	rename -uid "44847A7A-454F-3CDB-7A63-CD8B97C79F5F";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[10:11]" "e[20:21]";
	setAttr ".ix" -type "matrix" 0.054791416844971783 0 0 0 0 1 0 0 0 0 1 0 0 2.2563016321647891 2.8101931747067881 1;
	setAttr ".wt" 0.48376497626304626;
	setAttr ".re" 20;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySoftEdge -n "polySoftEdge2";
	rename -uid "360D7E7D-4469-04AE-5D39-ECACB23827F9";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[176:178]" "e[459]";
	setAttr ".ix" -type "matrix" 4.3541510445179226 0 0 0 0 4.3541510445179226 0 0 0 0 15.384666766119331 0
		 0 0 0.084883071105291502 1;
	setAttr ".a" 0;
createNode polyCube -n "polyCube3";
	rename -uid "47999986-488A-0C03-82E4-1881684BFB75";
	setAttr ".cuv" 4;
createNode polySplitRing -n "polySplitRing46";
	rename -uid "40F96081-4C62-F8F0-8CC7-89A77F7B7E58";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[6:7]" "e[10:11]";
	setAttr ".ix" -type "matrix" 0.087683040107048332 0 0 0 0 1 0 0 0 0 1 0 1.7337253692175811 -0.18582301530098655 2.0975388498019512 1;
	setAttr ".wt" 0.24828550219535828;
	setAttr ".re" 7;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak15";
	rename -uid "DC7770D2-4B43-36DD-EF2F-3D92B792DD8A";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk[0:7]" -type "float3"  0 -0.060142551 -0.19546328
		 0 -0.060142551 -0.19546328 0 -0.47111669 1.032447338 0 -0.47111675 1.032447338 0
		 -0.050118785 -0.57135427 0 -0.050118785 -0.57135427 0 0.49617612 -0.76681769 0 0.49617612
		 -0.76681769;
createNode polySplitRing -n "polySplitRing47";
	rename -uid "0E42DCDC-4E65-5546-B1BE-02BB004C86CF";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[10:13]";
	setAttr ".ix" -type "matrix" 0.087683040107048332 0 0 0 0 1 0 0 0 0 1 0 1.7337253692175811 -0.18582301530098655 2.0975388498019512 1;
	setAttr ".wt" 0.43165230751037598;
	setAttr ".re" 12;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing48";
	rename -uid "CD8786E0-41C1-D31F-F117-CD91058CE37C";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[10:11]" "e[20:21]";
	setAttr ".ix" -type "matrix" 0.087683040107048332 0 0 0 0 1 0 0 0 0 1 0 1.7337253692175811 -0.18582301530098655 2.0975388498019512 1;
	setAttr ".wt" 0.60168921947479248;
	setAttr ".dr" no;
	setAttr ".re" 20;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyExtrudeFace -n "polyExtrudeFace3";
	rename -uid "0959EBC4-4350-2391-475A-BD976AA1ADF4";
	setAttr ".ics" -type "componentList" 1 "f[2]";
	setAttr ".ix" -type "matrix" 0.087683040107048332 0 0 0 0 1 0 0 0 0 1 0 1.7337253692175811 -0.18582301530098655 2.0975388498019512 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 1.7337254 0.037205659 0.92845285 ;
	setAttr ".rs" 58661;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 1.6898838491640569 -0.18964689168190696 0.83072116085999559 ;
	setAttr ".cbx" -type "double3" 1.7775668892711054 0.26405821052336953 1.0261845798205669 ;
createNode polyTweak -n "polyTweak16";
	rename -uid "C93196C0-4904-B83D-FED5-319209B9537E";
	setAttr ".uopa" yes;
	setAttr -s 14 ".tk";
	setAttr ".tk[8]" -type "float3" 0 0.11527322 0.090213828 ;
	setAttr ".tk[9]" -type "float3" 0 0.11527322 0.090213828 ;
	setAttr ".tk[10]" -type "float3" 0 -0.08019007 -0.0050118794 ;
	setAttr ".tk[11]" -type "float3" 0 -0.08019007 -0.0050118794 ;
	setAttr ".tk[12]" -type "float3" 0 0.20548713 0.25560591 ;
	setAttr ".tk[13]" -type "float3" 0 0.20548713 0.25560591 ;
	setAttr ".tk[14]" -type "float3" 0 -0.065154433 -0.020047517 ;
	setAttr ".tk[15]" -type "float3" 0 -0.065154433 -0.020047517 ;
	setAttr ".tk[16]" -type "float3" 0 0.1202851 0.10524946 ;
	setAttr ".tk[17]" -type "float3" 0 0.1202851 0.10524946 ;
	setAttr ".tk[18]" -type "float3" 0 -0.020047517 -0.015035639 ;
	setAttr ".tk[19]" -type "float3" 0 -0.020047517 -0.015035639 ;
createNode polyCube -n "polyCube4";
	rename -uid "9E8E7A06-4BB7-4745-E38A-E189F777CDAF";
	setAttr ".cuv" 4;
createNode polySplitRing -n "polySplitRing49";
	rename -uid "5C54F938-426F-9E08-5704-CDA4FAA10D70";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[4:5]" "e[8:9]";
	setAttr ".ix" -type "matrix" 0.03333330020876149 0 0 0 0 1 0 0 0 0 1 0 0 -2.6813777023466527 2.8016959325801558 1;
	setAttr ".wt" 0.71939730644226074;
	setAttr ".dr" no;
	setAttr ".re" 9;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak17";
	rename -uid "870F1A1A-49D0-D7F4-C797-B78042D2B0CB";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk[0:7]" -type "float3"  0 -0.58417267 -0.87087685
		 0 -0.58417267 -0.87087685 0 -0.19367585 0.046528172 0 -0.19367585 0.046528172 0 -0.19367585
		 0.524306 0 -0.19367585 0.524306 0 0.37401319 -0.23348546 0 0.37401319 -0.23348546;
createNode polySplitRing -n "polySplitRing50";
	rename -uid "B69B9C8A-4166-ED2C-2A0C-C590FCC6CB7D";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[8:9]" "e[15]" "e[17]";
	setAttr ".ix" -type "matrix" 0.03333330020876149 0 0 0 0 1 0 0 0 0 1 0 0 -2.6813777023466527 2.8016959325801558 1;
	setAttr ".wt" 0.31364655494689941;
	setAttr ".re" 9;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak18";
	rename -uid "9569CE09-4A51-2B68-036E-A79DDB3539D2";
	setAttr ".uopa" yes;
	setAttr -s 5 ".tk";
	setAttr ".tk[8]" -type "float3" 0 0.16819219 -0.14780526 ;
	setAttr ".tk[9]" -type "float3" 0 0.16819219 -0.14780526 ;
	setAttr ".tk[10]" -type "float3" 0 -0.015290199 0.076450996 ;
	setAttr ".tk[11]" -type "float3" 0 -0.015290199 0.076450996 ;
createNode polySplitRing -n "polySplitRing51";
	rename -uid "4A980853-4F5E-6E72-EBEB-8FB8F5FB2B08";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[4:5]" "e[12:13]";
	setAttr ".ix" -type "matrix" 0.03333330020876149 0 0 0 0 1 0 0 0 0 1 0 0 -2.0776299056160927 -1.48809104418961 1;
	setAttr ".wt" 0.50989949703216553;
	setAttr ".dr" no;
	setAttr ".re" 12;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyExtrudeFace -n "polyExtrudeFace4";
	rename -uid "22B17C95-4D4D-5A02-7E6E-2499EB4D5D33";
	setAttr ".ics" -type "componentList" 1 "f[3]";
	setAttr ".ix" -type "matrix" 0.03333330020876149 0 0 0 0 1 0 0 0 0 1 0 0 -2.0776299056160927 -1.48809104418961 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0 -2.4118164 -2.0120051 ;
	setAttr ".rs" 54818;
	setAttr ".lt" -type "double3" 0 -5.5511151231257827e-17 0.36731973176980209 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.016666650104380745 -2.488102805291462 -2.0566849038621564 ;
	setAttr ".cbx" -type "double3" 0.016666650104380745 -2.3355299946415902 -1.9673252329846997 ;
createNode polyTweak -n "polyTweak19";
	rename -uid "66FF0D87-4674-E4F9-5343-6A83F8A403C1";
	setAttr ".uopa" yes;
	setAttr -s 6 ".tk";
	setAttr ".tk[0]" -type "float3" 0 0.018844754 -0.0047111884 ;
	setAttr ".tk[1]" -type "float3" 0 0.018844754 -0.0047111884 ;
	setAttr ".tk[16]" -type "float3" 0 -0.051823068 0.070667826 ;
	setAttr ".tk[17]" -type "float3" 0 -0.051823068 0.070667826 ;
	setAttr ".tk[18]" -type "float3" 0 -0.0094223768 0 ;
	setAttr ".tk[19]" -type "float3" 0 -0.0094223768 0 ;
createNode polyExtrudeFace -n "polyExtrudeFace5";
	rename -uid "46C45637-41B8-B51C-0F67-20BC1E96A243";
	setAttr ".ics" -type "componentList" 1 "f[3]";
	setAttr ".ix" -type "matrix" 0.03333330020876149 0 0 0 0 1 0 0 0 0 1 0 0 -2.0776299056160927 -1.48809104418961 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0 -2.6068766 -2.352519 ;
	setAttr ".rs" 42276;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.016666652091200263 -2.6492579993355752 -2.3971989080926557 ;
	setAttr ".cbx" -type "double3" 0.016666652091200263 -2.5644953068840981 -2.3078391478082318 ;
createNode polyTweak -n "polyTweak20";
	rename -uid "993D0EDA-4060-1535-0ABC-3BAC66224112";
	setAttr ".uopa" yes;
	setAttr -s 24 ".tk[0:23]" -type "float3"  -7.4505806e-09 1.8626451e-09
		 -3.7252903e-09 7.4505806e-09 1.8626451e-09 -3.7252903e-09 -7.4505806e-09 -1.4901161e-08
		 1.4901161e-08 7.4505806e-09 -1.4901161e-08 1.4901161e-08 -7.4505806e-09 2.9802322e-08
		 -7.4505806e-09 7.4505806e-09 2.9802322e-08 -7.4505806e-09 -7.4505806e-09 3.7252903e-09
		 3.7252903e-09 7.4505806e-09 3.7252903e-09 3.7252903e-09 7.4505806e-09 0 0 -7.4505806e-09
		 0 0 -7.4505806e-09 0 1.4901161e-08 7.4505806e-09 0 1.4901161e-08 7.4505806e-09 0
		 0 -7.4505806e-09 0 0 -7.4505806e-09 0 0 7.4505806e-09 0 0 7.4505806e-09 -1.4901161e-08
		 -7.4505806e-09 -7.4505806e-09 -1.4901161e-08 -7.4505806e-09 -7.4505806e-09 0 0 7.4505806e-09
		 0 0 -7.4505806e-09 -0.043327458 -0.023555942 7.4505806e-09 -0.043327458 -0.023555942
		 7.4505806e-09 0.024482708 -0.023555942 -7.4505806e-09 0.024482708 -0.023555942;
createNode polyExtrudeFace -n "polyExtrudeFace6";
	rename -uid "029CEEA6-4518-A741-AD37-A1A31B34C8A6";
	setAttr ".ics" -type "componentList" 1 "f[1]";
	setAttr ".ix" -type "matrix" -0.03333330020876149 4.0821519405677556e-18 0 0 -1.2246467991473532e-16 -1 0 0
		 0 0 1 0 0 0.37200970981348958 -6.7557166035190948 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -2.7755576e-17 0.13947603 -6.4629402 ;
	setAttr ".rs" 40596;
	setAttr ".lt" -type "double3" -1.2325951644078309e-32 -6.2992146221407808e-17 0.24211081594628067 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.016666652091200291 0.13774500820864644 -7.0580899531856902 ;
	setAttr ".cbx" -type "double3" 0.016666652091200235 0.14120705459345784 -5.8677909190670379 ;
createNode polyExtrudeFace -n "polyExtrudeFace7";
	rename -uid "8184479C-4CA8-0CBE-68D1-8B84042B432A";
	setAttr ".ics" -type "componentList" 1 "f[1]";
	setAttr ".ix" -type "matrix" -0.03333330020876149 4.0821519405677556e-18 0 0 -1.2246467991473532e-16 -1 0 0
		 0 0 1 0 0 0.37200970981348958 -6.7557166035190948 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -5.8980598e-17 -0.10263376 -6.4507098 ;
	setAttr ".rs" 63708;
	setAttr ".lt" -type "double3" -1.2325951644078309e-32 1.0513508466591936e-15 0.28348602797920192 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.016666652091200322 -0.10436479713688884 -7.0458595271126159 ;
	setAttr ".cbx" -type "double3" 0.016666652091200204 -0.10090272094975505 -5.8555605227962859 ;
createNode polyTweak -n "polyTweak21";
	rename -uid "9A866E1E-415A-CB35-F605-8887D71AE1E5";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[28:31]" -type "float3"  0 0 0.012934293 0 0 0.012934293
		 0 0 0.012934293 0 0 0.012934293;
createNode polyExtrudeFace -n "polyExtrudeFace8";
	rename -uid "533DC0B4-4E95-6391-483D-C7AC2807FD66";
	setAttr ".ics" -type "componentList" 1 "f[1]";
	setAttr ".ix" -type "matrix" -0.03333330020876149 4.0821519405677556e-18 0 0 -1.2246467991473532e-16 -1 0 0
		 0 0 1 0 0 0.37200970981348958 -6.7557166035190948 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -9.3675068e-17 -0.38611856 -6.5705304 ;
	setAttr ".rs" 40436;
	setAttr ".lt" -type "double3" -2.4651903288156619e-32 1.3446275343165226e-15 0.31743513017561664 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.016666652091200357 -0.38784961368809734 -7.1656799728886016 ;
	setAttr ".cbx" -type "double3" 0.016666652091200169 -0.38438753750096355 -5.9753809387699492 ;
createNode polyTweak -n "polyTweak22";
	rename -uid "F61CBE89-4ED5-5E30-811F-E883547EDBA1";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[32:35]" -type "float3"  0 0 -0.11899543 0 0 -0.11899543
		 0 0 -0.11899543 0 0 -0.11899543;
createNode polyExtrudeFace -n "polyExtrudeFace9";
	rename -uid "62EA606E-4DAF-D1B0-14D1-E08E0BE9AEBB";
	setAttr ".ics" -type "componentList" 1 "f[1]";
	setAttr ".ix" -type "matrix" -0.03333330020876149 4.0821519405677556e-18 0 0 -1.2246467991473532e-16 -1 0 0
		 0 0 1 0 0 0.37200970981348958 -6.7557166035190948 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -1.3183898e-16 -0.70355242 -6.5714536 ;
	setAttr ".rs" 51321;
	setAttr ".lt" -type "double3" 2.4651903288156619e-32 -2.9728823569552532e-16 0.46469623741955796 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.016666652091200395 -0.70528344776402507 -7.1666028912083037 ;
	setAttr ".cbx" -type "double3" 0.016666652091200131 -0.70182137157689128 -5.9763038570896514 ;
createNode polyExtrudeFace -n "polyExtrudeFace10";
	rename -uid "827BCC68-4CAF-D931-0CD3-9E92727EEA52";
	setAttr ".ics" -type "componentList" 1 "f[1]";
	setAttr ".ix" -type "matrix" -0.03333330020876149 4.0821519405677556e-18 0 0 -1.2246467991473532e-16 -1 0 0
		 0 0 1 0 0 0.37200970981348958 -6.7557166035190948 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -1.8735014e-16 -1.1682466 -6.5728049 ;
	setAttr ".rs" 65238;
	setAttr ".lt" -type "double3" 0 9.3458227268250482e-16 0.39661673950958998 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.016666652091200451 -1.1699777093149284 -7.1679542477146514 ;
	setAttr ".cbx" -type "double3" 0.016666652091200076 -1.1665156331277946 -5.9776552135959991 ;
createNode polyExtrudeFace -n "polyExtrudeFace11";
	rename -uid "D8D50FC3-4759-8112-EABD-C181389BD10D";
	setAttr ".ics" -type "componentList" 1 "f[1]";
	setAttr ".ix" -type "matrix" -0.03333330020876149 4.0821519405677556e-18 0 0 -1.2246467991473532e-16 -1 0 0
		 0 0 1 0 0 0.37200970981348958 -6.7557166035190948 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -2.3592239e-16 -1.5648617 -6.5739589 ;
	setAttr ".rs" 49574;
	setAttr ".lt" -type "double3" -4.9303806576313238e-32 -1.553878553606225e-15 0.39655081459426672 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.016666652091200499 -1.566592737696276 -7.169108193637503 ;
	setAttr ".cbx" -type "double3" 0.016666652091200027 -1.5631306615091423 -5.9788091595188506 ;
createNode polyExtrudeFace -n "polyExtrudeFace12";
	rename -uid "FB68ABA8-46D6-ABCD-4E15-4FAE5BA1406D";
	setAttr ".ics" -type "componentList" 1 "f[1]";
	setAttr ".ix" -type "matrix" -0.03333330020876149 4.0821519405677556e-18 0 0 -1.2246467991473532e-16 -1 0 0
		 0 0 1 0 0 0.37200970981348958 -6.7557166035190948 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -2.8449465e-16 -1.9614109 -6.5751119 ;
	setAttr ".rs" 59559;
	setAttr ".lt" -type "double3" 0 -6.7220534694101275e-16 0.49839812118351151 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.016666652091200548 -1.9631419625497917 -7.1702611858860381 ;
	setAttr ".cbx" -type "double3" 0.016666652091199979 -1.9596798863626579 -5.9799621517673858 ;
createNode polySplitRing -n "polySplitRing52";
	rename -uid "1D6E019E-4171-22A1-6E72-7A98A7EA6E13";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[100:101]" "e[103]" "e[105]";
	setAttr ".ix" -type "matrix" -0.03333330020876149 4.0821519405677556e-18 0 0 -1.2246467991473532e-16 -1 0 0
		 0 0 1 0 0 0.37200970981348958 -6.7557166035190948 1;
	setAttr ".wt" 0.58672702312469482;
	setAttr ".dr" no;
	setAttr ".re" 105;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak23";
	rename -uid "AC7D837A-4095-849D-1FF4-FDB91E5FE5B8";
	setAttr ".uopa" yes;
	setAttr -s 25 ".tk";
	setAttr ".tk[32]" -type "float3" -1.110223e-16 0.15299413 -0.21532507 ;
	setAttr ".tk[33]" -type "float3" 1.110223e-16 0.15299413 -0.21532507 ;
	setAttr ".tk[34]" -type "float3" 0 0.010985598 0.10436318 ;
	setAttr ".tk[35]" -type "float3" 0 0.010985598 0.10436318 ;
	setAttr ".tk[36]" -type "float3" -8.8817842e-16 0.25499019 -0.48164806 ;
	setAttr ".tk[37]" -type "float3" 8.8817842e-16 0.25499019 -0.48164806 ;
	setAttr ".tk[38]" -type "float3" 0 0 0.060420789 ;
	setAttr ".tk[39]" -type "float3" 0 0 0.060420789 ;
	setAttr ".tk[40]" -type "float3" -1.9984014e-15 0.26065671 -0.75363761 ;
	setAttr ".tk[41]" -type "float3" 1.9984014e-15 0.26065671 -0.75363761 ;
	setAttr ".tk[42]" -type "float3" -2.220446e-16 -0.15929116 -0.038449593 ;
	setAttr ".tk[43]" -type "float3" 2.220446e-16 -0.15929116 -0.038449593 ;
	setAttr ".tk[44]" -type "float3" -5.5511151e-16 0.20399216 -1.03696 ;
	setAttr ".tk[45]" -type "float3" 5.5511151e-16 0.20399216 -1.03696 ;
	setAttr ".tk[46]" -type "float3" -5.5511151e-16 -0.19774081 -0.19774081 ;
	setAttr ".tk[47]" -type "float3" 5.5511151e-16 -0.19774081 -0.19774081 ;
	setAttr ".tk[48]" -type "float3" -4.4408921e-16 0.11332896 -1.3939466 ;
	setAttr ".tk[49]" -type "float3" 4.4408921e-16 0.11332896 -1.3939466 ;
	setAttr ".tk[50]" -type "float3" -8.8817842e-16 -0.2746399 -0.40097439 ;
	setAttr ".tk[51]" -type "float3" 8.8817842e-16 -0.2746399 -0.40097439 ;
	setAttr ".tk[52]" -type "float3" -1.110223e-16 0 -2.1476839 ;
	setAttr ".tk[53]" -type "float3" 1.110223e-16 0 -2.1476839 ;
	setAttr ".tk[54]" -type "float3" -9.9920072e-16 -0.19224796 -0.71406388 ;
	setAttr ".tk[55]" -type "float3" 9.9920072e-16 -0.19224796 -0.71406388 ;
createNode polyCube -n "polyCube5";
	rename -uid "E0E08900-4140-5D08-438D-40BC3F856203";
	setAttr ".cuv" 4;
createNode polySplitRing -n "polySplitRing53";
	rename -uid "72E80E66-44B4-F18A-3FB3-27B39B535BAA";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[4:5]" "e[8:9]";
	setAttr ".ix" -type "matrix" 0.023012869104460574 0 -0 0 -0 0.79242939356019126 -0.60996365155788379 0
		 0 0.19798245092048203 0.25720731574378047 0 0 1.4015440702648601 -1.8578607443045823 1;
	setAttr ".wt" 0.34311246871948242;
	setAttr ".re" 5;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak24";
	rename -uid "6E1E7394-47D4-BA0D-BD3D-87BC27B89E03";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk[0:7]" -type "float3"  0 0.30771846 0.083157293 0
		 0.30771846 0.083157293 0 -0.3996042 -0.42757052 0 -0.3996042 -0.42757052 0 -0.29714465
		 -0.015912265 0 -0.29714465 -0.015912265 0 0.40446717 0.29853892 0 0.40446717 0.29853892;
createNode polySplitRing -n "polySplitRing54";
	rename -uid "45732BBC-4DF5-C65F-0243-74B03388F603";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[8:9]" "e[12:13]";
	setAttr ".ix" -type "matrix" 0.023012869104460574 0 -0 0 -0 0.79242939356019126 -0.60996365155788379 0
		 0 0.19798245092048203 0.25720731574378047 0 0 1.4015440702648601 -1.8578607443045823 1;
	setAttr ".wt" 0.56025463342666626;
	setAttr ".dr" no;
	setAttr ".re" 12;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr -s 24 ".dsm";
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultRenderGlobals;
	setAttr ".ren" -type "string" "arnold";
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
connectAttr "polySoftEdge2.out" "pCubeShape1.i";
connectAttr ":defaultColorMgtGlobals.cme" "imagePlaneShape1.cme";
connectAttr ":defaultColorMgtGlobals.cfe" "imagePlaneShape1.cmcf";
connectAttr ":defaultColorMgtGlobals.cfp" "imagePlaneShape1.cmcp";
connectAttr ":defaultColorMgtGlobals.wsn" "imagePlaneShape1.ws";
connectAttr ":sideShape.msg" "imagePlaneShape1.ltc";
connectAttr "polySplitRing45.out" "pCubeShape2.i";
connectAttr "polyExtrudeFace3.out" "pCubeShape3.i";
connectAttr "polySplitRing50.out" "pCubeShape4.i";
connectAttr "polyExtrudeFace5.out" "pCubeShape5.i";
connectAttr "polySplitRing52.out" "pCubeShape7.i";
connectAttr "polySplitRing54.out" "pCubeShape8.i";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "polyTweak1.out" "polySplitRing1.ip";
connectAttr "pCubeShape1.wm" "polySplitRing1.mp";
connectAttr "polyCube1.out" "polyTweak1.ip";
connectAttr "polySplitRing1.out" "polySplitRing2.ip";
connectAttr "pCubeShape1.wm" "polySplitRing2.mp";
connectAttr "polySplitRing2.out" "polySplitRing3.ip";
connectAttr "pCubeShape1.wm" "polySplitRing3.mp";
connectAttr "polySplitRing3.out" "polySplitRing4.ip";
connectAttr "pCubeShape1.wm" "polySplitRing4.mp";
connectAttr "polySplitRing4.out" "polySplitRing5.ip";
connectAttr "pCubeShape1.wm" "polySplitRing5.mp";
connectAttr "polySplitRing5.out" "polySplitRing6.ip";
connectAttr "pCubeShape1.wm" "polySplitRing6.mp";
connectAttr "polySplitRing6.out" "polySplitRing7.ip";
connectAttr "pCubeShape1.wm" "polySplitRing7.mp";
connectAttr "polySplitRing7.out" "polySplitRing8.ip";
connectAttr "pCubeShape1.wm" "polySplitRing8.mp";
connectAttr "polySplitRing8.out" "polySplitRing9.ip";
connectAttr "pCubeShape1.wm" "polySplitRing9.mp";
connectAttr "polyTweak2.out" "polySplitRing10.ip";
connectAttr "pCubeShape1.wm" "polySplitRing10.mp";
connectAttr "polySplitRing9.out" "polyTweak2.ip";
connectAttr "polyTweak3.out" "polySplitRing11.ip";
connectAttr "pCubeShape1.wm" "polySplitRing11.mp";
connectAttr "polySplitRing10.out" "polyTweak3.ip";
connectAttr "polyTweak4.out" "polySplitRing12.ip";
connectAttr "pCubeShape1.wm" "polySplitRing12.mp";
connectAttr "polySplitRing11.out" "polyTweak4.ip";
connectAttr "polyTweak5.out" "polySplitRing13.ip";
connectAttr "pCubeShape1.wm" "polySplitRing13.mp";
connectAttr "polySplitRing12.out" "polyTweak5.ip";
connectAttr "polyTweak6.out" "polySplitRing14.ip";
connectAttr "pCubeShape1.wm" "polySplitRing14.mp";
connectAttr "polySplitRing13.out" "polyTweak6.ip";
connectAttr "polySplitRing14.out" "polyExtrudeFace1.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace1.mp";
connectAttr "polyTweak7.out" "polyExtrudeFace2.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace2.mp";
connectAttr "polyExtrudeFace1.out" "polyTweak7.ip";
connectAttr "polyTweak8.out" "polySplitRing15.ip";
connectAttr "pCubeShape1.wm" "polySplitRing15.mp";
connectAttr "polyExtrudeFace2.out" "polyTweak8.ip";
connectAttr "polySplitRing15.out" "polySplitRing16.ip";
connectAttr "pCubeShape1.wm" "polySplitRing16.mp";
connectAttr "polySplitRing16.out" "polySplitRing17.ip";
connectAttr "pCubeShape1.wm" "polySplitRing17.mp";
connectAttr "polyTweak9.out" "polySplitRing18.ip";
connectAttr "pCubeShape1.wm" "polySplitRing18.mp";
connectAttr "polySplitRing17.out" "polyTweak9.ip";
connectAttr "polyTweak10.out" "polySoftEdge1.ip";
connectAttr "pCubeShape1.wm" "polySoftEdge1.mp";
connectAttr "polySplitRing18.out" "polyTweak10.ip";
connectAttr "polyTweak11.out" "polySplitRing19.ip";
connectAttr "pCubeShape1.wm" "polySplitRing19.mp";
connectAttr "polySoftEdge1.out" "polyTweak11.ip";
connectAttr "polyTweak12.out" "polySplitRing20.ip";
connectAttr "pCubeShape2.wm" "polySplitRing20.mp";
connectAttr "polyCube2.out" "polyTweak12.ip";
connectAttr "polySplitRing20.out" "polySplitRing21.ip";
connectAttr "pCubeShape2.wm" "polySplitRing21.mp";
connectAttr "polySplitRing21.out" "polySplitRing22.ip";
connectAttr "pCubeShape2.wm" "polySplitRing22.mp";
connectAttr "polySplitRing22.out" "polySplitRing23.ip";
connectAttr "pCubeShape2.wm" "polySplitRing23.mp";
connectAttr "polySplitRing23.out" "polySplitRing24.ip";
connectAttr "pCubeShape2.wm" "polySplitRing24.mp";
connectAttr "polySplitRing24.out" "polySplitRing25.ip";
connectAttr "pCubeShape2.wm" "polySplitRing25.mp";
connectAttr "polyTweak13.out" "polySplitRing26.ip";
connectAttr "pCubeShape2.wm" "polySplitRing26.mp";
connectAttr "polySplitRing25.out" "polyTweak13.ip";
connectAttr "polySplitRing26.out" "polySplitRing27.ip";
connectAttr "pCubeShape2.wm" "polySplitRing27.mp";
connectAttr "polySplitRing27.out" "polySplitRing28.ip";
connectAttr "pCubeShape2.wm" "polySplitRing28.mp";
connectAttr "polySplitRing28.out" "polySplitRing29.ip";
connectAttr "pCubeShape2.wm" "polySplitRing29.mp";
connectAttr "polySplitRing29.out" "polySplitRing30.ip";
connectAttr "pCubeShape2.wm" "polySplitRing30.mp";
connectAttr "polySplitRing30.out" "polySplitRing31.ip";
connectAttr "pCubeShape2.wm" "polySplitRing31.mp";
connectAttr "polySplitRing31.out" "polySplitRing32.ip";
connectAttr "pCubeShape2.wm" "polySplitRing32.mp";
connectAttr "polyTweak14.out" "polySplitRing33.ip";
connectAttr "pCubeShape2.wm" "polySplitRing33.mp";
connectAttr "polySplitRing32.out" "polyTweak14.ip";
connectAttr "polySplitRing33.out" "polySplitRing34.ip";
connectAttr "pCubeShape2.wm" "polySplitRing34.mp";
connectAttr "polySplitRing34.out" "polySplitRing35.ip";
connectAttr "pCubeShape2.wm" "polySplitRing35.mp";
connectAttr "polySplitRing35.out" "polySplitRing36.ip";
connectAttr "pCubeShape2.wm" "polySplitRing36.mp";
connectAttr "polySplitRing36.out" "polySplitRing37.ip";
connectAttr "pCubeShape2.wm" "polySplitRing37.mp";
connectAttr "polySplitRing37.out" "polySplitRing38.ip";
connectAttr "pCubeShape2.wm" "polySplitRing38.mp";
connectAttr "polySplitRing38.out" "polySplitRing39.ip";
connectAttr "pCubeShape2.wm" "polySplitRing39.mp";
connectAttr "polySplitRing39.out" "polySplitRing40.ip";
connectAttr "pCubeShape2.wm" "polySplitRing40.mp";
connectAttr "polySplitRing40.out" "polySplitRing41.ip";
connectAttr "pCubeShape2.wm" "polySplitRing41.mp";
connectAttr "polySplitRing41.out" "polySplitRing42.ip";
connectAttr "pCubeShape2.wm" "polySplitRing42.mp";
connectAttr "polySplitRing42.out" "polySplitRing43.ip";
connectAttr "pCubeShape2.wm" "polySplitRing43.mp";
connectAttr "polySplitRing43.out" "polySplitRing44.ip";
connectAttr "pCubeShape2.wm" "polySplitRing44.mp";
connectAttr "polySplitRing44.out" "polySplitRing45.ip";
connectAttr "pCubeShape2.wm" "polySplitRing45.mp";
connectAttr "polySplitRing19.out" "polySoftEdge2.ip";
connectAttr "pCubeShape1.wm" "polySoftEdge2.mp";
connectAttr "polyTweak15.out" "polySplitRing46.ip";
connectAttr "pCubeShape3.wm" "polySplitRing46.mp";
connectAttr "polyCube3.out" "polyTweak15.ip";
connectAttr "polySplitRing46.out" "polySplitRing47.ip";
connectAttr "pCubeShape3.wm" "polySplitRing47.mp";
connectAttr "polySplitRing47.out" "polySplitRing48.ip";
connectAttr "pCubeShape3.wm" "polySplitRing48.mp";
connectAttr "polyTweak16.out" "polyExtrudeFace3.ip";
connectAttr "pCubeShape3.wm" "polyExtrudeFace3.mp";
connectAttr "polySplitRing48.out" "polyTweak16.ip";
connectAttr "polyTweak17.out" "polySplitRing49.ip";
connectAttr "pCubeShape4.wm" "polySplitRing49.mp";
connectAttr "polyCube4.out" "polyTweak17.ip";
connectAttr "polyTweak18.out" "polySplitRing50.ip";
connectAttr "pCubeShape4.wm" "polySplitRing50.mp";
connectAttr "polySplitRing49.out" "polyTweak18.ip";
connectAttr "|pCube5|polySurfaceShape1.o" "polySplitRing51.ip";
connectAttr "pCubeShape5.wm" "polySplitRing51.mp";
connectAttr "polyTweak19.out" "polyExtrudeFace4.ip";
connectAttr "pCubeShape5.wm" "polyExtrudeFace4.mp";
connectAttr "polySplitRing51.out" "polyTweak19.ip";
connectAttr "polyTweak20.out" "polyExtrudeFace5.ip";
connectAttr "pCubeShape5.wm" "polyExtrudeFace5.mp";
connectAttr "polyExtrudeFace4.out" "polyTweak20.ip";
connectAttr "polySurfaceShape2.o" "polyExtrudeFace6.ip";
connectAttr "pCubeShape7.wm" "polyExtrudeFace6.mp";
connectAttr "polyTweak21.out" "polyExtrudeFace7.ip";
connectAttr "pCubeShape7.wm" "polyExtrudeFace7.mp";
connectAttr "polyExtrudeFace6.out" "polyTweak21.ip";
connectAttr "polyTweak22.out" "polyExtrudeFace8.ip";
connectAttr "pCubeShape7.wm" "polyExtrudeFace8.mp";
connectAttr "polyExtrudeFace7.out" "polyTweak22.ip";
connectAttr "polyExtrudeFace8.out" "polyExtrudeFace9.ip";
connectAttr "pCubeShape7.wm" "polyExtrudeFace9.mp";
connectAttr "polyExtrudeFace9.out" "polyExtrudeFace10.ip";
connectAttr "pCubeShape7.wm" "polyExtrudeFace10.mp";
connectAttr "polyExtrudeFace10.out" "polyExtrudeFace11.ip";
connectAttr "pCubeShape7.wm" "polyExtrudeFace11.mp";
connectAttr "polyExtrudeFace11.out" "polyExtrudeFace12.ip";
connectAttr "pCubeShape7.wm" "polyExtrudeFace12.mp";
connectAttr "polyTweak23.out" "polySplitRing52.ip";
connectAttr "pCubeShape7.wm" "polySplitRing52.mp";
connectAttr "polyExtrudeFace12.out" "polyTweak23.ip";
connectAttr "polyTweak24.out" "polySplitRing53.ip";
connectAttr "pCubeShape8.wm" "polySplitRing53.mp";
connectAttr "polyCube5.out" "polyTweak24.ip";
connectAttr "polySplitRing53.out" "polySplitRing54.ip";
connectAttr "pCubeShape8.wm" "polySplitRing54.mp";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "pCubeShape1.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape2.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape3.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape4.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape5.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape6.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape7.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape8.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape9.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape10.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape11.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape12.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape13.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape14.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape15.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape16.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape17.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape18.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape19.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape20.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape21.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape22.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape23.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape24.iog" ":initialShadingGroup.dsm" -na;
// End of Bluefin_Tuna.ma
